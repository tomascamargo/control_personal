/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import modelo.PersonaGrupo;
//import modelo.Empleado;
import modelo.Userinfo;

/**
 *
 * @author Viktor
 */
public class ContUserinfo extends ControladorGeneral {

    public ContUserinfo() {
        super();
    }
    
    Userinfo setearEnObjeto(List lista){
        Userinfo user = new Userinfo(Integer.parseInt(lista.get(0).toString()),Long.parseLong(lista.get(1).toString()), lista.get(2).toString(), Long.parseLong(lista.get(3).toString()), Integer.parseInt(lista.get(4).toString()),Integer.parseInt(lista.get(5).toString()),
        lista.get(6).toString(),lista.get(7).toString(),lista.get(8).toString(),Timestamp.valueOf(lista.get(9).toString()));
        return user;
    }
    
    public int create(Userinfo user) {

        Object[] arreglo = {
            user.getUserid(),
            user.getName(),
            user.getCucupers(),
            user.getTerminal(),
            user.getIp(),
            user.getUsuariosistema()
        };

        return ejecutarSentencia(arreglo, BDSentencias.userinfoInsert, BDSentencias.conexionRelojLocal);
    }
    
    public int update(Userinfo user){
        Object[] arreglo = {user.getUserid(),user.getName(),user.getCucupers(),user.getReloj(),user.getIdSeccion(),user.getTerminal(),user.getIp(),user.getUsuariosistema(),user.getFechaauditoria(),user.getId()};

        return ejecutarSentencia(arreglo, BDSentencias.userinfoUPDATE, BDSentencias.conexionRelojLocal);
    }
    
    public List<Userinfo> listar(){
        List<List> lista = new ArrayList<>();
        List<Userinfo> listUserinfo = new ArrayList<>();        
        lista = dameListaCompleta(BDSentencias.coluUSERINFO, BDSentencias.consultaUserinfoALL, BDSentencias.conexionRelojLocal);
        for (List l : lista) {
            Userinfo u = setearEnObjeto(l);
            listUserinfo.add(u);
        }
        
        Collections.sort(listUserinfo, new Comparator<Userinfo>() {

            @Override
            public int compare(Userinfo o1, Userinfo o2) {
                return new String(o1.getName()).compareTo(new String(o2.getName()));
//                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
        
        return listUserinfo;
    }
    
    public Userinfo encontrar(Long userid, String reloj){
        Object [] parametros = {userid,reloj};
        Userinfo userinfo = null;
        List<List> lista = buscarPorParametro(BDSentencias.coluUSERINFO, consultaUserinfoFINDxUSERID, parametros, BDSentencias.conexionRelojLocal);
        for (List l : lista) {
            userinfo = setearEnObjeto(l);
        }
        return userinfo;
    }
    
    public Userinfo encontrar(int id){
        Object [] parametros = {id};
        Userinfo userinfo = null;
        List<List> lista = buscarPorParametro(BDSentencias.coluUSERINFO, consultaUserinfoID, parametros, BDSentencias.conexionRelojLocal);
        for (List l : lista) {
            userinfo = setearEnObjeto(l);
        }
        return userinfo;
    }
    
    public Userinfo encontrarUSERID(int userid){
        Object [] parametros = {userid};
        Userinfo userinfo = null;
        List<List> lista = buscarPorParametro(BDSentencias.coluUSERINFO, consultaUserinfoUSERID, parametros, BDSentencias.conexionRelojLocal);
        for (List l : lista) {
            userinfo = setearEnObjeto(l);
        }
        return userinfo;
    }
    
//    f.getUserinfoid()
    public Userinfo encontrar(List<Userinfo> lista,Long userid){
        Userinfo userinfo = null;
        
        for (Userinfo u : lista) {
            if((u.getUserid() == userid || (userid - u.getUserid() == 0))){
                userinfo = u;
                break;
            }
        }
        return userinfo;
    }
    
    public boolean tieneHorario(int userinfoid) {
        boolean bandera = false;

        ContPersonaGrupo contPersonaGrupo = new ContPersonaGrupo();
        Time horaEntrada = contPersonaGrupo.getHoraEntrada(contPersonaGrupo.getIdGrupo(userinfoid)); //treae la hora en la que debe de entrar el empleado dependeiendo de su cuil

        if (horaEntrada != null) {
            bandera = true;
        }

        return bandera;
    }
    
    public PersonaGrupo tieneHorario(List<PersonaGrupo> lista, int userinfoid) {
        PersonaGrupo per = null;

        for(PersonaGrupo p: lista){
            if(userinfoid == p.getUserinfoid()){
                per = p;
                break;
            }
        }
        

        return per;
    }
    
//
    public String buscarFuncion(long cuil) {
        String funcion = "";
        Object[] datos = {cuil};

        List<List> lista = buscarPorParametro(BDSentencias.legajoFuncColumnas, BDSentencias.consultaFuncionLegajo, datos, BDSentencias.conexionPersonal);

        for (List l : lista) {
            funcion = l.get(0).toString();
        }

        if (funcion.equals("")) {
            lista = buscarPorParametro(BDSentencias.proveedoresFuncColumnas, BDSentencias.consultaFuncionProveedores, datos, BDSentencias.conexionProveedores);

            for (List l : lista) {
                funcion = l.get(0).toString();
            }
        }

        if (funcion.equals("")) {
            funcion = "Sin Funcion";
        }

        return funcion;
    }
//
    public String getHorario(int id) {

        ContPersonaGrupo contPersonaGrupo = new ContPersonaGrupo();
        int idGrupo = contPersonaGrupo.getIdGrupo(id);
        Time horaEntrada = contPersonaGrupo.getHoraEntrada(idGrupo); //treae la hora en la que debe de entrar el empleado dependeiendo de su cuil
        Time horaSalida = contPersonaGrupo.getHoraSalida(idGrupo);

        return horaEntrada + " a " + horaSalida;
    }

    
    public List<Userinfo> obtenerEmpleadosSinCapataz() {
        List<Userinfo> listaEmpleados = new ArrayList<>();
        List<List> lista= dameListaCompleta(coluUSERINFO, consultaUserinfoSinCapataz,conexionRelojLocal);
        
        for (List list : lista) {
            Userinfo u= setearEnObjeto(list);
            listaEmpleados.add(u);
        }
      return listaEmpleados; 
    }

//    public List<Userinfo> obtenerEmpleadosSinCapataz(String texto) {
//       Object [] datos = {"%"+texto+"%"};
//         List<Userinfo> listaEmpleados = new ArrayList<>();
//        List<List> lista= buscarPorParametro(coluUSERINFO, consultaUserinfoSinCapatazPorNombre,datos,conexionRelojLocal);
//        
//        for (List list : lista) {
//            Userinfo u= setearEnObjeto(list);
//            listaEmpleados.add(u);
//        }
//      return listaEmpleados;
//    }
    
    public List<Userinfo> obtenerEmpleadosPorSeccion(int id_seccion) {
       Object [] datos = {id_seccion};
         List<Userinfo> listaEmpleados = new ArrayList<>();
        List<List> lista= buscarPorParametro(coluUSERINFO, consultaUserinfoSinCapatazPorSeccion,datos,conexionRelojLocal);
        
        for (List list : lista) {
            Userinfo u= setearEnObjeto(list);
            listaEmpleados.add(u);
        }
      return listaEmpleados;
    }

    public List<Userinfo> obtenerEmpleadosPorArea(int idaArea) {
        Object [] datos = {idaArea};
         List<Userinfo> listaEmpleados = new ArrayList<>();
        List<List> lista= buscarPorParametro(coluUSERINFO, consultaUserinfoSinCapatazPorArea,datos,conexionRelojLocal);
        
        for (List list : lista) {
            Userinfo u= setearEnObjeto(list);
            listaEmpleados.add(u);
        }
      return listaEmpleados;
    }

    public List<Userinfo> obtenerEmpleadosPorUSUARIO(String user) {
        Object [] datos = {user};
         List<Userinfo> listaEmpleados = new ArrayList<>();
        List<List> lista= buscarPorParametro(coluUSERINFO, consultaUserinfoSinCapatazPorUSUARIO,datos,conexionRelojLocal);
        
        for (List list : lista) {
            Userinfo u= setearEnObjeto(list);
            listaEmpleados.add(u);
        }
      return listaEmpleados;
    }
}

