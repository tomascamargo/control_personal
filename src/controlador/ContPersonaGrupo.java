/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import modelo.Grupo;
import modelo.PersonaGrupo;
import modelo.Userinfo;

/**
 *
 * @author Viktor
 */
public class ContPersonaGrupo extends ControladorGeneral{

    public ContPersonaGrupo() {
        super();
    }
    
    PersonaGrupo setearObjeto(List l){
        return new PersonaGrupo(Integer.parseInt(l.get(0).toString()),Integer.parseInt(l.get(1).toString()),Integer.parseInt(l.get(2).toString()));
    }
    
    public List<PersonaGrupo> listarTodos(){
        List<PersonaGrupo> listaResult = new ArrayList<>();
        List<List> lista = dameListaCompleta(BDSentencias.personaGrupoColumnas, BDSentencias.consultaPersonaGrupoALL,BDSentencias.conexionRelojLocal);
        for (List list : lista) {
            listaResult.add(setearObjeto(list));
        }
        return listaResult;
    }
    
    public int getIdGrupo(int  iuser_d){
        Object[] datos = {iuser_d};
        int id = 0;
        int i = 0;
//        System.err.println("Sentencia sql: "+BDSentencias.personaGrupoPorId);
//        System.err.println("Parametro: "+cuil);
        List lista = buscarPorParametro(BDSentencias.personaGrupoColumnas, BDSentencias.personaGrupoPorId, datos, BDSentencias.conexionRelojLocal);
        for (Iterator it = lista.iterator(); it.hasNext();) {
            List object = (List) it.next();
            
                    id = Integer.parseInt(object.get(2).toString());
        }
        
//        System.err.println("id grupo: "+id);
        
        
        return id;
    }

    public Time getHoraEntrada(int idGrupo) {
        Object[] parametros = {idGrupo};
        Time hora = null;
        int i =0;
        List lista = buscarPorParametro(BDSentencias.grupoColumnas , BDSentencias.grupoPorID, parametros, BDSentencias.conexionRelojLocal);
        for (Iterator it = lista.iterator(); it.hasNext();) {
            List l = (List) it.next();
            
            hora = Time.valueOf(l.get(2).toString());
//            System.out.println("HORA E: "+hora.toString());
            
        }
        return hora;
    }
    
    public Time getHoraSalida(int idGrupo) {
        Object[] parametros = {idGrupo};
        Time hora = null;
        int i =0;
        List lista = buscarPorParametro(BDSentencias.grupoColumnas , BDSentencias.grupoPorID, parametros, BDSentencias.conexionRelojLocal);
        for (Iterator it = lista.iterator(); it.hasNext();) {
            List l = (List) it.next();
            
            hora = Time.valueOf(l.get(3).toString());
//            System.out.println("HORA E: "+hora.toString());
            
        }
        return hora;
    }
    
    public String traerHorario(Userinfo e) {
        int idGrupo = getIdGrupo(e.getId());
        String horaE = getHoraEntrada(idGrupo).toString();
        String horaS = getHoraSalida(idGrupo).toString();

        return horaE + " - " + horaS;
    }
    
    public String traerHorario(List<Grupo> lista,int idGrupo) {
        String horaE = null;
        String horaS = null;

        for (Grupo grupo : lista) {
            if(grupo.getId() == idGrupo){
                horaE = grupo.getEntrada().toString();
                horaS = grupo.getSalida().toString();
                break;
            }
        }
        
        return horaE + " - " + horaS;
    }
    
    public int update(PersonaGrupo p){
        System.out.println("user: "+p.getUserinfoid()
                + " grupo: "+p.getGrupo_id()
                + " ID: "+p.getId());
        System.out.println("SENTENCIA: "+updatePersonaGrupo1);
        Object [] datos = {p.getUserinfoid(),p.getGrupo_id(),p.getId()};
        return  ejecutarSentencia(datos, BDSentencias.updatePersonaGrupo1, conexionRelojLocal);
    }
    
    public int insert(PersonaGrupo p){
        Object [] datos = {p.getUserinfoid(),p.getGrupo_id()};
        return  ejecutarSentencia(datos, BDSentencias.personaGrupoInsert, conexionRelojLocal);
    }
    
}
