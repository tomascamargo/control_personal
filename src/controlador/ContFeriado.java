/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import modelo.Feriado;

/**
 *
 * @author Viktor
 */
public class ContFeriado extends ControladorGeneral{

    public ContFeriado() {
    }
    
    public List<Feriado> feriados(int anio){
        List<Feriado> lista = new ArrayList<>();
        Object[] datos = new Object[]{"%"+anio+"%"};
        
        List<List> listaL = buscarPorParametro(BDSentencias.feriadoColumnas, BDSentencias.consultaFeriados, datos, BDSentencias.conexionRelojLocal);
        
        for (List list : listaL) {
            Feriado f = new Feriado(
                    Integer.parseInt(list.get(0).toString()),
                    Date.valueOf(list.get(1).toString()),
                    list.get(2).toString());
            
            lista.add(f);
        }
        
        return lista;
    }

    public int create(Feriado f) {
        Object[] datos =  {f.getFecha(), f.getDetalle()};
        return ejecutarSentencia(datos, BDSentencias.feriadoInsert, BDSentencias.conexionRelojLocal);
    }

    public List<Feriado> feriados(int anio, int mes) {
        List<Feriado> lista = new ArrayList<>();
        String anioString = "%"+anio+"%";
        String mesString = "";
        if(mes < 10){
            mesString = "%-0"+mes+"-%";
        }else{
            mesString = "%-"+mes+"-%";
        }
        Object[] datos = new Object[]{anioString,mesString};
        
        List<List> listaL = buscarPorParametro(BDSentencias.feriadoColumnas, BDSentencias.consultaFeriadosMes, datos, BDSentencias.conexionRelojLocal);
        
        for (List list : listaL) {
            Feriado f = new Feriado(
                    Integer.parseInt(list.get(0).toString()),
                    Date.valueOf(list.get(1).toString()),
                    list.get(2).toString());
            
            lista.add(f);
        }
        
        return lista;
    }

    public boolean isFeriado(String fecha) {
        boolean resul = false;
        
        Object[] datos ={fecha};
        List<List> feriados = buscarPorParametro(BDSentencias.feriadoColumnas, BDSentencias.consultaFeriado, datos, conexionRelojLocal);
        for (List list : feriados) {
            resul = true;
        }
        
        
        return resul;
    }

    List<Feriado> findAll() {
        List<Feriado> lista = new ArrayList<>();
        
        List<List> listaL = dameListaCompleta(BDSentencias.feriadoColumnas, BDSentencias.consultaFeriadosTodos, BDSentencias.conexionRelojLocal);
        
        for (List list : listaL) {
            Feriado f = new Feriado(
                    Integer.parseInt(list.get(0).toString()),
                    Date.valueOf(list.get(1).toString()),
                    list.get(2).toString());
            
            lista.add(f);
        }
        
        return lista;
    }
}
