/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import controlador.fichadas.Utilidades;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.Feriado;
import modelo.Fichada;
import modelo.Grupo;
import modelo.LicenciaEmpleado;
import modelo.Userinfo;
import vista.V_principal;
import vista.V_ver_fichada;

/**
 *
 * @author Viktor
 */
public class ContFichada extends ControladorGeneral {

    Utilidades u = new Utilidades();
    List<Feriado> listaFeriado = new ArrayList<>();
    ContPersonaGrupo contPersonaGrupo = new ContPersonaGrupo();
    ContGrupo contGrupo = new ContGrupo();
    ContUserinfo contUserinfo = new ContUserinfo();
    ContFeriado contFeriado;

    public ContFichada() {
        super();
        contFeriado = new ContFeriado();
        listaFeriado = contFeriado.findAll();
    }

    public Fichada setearObjeto(List lista) {
        Fichada fichada = null;
        if (lista.get(5) == null) {
            fichada = new Fichada(
                    Integer.parseInt(lista.get(0).toString()),
                    Date.valueOf(lista.get(1).toString()),
                    Boolean.valueOf(lista.get(2).toString()),
                    Boolean.valueOf(lista.get(3).toString()),
                    new Time(((Time) lista.get(4)).getTime()),
                    "",
                    lista.get(6).toString()
            );
        } else {
            fichada = new Fichada(
                    Integer.parseInt(lista.get(0).toString()),
                    Date.valueOf(lista.get(1).toString()),
                    Boolean.valueOf(lista.get(2).toString()),
                    Boolean.valueOf(lista.get(3).toString()),
                    new Time(((Time) lista.get(4)).getTime()),
                    lista.get(5).toString(),
                    ""
            );
        }

        return fichada;
    }

    public String licencia(Object dia, List<LicenciaEmpleado> licenciasEmploye) {
        String licencia = "";

        Date fecha = Date.valueOf(dia.toString());
//        System.err.println("Fecha evaludada: "+fecha);

        for (LicenciaEmpleado l : licenciasEmploye) {
            if (l.getFecha_inicio().getTime() <= fecha.getTime()
                    && l.getFecha_fin().getTime() >= fecha.getTime()) {
                licencia = l.getDetalle();
                break;
            }
        }

//        System.err.println("DEVUELVE: "+licencia);
        return licencia;
    }

    public int insertarEnTablaAuditoria(Fichada fichada) {
        Object[] datos = {fichada.getUserinfoid(), fichada.getFecha(), true, fichada.getHora(), fichada.getObservacion(), fichada.getTerminal(), fichada.getIp(), fichada.getUsuario(), fichada.getFechasubido()};
        return ejecutarSentencia(datos, BDSentencias.fichadaPorSistemaINSERT, BDSentencias.conexionRelojLocal);
    }

    public int insertar(Fichada fichada) {

        Object[] datos = {fichada.getUserinfoid(), fichada.getFecha(), true, fichada.getHora(), fichada.getObservacion(), fichada.getTerminal(), fichada.getIp(), fichada.getUsuario(), fichada.getFechasubido()};
        return ejecutarSentencia(datos, BDSentencias.fichadaPorSistemaINSERT, BDSentencias.conexionRelojLocal);

    }

    public int update(Fichada fichadaFila) {
        Object[] datos = {fichadaFila.getObservacion(), fichadaFila.getUserinfoid(),fichadaFila.getFecha(), fichadaFila.getHora()};
        return ejecutarSentencia(datos, BDSentencias.fichadaDetalleUPDATE, BDSentencias.conexionRelojLocal);
    }

    public int delete(Fichada f) {
        Object[] datos = {f.isEstado(), f.getUserinfoid(), f.getFecha(), f.getHora()};
        return ejecutarSentencia(datos, BDSentencias.fichadaPorSistemaUPDATE, BDSentencias.conexionRelojLocal);
    }

    public List<Fichada> mesTipo(int ano, int mes, String cuil) {

        List<Fichada> listaFichadas = new ArrayList<>();
        try {

            List<String> fechas = u.fechas(ano, mes);

            for (String fech : fechas) {
                Object[] parametros = {cuil, fech + "%"};
                for (List lista : buscarPorParametro(BDSentencias.fichadaColumnas, BDSentencias.fichadaMes3, parametros, BDSentencias.conexionRelojLocal)) {

                    Fichada f = setearObjeto(lista);
                    listaFichadas.add(f);
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(V_ver_fichada.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listaFichadas;
    }
    //ESTO SERA USADO PARA TENER EXACTAMENTE IGUAL LAS FICHADAS CON LO MOSTRADO EN LA TABLA
    List<List<Fichada>> listaLF;

    public void setListaLF(List<List<Fichada>> listaLF) {
        this.listaLF = listaLF;
    }

    public List<List<Fichada>> getListLF() {
        return this.listaLF;
    }

    //CLON ********************************************************************************************************************************************************************/
    public int totalesSERENOS(Object[] vector, List<Fichada> fichadaDia, int userinfoid, Time hEntrada, Time hSalida, Grupo grupo, List<LicenciaEmpleado> licenciasEmploye) {
///////////////////////////////////////////////////////////////////////
        int cont = 0;
        int contTardanza = 0;
        for (int j = 0; j < vector.length - 3; j++) {
            if (vector[j + 1] != null) {
                cont++;
            }
        }

        String fechaInicial = "";
        long primeraMarcada = 0;
        long segundaMarcada = 0;
        long day = 0;
        int marca = 0;

        if (((cont % 2) == 0 && cont != 0)) {                     //entonces es par

//
            for (int k = 0; k < fichadaDia.size(); k++) {
                String fechaAux = fichadaDia.get(k).getFecha().toString();

                if (fechaInicial.equals(fechaAux)) {

                    if (marca == 1) {
                        segundaMarcada = fichadaDia.get(k).getHora().getTime();
                        day = 86400000 - (segundaMarcada - primeraMarcada);

                        vector[7] = (day + 20000) / 3600000 + ":" + day % 3600000 / 60000 + ":0";//calculo normal
                        day = 0;

                        marca = 0;
                    } else {
                        //MARCADA DE ENTRADA SUPUESTA
                        primeraMarcada = fichadaDia.get(k).getHora().getTime();
                        marca = 1;

                    }

                } else {
                    //PRIMERA MARCADA DE CADA DIA
                    fechaInicial = fechaAux;
                    primeraMarcada = fichadaDia.get(k).getHora().getTime();
                    marca = 1;
                }

            }
//            }
        } else {
            if (cont == 0) {
                String licenciaString = licencia(vector[0], licenciasEmploye);
                if (!licenciaString.equals("")) {
                    vector[7] = licenciaString;
//                    vector[8] = "Licencia";
                } else if (contFeriado.isFeriado(vector[0].toString())) {
                    vector[7] = "Dia no laborable";
                    vector[8] = "Dia no laborable";
                } else {
                    vector[7] = null;
                    vector[8] = null;
                }
            } else {
                vector[7] = "Inconcluso";
                vector[8] = "Inconcluso";
            }
            //no se puede calcular

        }

        return contTardanza;

    }
///////////////////////////////////////////////////////////////////////////////////////////

    public List<Object[]> retornarFilaDia(List<Fichada> listaFichada, List<String> fechas, int userinfoid,
            boolean radiografia) {
        List<Object[]> listaFichadaDia = new ArrayList<>();
        int idGrupo = this.contPersonaGrupo.getIdGrupo(userinfoid);
        List grupoRs = buscarPorId(BDSentencias.grupoColumnas, BDSentencias.grupoPorID, idGrupo, BDSentencias.conexionRelojLocal);

        List<LicenciaEmpleado> licenciasEmploye = new ContLicenciaEmpleado().buscar(userinfoid);

        Grupo grupo = this.contGrupo.setearEnObjeto(grupoRs);

        int contTardanza = 0;

        for (String dia : fechas) {
            List<Fichada> fichadaDia = new ArrayList<>();
            Object[] vector = new Object[10];

            vector[0] = dia;

            int i = 1;
            Date fechatmp = new Date(Calendar.getInstance().getTimeInMillis());
            Time horatmp = new Time(Calendar.getInstance().getTimeInMillis());
            //HORAS DE ENTRADA Y SALIDA
            for (Fichada f : listaFichada) {
                if (radiografia) {
                    if (!f.isFichadaPorSistema()) {
                        if (dia.equals(u.traerFecha(f.getFecha()))) {
                            if (f.getFecha().getTime() != fechatmp.getTime() || f.getHora().getTime() != horatmp.getTime()) {
                                fichadaDia.add(f);
                            }
                        }
                    }
                } else {
                    if (dia.equals(u.traerFecha(f.getFecha()))) {
                        if (f.getFecha().getTime() != fechatmp.getTime() || f.getHora().getTime() != horatmp.getTime()) {
                            fichadaDia.add(f);
                        }
                    }
                }

                fechatmp = f.getFecha();
                horatmp = f.getHora();

            }

            for (Fichada f : fichadaDia) {
                if (radiografia) {
                    if (!f.isFichadaPorSistema()) {
                        if (i < 7) {
                            vector[i] = u.traerHora(f.getHora());
                            i++;
                        }
                    }
                } else {
                    if (i < 7) {
                        vector[i] = u.traerHora(f.getHora());
                        i++;
                    }
                }
            }

            //ORDENAMOS EL VECTOR
//            ordenar(vector, fichadaDia, grupo.getEntrada(), grupo.getSalida());
            //TOTALES
            if (!radiografia) {
                if (grupo.getEntrada().getTime() > grupo.getSalida().getTime()) {
//                    System.out.println("ENTRO ES SERENO NOCTURNO");
                    contTardanza = contTardanza + totalesSERENOS(vector, fichadaDia, userinfoid, grupo.getEntrada(), grupo.getSalida(), grupo, licenciasEmploye);
                } else {
                    contTardanza = contTardanza + totales(vector, fichadaDia, userinfoid, grupo.getEntrada(), grupo.getSalida(), grupo, licenciasEmploye);
                }
            }
            //RETRAZO
//            retrazo(vector, fichadaDia, grupo.getEntrada());
            //AÑADIMOS LA NUEVA FILA
            if (listaLF != null) {
                listaLF.add(fichadaDia);
            }

            listaFichadaDia.add(vector);

        }
        System.out.println("TARDANZAS > 15min: " + contTardanza);
        return listaFichadaDia;
    }

    private Object[] ordenar(Object[] vector, List<Fichada> fichadaDia, Time horaEntrada, Time horaSalida) {
        /////////////////////////////////////////////////////////////////////// 
        Time h;
        Object[] vectorAUX = vector;
        List<Fichada> fichadaDiaAUX = fichadaDia;

        for (int j = 1; j < vector.length - 3; j++) {//empezamos desde la primer hora de entrada

            if (vector[j] != null) {
                h = Time.valueOf(vector[1].toString());

                if ((horaEntrada.getTime() - Long.parseLong("7200000")) > h.getTime()) {//10800000 son 3 hs en miliseg.
                    Object o = vector[1];

                    for (int i = 1; i < vector.length - 3; i++) { //corremos todas las celdas una pocsicion para adelante
                        vectorAUX[i] = vectorAUX[i + 1];
                    }

                    //colocamos  "o" en la ultima celda de marcadas
                    boolean b = true; //para saber si no  ha sido insertado
                    for (int i = 1; i < vector.length - 3; i++) {
                        if (b && vectorAUX[i] == null) {
                            vectorAUX[i] = o;
                            b = false;
                        }
                    }

                }
            }
        }

        // lista
        for (int j = 0; j < fichadaDia.size(); j++) {

            String horaString = fichadaDia.get(j).getHora().toString();

            h = Time.valueOf(horaString.split(":")[0] + ":" + horaString.split(":")[1] + ":00");

            if ((horaEntrada.getTime() - Long.parseLong("7200000")) > h.getTime()) {
                //LIST DE FICHADAS

                Fichada f = fichadaDia.get(0);

                for (int i = 0; i < fichadaDia.size() - 1; i++) {
                    fichadaDiaAUX.set(i, fichadaDiaAUX.get(i + 1));
                    fichadaDiaAUX.set(i + 1, null);
                }

                boolean b2 = true; //para saber si no  ha sido insertado
                for (int i = 0; i < fichadaDia.size(); i++) {
                    if (b2 && fichadaDia.get(i) == null) {
                        fichadaDiaAUX.set(i, f);
                        b2 = false;
                    }
                }
            }
        }
        fichadaDia = fichadaDiaAUX;
//        System.out.println("h");

        return vectorAUX;
    }

    public int totales(Object[] vector, List<Fichada> fichadaDia, int userinfoid, Time hEntrada, Time hSalida, Grupo grupo, List<LicenciaEmpleado> licenciasEmploye) {
///////////////////////////////////////////////////////////////////////
        int cont = 0;
        int contTardanza = 0;
        for (int j = 0; j < vector.length - 3; j++) {
            if (vector[j + 1] != null) {
                cont++;
            }
        }

        String fechaInicial = "";
        long primeraMarcada = 0;
        long segundaMarcada = 0;
        long day = 0;
        long dayAux = 0;//se usara cuando haya solo una marcada donde se entre en la mañaa y se salfa la tarde ej: 7:30 a 18:30
        int marca = 0;
        boolean isJornadaLarga = false;
        boolean bandera = false;//identifica si es la primer marcada para hacer las hs normales

        if (((cont % 2) == 0 && cont != 0)) {                       //entonces es par

            //calculo de totales normales y extras
            bandera = false;
            isJornadaLarga = false;
//
            for (int k = 0; k < fichadaDia.size(); k++) {
                String fechaAux = fichadaDia.get(k).getFecha().toString();

                if (fechaInicial.equals(fechaAux)) {

                    if (marca == 1) {
                        segundaMarcada = fichadaDia.get(k).getHora().getTime();
                        if (!bandera && ((primeraMarcada < hSalida.getTime() && segundaMarcada > hSalida.getTime())
                                || (primeraMarcada < hSalida.getTime() && segundaMarcada < hSalida.getTime()))
                                && !(grupo.getEntrada().getTime() > grupo.getSalida().getTime())) {//es la se calculan el primer par de horas (hs normales)

                            if (u.menorQ(hEntrada.getTime(), primeraMarcada) && u.menorQ(hEntrada.getTime(), segundaMarcada)) {//llega temprano se va temprano antes de ala hora de entrada
                                day = segundaMarcada - primeraMarcada;
                            } else if (u.menorQ(hEntrada.getTime(), primeraMarcada) && !u.menorQ(hSalida.getTime(), segundaMarcada)) { // llega temprano, se va tarde
                                day = segundaMarcada - hEntrada.getTime();
                            } else if (u.menorQ(hEntrada.getTime(), primeraMarcada) && u.menorQ(hSalida.getTime(), segundaMarcada)) {//llega temprano se va temprano
                                day = segundaMarcada - hEntrada.getTime();
                            } else if (!u.menorQ(hEntrada.getTime(), primeraMarcada) && !u.menorQ(hSalida.getTime(), segundaMarcada)) {//llega tarde se va tarde
                                day = segundaMarcada - primeraMarcada;
                            } else if (!u.menorQ(hEntrada.getTime(), primeraMarcada) && u.menorQ(hSalida.getTime(), segundaMarcada)) {//llega tarde se va temprano
                                day = segundaMarcada - primeraMarcada; //TODOS ENTRAN AQUI!
                            }

                            if (day > 21600000 /*(hSalida.getTime() - hEntrada.getTime())*/ && //si el dia trabajado es mayor a 6 hs
                                    !u.menorQ(hEntrada.getTime(), primeraMarcada)
                                    && !u.menorQ(hSalida.getTime(), segundaMarcada)) { //y si ademas ha llegado tarde y se fue MUY tarde
//                                System.err.println("aaaa1");
                                if (day > (/*(hSalida.getTime() - hEntrada.getTime())*/21600000 + 3600000)) {
//                                    System.err.println("aaaa1.1");
                                    dayAux = day - (3600000 + (hSalida.getTime() - primeraMarcada));
                                    day = hSalida.getTime() - primeraMarcada;
                                    isJornadaLarga = true;
                                } else {
//                                    System.err.println("aaaa1.2");
                                    day = 21600000/*hSalida.getTime() - hEntrada.getTime()*/;
                                }
                            } else if (day > 21600000/*hSalida.getTime() - hEntrada.getTime()*/) {//joranada larga comun
//                                System.err.println("aaaa2");
                                if (day > (21600000/*(hSalida.getTime() - hEntrada.getTime())*/ + 3600000)) {
//                                    System.err.println("aaaa2.1");
                                    dayAux = day - (3600000 + (21600000/*hSalida.getTime() - hEntrada.getTime()*/));
                                    day = 21600000/*hSalida.getTime() - hEntrada.getTime()*/;
                                    isJornadaLarga = true;
                                } else {
//                                    System.err.println("aaaa2.2");
                                    day = 21600000/*hSalida.getTime() - hEntrada.getTime()*/;
                                }
                            } else if (day < (21600000/*hSalida.getTime() - hEntrada.getTime()*/) //llega tarde se va tarde pero no completa 6hs
                                    && !u.menorQ(hEntrada.getTime(), primeraMarcada)
                                    && !u.menorQ(hSalida.getTime(), segundaMarcada)) {
                                if (primeraMarcada < (hEntrada.getTime() + 900000)) {//llega antes de 15 min tarde se toman 6hs
                                    day = 21600000/*hSalida.getTime() - hEntrada.getTime()*/;
                                } else {
                                    day = hSalida.getTime() - primeraMarcada;
                                }
                            }
                        } else {
                            day = day + (segundaMarcada - primeraMarcada);
                        }

                        if (!bandera) {//se asigna la cant de hs normales
                            bandera = true;
                            if (grupo.getEntrada().getTime() > grupo.getSalida().getTime()) {
//                                System.out.println("c --> "+vector[1]+"-"+vector[2]);
                                vector[7] = ((86400000 - (day + 20000)) / 3600000) + ":" + (3600000 - (day % 3600000)) / 60001 + ":0";// calculo para los serenos
                            } else {
                                if (isJornadaLarga) {
                                    vector[7] = (day + 20000) / 3600000 + ":" + day % 3600000 / 60000 + ":0";//calculo normal
//                                    System.out.println("1: "+ Time.valueOf(grupo.getSalida().toString())
//                                            +" 2: "+Time.valueOf(vector[1].toString()));
//                                    vector[7] = 
//                                            Time.valueOf(grupo.getSalida().toString()).getTime()/ 3600000
//                                            + Time.valueOf(vector[1].toString()).getTime() % 3600000 / 60000 + ":0";
                                    vector[8] = (dayAux) / 3600000 + ":" + dayAux % 3600000 / 60000 + ":0";
                                } else {
                                    vector[7] = (day + 20000) / 3600000 + ":" + day % 3600000 / 60000 + ":0";//calculo normal
                                }
                            }
                            day = 0;

                        } else if (bandera) {
                            if (isJornadaLarga) {
                                day = day + dayAux;
                                isJornadaLarga = false;
                            }
                            vector[8] = (day) / 3600000 + ":" + day % 3600000 / 60000 + ":0";

                        }

                        marca = 0;
                    } else {
                        //MARCADA DE ENTRADA SUPUESTA
                        primeraMarcada = fichadaDia.get(k).getHora().getTime();
                        marca = 1;

                    }

                } else {
                    //PRIMERA MARCADA DE CADA DIA
                    fechaInicial = fechaAux;
                    primeraMarcada = fichadaDia.get(k).getHora().getTime();
//                        System.out.println("primera marcada: "+new Time(primeraMarcada));

                    //--------RETRAZO
                    Long h2 = grupo.getEntrada().getTime();

                    Long dia = primeraMarcada - h2; //se resta la hora de entrada la hora de entrada real

                    if ((dia + 900000) / 3600000 < 0 || dia % 3600000 / 60000 < 0) { //comparamos si hay tardanza
                        vector[9] = "00:00";
                    } else {
//                System.out.println("horas : " + (dia + 20000) / 3600000 + " : " + dia % 3600000 / 60000);

                        if ((fichadaDia.size() % 2) == 0 && fichadaDia.size() > 0) {
                            if (((dia + 900000) / 3600000 > 0) || (dia % 3600000 / 60000 > 15)) {
//                                vector[9] = (dia + 900000) / 3600000 + ":" + dia % 3600000 / 60000 + ":0";
                                vector[9] = "00:00";
                                contTardanza = contTardanza + 1;
                            } else {
//                                vector[9] = "00:00";
                                vector[9] = (dia + 900000) / 3600000 + ":" + dia % 3600000 / 60000 + ":0";
                            }
                        } else {
                            vector[9] = "Inconcluso";
                        }
                    }

                    //FIN DE RETRAZO
                    marca = 1;
                }

            }
//            }
        } else {
            if (cont == 0) {
                String licenciaString = licencia(vector[0], licenciasEmploye);
                if (!licenciaString.equals("")) {
                    vector[7] = licenciaString;
//                    vector[8] = "Licencia";
                } else if (contFeriado.isFeriado(vector[0].toString())) {
                    vector[7] = "Dia no laborable";
                    vector[8] = "Dia no laborable";
                } else {
                    vector[7] = null;
                    vector[8] = null;
                }
            } else {
                vector[7] = "Inconcluso";
                vector[8] = "Inconcluso";
            }
            //no se puede calcular

        }

        return contTardanza;

    }

//    public void retrazo(Object[] vector, List<Fichada> fichadaDia, Time t) {
//        if (t != null && vector[1] != null) {//si hay almenos una marcada, la priera se tomara como hora de entrada
//            Time h = Time.valueOf(vector[1].toString());
//            Long h1 = t.getTime();
//            Long h2 = h.getTime();
//
//            Long dia = h2 - h1;
//
//            if ((dia + 20000) / 3600000 < 0 || dia % 3600000 / 60000 < 0) {
//                vector[9] = "00:00";
//            } else {
////                System.out.println("horas : " + (dia + 20000) / 3600000 + " : " + dia % 3600000 / 60000);
//
//                if ((fichadaDia.size() % 2) == 0 && fichadaDia.size() > 0) {
//                    vector[9] = (dia + 20000) / 3600000 + ":" + dia % 3600000 / 60000 + ":0";
//                } else {
//                    vector[9] = "Inconcluso";
//                }
//            }
//        }
//    }
    //trae el dia de la semana 1 = doingo, 7 = sabado, 8 = feriado
    public int diaDeSemana(String fecha) {
        int dia = 0;
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(Timestamp.valueOf(fecha + " 00:00:00").getTime());

        dia = c.get(Calendar.DAY_OF_WEEK);

//        if (contFeriado.isFeriado(fecha)) {
//            dia = 8;
//        }
        for (Feriado f : listaFeriado) {
            if (f.getFecha().toString().equals(fecha)) {
                dia = 8;
            }
        }

        return dia;
    }

    public String[] resumenHoras(List<Object[]> listaFichadaDia) {

        String rNormal = "00:00:00";
        String rExeso = "00:00:00";
        String rRetrazo = "00:00:00";

        String rSabadoMañama = "00:00:00";
        String rSabadoTarde = "00:00:00";
        String rDomingo = "00:00:00";
        String rFeriado = "00:00:00";

        for (Object[] hNormal : listaFichadaDia) {
            int dia = diaDeSemana(hNormal[0].toString());
            if (dia != 1 && dia != 7 && dia != 8) {
                if (hNormal[7] != null && !hNormal[7].toString().equals("Inconcluso") && !hNormal[7].toString().equals("Dia no laborable")) {
                    rNormal = u.sumar(rNormal, hNormal[7].toString());
                }
                if (hNormal[8] != null && !hNormal[8].toString().equals("Inconcluso") && !hNormal[7].toString().equals("Dia no laborable")) {
                    rExeso = u.sumar(rExeso, hNormal[8].toString());
                }
                if (hNormal[9] != null && !hNormal[9].toString().equals("Inconcluso") && !hNormal[7].toString().equals("Dia no laborable")) {
                    rRetrazo = u.sumar(rRetrazo, hNormal[9].toString());
                }
            } else if (dia == 1) {
                if (hNormal[7] != null && !hNormal[7].toString().equals("Inconcluso") && !hNormal[7].toString().equals("Dia no laborable")) {
                    rDomingo = u.sumar(rDomingo, hNormal[7].toString());
                }
                if (hNormal[8] != null && !hNormal[8].toString().equals("Inconcluso") && !hNormal[7].toString().equals("Dia no laborable")) {
                    rDomingo = u.sumar(rDomingo, hNormal[8].toString());
                }
            } else if (dia == 7) {
                if (hNormal[7] != null && !hNormal[7].toString().equals("Inconcluso") && !hNormal[7].toString().equals("Dia no laborable")) {
                    rSabadoMañama = u.sumar(rSabadoMañama, hNormal[7].toString());
                }
                if (hNormal[8] != null && !hNormal[8].toString().equals("Inconcluso") && !hNormal[7].toString().equals("Dia no laborable")) {
                    rSabadoTarde = u.sumar(rSabadoTarde, hNormal[8].toString());
                }
            } else if (dia == 8) {
                if (hNormal[7] != null && !hNormal[7].toString().equals("Inconcluso") && !hNormal[7].toString().equals("Dia no laborable")) {
                    rFeriado = u.sumar(rFeriado, hNormal[7].toString());
                }
                if (hNormal[8] != null && !hNormal[8].toString().equals("Inconcluso") && !hNormal[7].toString().equals("Dia no laborable")) {
                    rFeriado = u.sumar(rFeriado, hNormal[8].toString());
                }
            }
        }

        String[] vectorResumen = {rNormal, rExeso, rRetrazo, rSabadoMañama, rSabadoTarde, rDomingo, rFeriado};

        return vectorResumen;
    }

    public String[] resumenHorasReporte(List<Object[]> listaFichadaDia) {

        String rNormal = "00:00:00";
        String rExeso = "00:00:00";
        String rRetrazo = "00:00:00";
        int contInconculosos = 0;
        int contLicencia = 0;

        String rSabadoMañama = "00:00:00";
        String rSabadoTarde = "00:00:00";
        String rDomingo = "00:00:00";
        String rFeriado = "00:00:00";

        for (Object[] hNormal : listaFichadaDia) {

            int dia = diaDeSemana(hNormal[0].toString());
            if (dia != 1 && dia != 7 && dia != 8) {
                if (hNormal[7] != null && !hNormal[7].toString().equals("Inconcluso") && !hNormal[7].toString().equals("Licencia") && !hNormal[7].toString().equals("Dia no laborable")) {
                    rNormal = u.sumar(rNormal, hNormal[7].toString());
                }
                if (hNormal[8] != null && !hNormal[8].toString().equals("Inconcluso") && !hNormal[8].toString().equals("Licencia") && !hNormal[7].toString().equals("Dia no laborable")) {
                    rExeso = u.sumar(rExeso, hNormal[8].toString());
                }
                if (hNormal[9] != null && !hNormal[9].toString().equals("Inconcluso") && !hNormal[9].toString().equals("Licencia") && !hNormal[7].toString().equals("Dia no laborable")) {
                    rRetrazo = u.sumar(rRetrazo, hNormal[9].toString());
                }
                if (hNormal[7] != null && hNormal[7].toString().equals("Inconcluso")) {
                    contInconculosos++;
                }
                if (hNormal[7] != null && hNormal[7].toString().equals("Licencia")) {
                    contLicencia++;
                }
            } else if (dia == 1) {
                if (hNormal[7] != null && !hNormal[7].toString().equals("Inconcluso") && !hNormal[7].toString().equals("Licencia") && !hNormal[7].toString().equals("Dia no laborable")) {
                    rDomingo = u.sumar(rDomingo, hNormal[7].toString());
                }
                if (hNormal[8] != null && !hNormal[8].toString().equals("Inconcluso") && !hNormal[8].toString().equals("Licencia") && !hNormal[7].toString().equals("Dia no laborable")) {
                    rDomingo = u.sumar(rDomingo, hNormal[8].toString());
                }
            } else if (dia == 7) {
                if (hNormal[7] != null && !hNormal[7].toString().equals("Inconcluso") && !hNormal[7].toString().equals("Licencia") && !hNormal[7].toString().equals("Dia no laborable")) {
                    rSabadoMañama = u.sumar(rSabadoMañama, hNormal[7].toString());
                }
                if (hNormal[8] != null && !hNormal[8].toString().equals("Inconcluso") && !hNormal[8].toString().equals("Licencia") && !hNormal[7].toString().equals("Dia no laborable")) {
                    rSabadoTarde = u.sumar(rSabadoTarde, hNormal[8].toString());
                }
            } else if (dia == 8) {
                if (hNormal[7] != null && !hNormal[7].toString().equals("Inconcluso") && !hNormal[7].toString().equals("Licencia") && !hNormal[7].toString().equals("Dia no laborable")) {
                    rFeriado = u.sumar(rFeriado, hNormal[7].toString());
                }
                if (hNormal[8] != null && !hNormal[8].toString().equals("Inconcluso") && !hNormal[8].toString().equals("Licencia") && !hNormal[7].toString().equals("Dia no laborable")) {
                    rFeriado = u.sumar(rFeriado, hNormal[8].toString());
                }
            }
        }

        String[] vectorResumen = {rNormal, rExeso, rRetrazo, contInconculosos + "", contLicencia + "", rSabadoMañama, rSabadoTarde, rDomingo, rFeriado};

        return vectorResumen;
    }

    public List<List<Fichada>> ordenarListaFichada(List<Fichada> fichadaDia, int userinfoid) {
        ContPersonaGrupo contPersonaGrupo = new ContPersonaGrupo();
        Time horaEntrada = contPersonaGrupo.getHoraEntrada(contPersonaGrupo.getIdGrupo(userinfoid)); //treae la hora en la que debe de entrar el empleado dependeiendo de su cuil
        Time horaSalida = contPersonaGrupo.getHoraSalida(contPersonaGrupo.getIdGrupo(userinfoid));
        ///////////////////////////////////////////////////////////////////////

        // lista
        List<Fichada> fichadaDiaAUX = fichadaDia;

        for (int j = 0; j < fichadaDia.size(); j++) {

            String horaString = fichadaDia.get(j).getHora().toString();

            Time h = Time.valueOf(horaString.split(":")[0] + ":" + horaString.split(":")[1] + ":00");

            if ((horaEntrada.getTime() - Long.parseLong("7200000")) > h.getTime()) {
                //LIST DE FICHADAS

                Fichada f = fichadaDia.get(0);

                for (int i = 0; i < fichadaDia.size() - 1; i++) {
                    fichadaDiaAUX.set(i, fichadaDiaAUX.get(i + 1));
                    fichadaDiaAUX.set(i + 1, null);
                }

                boolean b2 = true; //para saber si no  ha sido insertado
                for (int i = 0; i < fichadaDia.size(); i++) {
                    if (b2 && fichadaDia.get(i) == null) {
                        fichadaDiaAUX.set(i, f);
                        b2 = false;
                    }
                }
            }
        }

        fichadaDia = fichadaDiaAUX;
//        System.out.println("h");
        return null;
    }

    public boolean esMarcadaPorSistema(Date fecha, Userinfo empleado, Time hora) {
        boolean b = false;

        Object[] parametros = {fecha, empleado.getId(), hora};

        for (List l : buscarPorParametro(BDSentencias.fichadaColumnas, BDSentencias.consultaFichadaXsistema, parametros, BDSentencias.conexionRelojLocal)) {

            b = Boolean.valueOf(l.get(3).toString());

        }

        return b;
    }

    private void exportAExcel(List<Fichada> lista) {
        Calendar c = Calendar.getInstance();
        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            fichero = new FileWriter("D:/prueba-duplicado"+c.get(Calendar.YEAR)+c.get(Calendar.MONTH)+c.get(Calendar.DAY_OF_MONTH)+c.get(Calendar.HOUR_OF_DAY)+c.get(Calendar.MINUTE)+c.get(Calendar.SECOND)+".csv");
            pw = new PrintWriter(fichero);

            for (Fichada f : lista) {
                pw.println(
                        f.getUserinfoid() + ";"
                        + f.getFecha() + ";"
                        + f.getHora() + ";"
                        + f.getReloj() + ";"
                        + f.getFechasubido() + ";"
                        + f.getTerminal() + ";"
                        + f.getIp() + ";"
                        + f.getObservacion() + ";"
                        + f.getUsuario());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Nuevamente aprovechamos el finally para 
                // asegurarnos que se cierra el fichero.
                if (null != fichero) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void ficharAloLoco(List<Fichada> fichadas, String[] datosConexion, File acces, int total, List<Userinfo> user_fail) {
        //exportAExcel(fichadas); 
        //System.err.println("FICHERO 2 CREADO!");
        

        int contadorFallidos = 0;
        int contadorTotal = 0;
        float porcentaje = 0;

        List<Fichada> fichadaServer = this.ultimoMes();
        List<Fichada> fichadasInsertar = new ArrayList<Fichada>();
        int contador = 0;
        for (Fichada f : fichadas) {
                Object[] datos = {
                    f.getUserinfoid(),
                    f.getFecha(),
                    f.getHora(),
                    f.getTerminal(), 
                    f.getIp(), 
                    f.getUsuario()
                };
                if(!this.existeEnBD(fichadaServer,f)){
                    fichadasInsertar.add(f);
                }
        }
        
        
        try {
//            conexion = bd.estableceConexion(datosConexion);
            conexion = bd.estableceConexion(conexion,datosConexion);
            PreparedStatement pe = conexion.prepareStatement(
                    "INSERT IGNORE INTO fichada (USERINFOID,FECHA,HORA,TERMINAL,IP,USUARIOSISTEMA,FECHASUBIDO) VALUES (?,?,?,?,?,?,CURRENT_TIMESTAMP())"
            );
            for(Fichada f: fichadasInsertar){
                //f.getUserinfoid(), f.getFecha(), f.getHora(), f.getReloj(), f.getTerminal(), f.getIp(), f.getUsuario()
                pe.setObject(1, f.getUserinfoid());
                pe.setObject(2, f.getFecha());
                pe.setObject(3, f.getHora());
                pe.setObject(4, f.getTerminal());
                pe.setObject(5, f.getIp());
                pe.setObject(6, f.getUsuario());
                pe.addBatch();
        
                contadorTotal++;
                porcentaje = (contadorTotal * 100) / fichadasInsertar.size();
                V_principal.ingremetarLabel(contadorTotal+" de "+fichadasInsertar.size());
                V_principal.incrementoDeBarra(porcentaje);

                System.out.println("CONTADOR: "+contador);
                contador++;
            }
            pe.executeBatch();
            
        } catch (Exception e) {
            System.out.println("Problemas al ejecutarSentencia " + e.getMessage());
            e.printStackTrace();
        } finally {
            bd.cierraConexion(conexion);
        }
        this.updateAccess(acces, user_fail);
        if (contadorFallidos > 0) {
            JOptionPane.showMessageDialog(null, "No han podido subir " + contadorFallidos + " registros.\n"
                    + "Se recomienda volver a subir los fichadas.", "", JOptionPane.ERROR_MESSAGE);
        }
    }
        
    private void updateAccess(File acces, List<Userinfo> user_fail){
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_YEAR, -30);//regrocede una semana
        int anio = c.get(Calendar.YEAR);
        int mes = c.get(Calendar.MONTH);
        int dia = c.get(Calendar.DAY_OF_MONTH);
        
        String exString = "";
        for(Userinfo u: user_fail){
            exString += " AND a.USERID <> (SELECT  b.userid from userinfo as b where b.badgenumber = '"+u.getUserid()+"')";
        }
        Conectate conexion = null;
        PreparedStatement psmt = null;

        conexion = new Conectate(acces.getAbsolutePath());

        Connection conn = conexion.getConnection();
        try {
            System.out.println("UPDATE CHECKINOUT a  SET a.subidoBD = true WHERE a.USERID > 0 "+exString+" AND a.CHECKTIME >= #" + (anio) + "-" + (mes < 10 ? "0"+mes : mes) + "-"+(dia < 10 ? "0"+dia : dia)+" 00:00:00#;");
            psmt = conn.prepareStatement("UPDATE CHECKINOUT a  SET a.subidoBD = true WHERE a.USERID > 0 "+exString+" AND a.CHECKTIME >= #" + (anio) + "-" + (mes < 10 ? "0"+mes : mes) + "-"+(dia < 10 ? "0"+dia : dia)+" 00:00:00#;");
            psmt.executeUpdate();
            psmt.close();
            //tiladarDestildar(f, acces, true);
            System.err.println("tildo todo");
        } catch (SQLException ex) {
            System.out.println("problemas checar fichada en ACCES " + ex.getMessage());

            ex.printStackTrace();
        }
        finally{
            conexion.desconectar("");
        }
    };
     
    private List<Fichada> ultimoMes(){
        
        Calendar c = Calendar.getInstance();
        int anio = c.get(Calendar.YEAR);//toma el anio actual 
        int mes = c.get(Calendar.MONTH)+1;
        
        if(mes == 1){
            mes = 12;
            anio = anio -1;
        }else{
            mes = mes -1;
        }
        
        String param = anio+"-"+mes+"-01";
        Object [] parametros = {param};
        List<Fichada> marcadas = new ArrayList<Fichada>();
        List<List> lista = buscarPorParametro(BDSentencias.fichadaColumnasBasic, fichadaMesTodas, parametros, BDSentencias.conexionRelojLocal);
        for (List l : lista) {
            Fichada f = new Fichada();
            f.setUserinfoid(Integer.parseInt(l.get(0).toString()));
            f.setFecha(Date.valueOf(l.get(1).toString()));
            f.setHora(new Time(((Time) l.get(2)).getTime()));
            marcadas.add(f);
        }
        return marcadas;
    }
    
    private boolean existeEnBD(List<Fichada> fichadaSrv,Fichada fichada) {
        boolean existe = false;
        int cont = 0;
        for (Fichada f : fichadaSrv) {
            if (f.getFecha() == fichada.getFecha() && f.getHora() == fichada.getHora() && f.getUserinfoid() == fichada.getUserinfoid()){
                cont++;
                System.out.println("Existe!!! -"+cont);
                existe = true;
                break;
            }
        }
        return existe;
    }


    private int buscarUserinfoid(Userinfo u, File access) {

        Conectate conexion = null;
        PreparedStatement psmt = null;

        int result = 0;

        conexion = new Conectate(access.getAbsolutePath());

        Connection conn = conexion.getConnection();

        try {

            psmt = conn.prepareStatement("SELECT A.USERID FROM USERINFO A WHERE BADGENUMBER = ?;");
            psmt.setString(1, u.getUserid() + "");

            ResultSet r = psmt.executeQuery();

            while (r.next()) {
                result = r.getInt("USERID");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ContFichada.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            conexion.desconectar(access.getAbsolutePath());
        }
        return result;
    }

}
