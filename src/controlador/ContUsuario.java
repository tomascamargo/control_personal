/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import modelo.Area;
import modelo.Usuario;

/**
 *
 * @author Viktor
 */
public class ContUsuario extends ControladorGeneral{

    public ContUsuario() {
        super();
    }
    
    private Usuario setearObjeto(List lista){
        return new Usuario(
                Integer.parseInt(lista.get(0).toString()),
                lista.get(1).toString(),
                Timestamp.valueOf(lista.get(2).toString()),
                lista.get(3).toString(), 
                lista.get(4).toString(),
                Integer.parseInt(lista.get(5).toString()),
                Integer.parseInt(lista.get(6).toString()),
                Boolean.parseBoolean(lista.get(7).toString())
        );
    }
    
    public List<Usuario> encontrarTodos(){
        List<Usuario> listaUsuarios = new ArrayList<>();
        List<List> lista = dameListaCompleta(columnasUsuariosDeSistema, consultaUsuariosDeSistema, conexionRelojLocal);
        for (List lista1 : lista) {
//            System.out.println(lista1.toString());
            listaUsuarios.add(setearObjeto(lista1));
            
        }
        
        return  listaUsuarios;
    }
    
    public Usuario encontrar(String user){
        Usuario usuario = new Usuario();
        Object[] datos = {user};
        List<List> lista = buscarPorParametro(columnasUsuariosDeSistema, consultaUsuariosDeSistemaPorUsuario,datos, conexionRelojLocal);
        for (List lista1 : lista) {
            
            usuario= setearObjeto(lista1);
        }
        
        return  usuario;
    }
    
    public Usuario encontrar(int id){
        Usuario usuario = new Usuario();
        Object[] datos = {id};
        List<List> lista = buscarPorParametro(columnasUsuariosDeSistema, consultaUsuariosDeSistemaPorId,datos, conexionRelojLocal);
        for (List lista1 : lista) {
//            System.out.println("TAMAÑO EM: "+lista.size());
            usuario= setearObjeto(lista1);
        }
        
        return  usuario;
    }
    
    public int insertar(Usuario usuario){
        Object[] datos =  {usuario.getUsuario(),usuario.getFechamodificacion(),usuario.getUseralta(),usuario.getTipo(),usuario.getIdaArea(),usuario.getIdaSeccion()};
        return ejecutarSentencia(datos, usuarioDeSistemaINSERT, conexionRelojLocal);
    }
    
    public int update(Usuario usuario){
        Object[] datos =  {usuario.getUsuario(),usuario.getFechamodificacion(),usuario.getUseralta(),usuario.getTipo(),usuario.getIdaArea(),usuario.getIdaSeccion(),usuario.getId()};
        return ejecutarSentencia(datos, usuariosDeSistemaUPDATE, conexionRelojLocal);
    }
    
    public int delete(Usuario usuario){
        Object[] datos =  {true,usuario.getId()};
        return ejecutarSentencia(datos, usuariosDeSistemaDELETE, conexionRelojLocal);
    }
    
    public List<Usuario> getCapataces() {
         List<Usuario> listaRetornar = new ArrayList<>();
        List<List> listaList = new ArrayList<>();
        
        listaList = dameListaCompleta(columnasUsuariosDeSistema, consultaUserinfoCAPATAZ, conexionRelojLocal);
//        System.out.println("tamaño: "+listaList.size());
        for (List list : listaList) {
            listaRetornar.add(setearObjeto(list));
        }

        return listaRetornar;
    }

    public List<Usuario> masDeUnArea(String nombre) {
        List<Usuario> listaRetornar = new ArrayList<>();
        List<List> listaList = new ArrayList<>();
        Object[] datos ={nombre};
        
        listaList = buscarPorParametro(columnasUsuariosDeSistema, consultaUserinfoXDETALLE, datos,conexionRelojLocal);
//        System.out.println("tamaño: "+listaList.size());
        for (List l : listaList) {
            listaRetornar.add(setearObjeto(l));
        }
        return listaRetornar;
    }

    public List<Area> encontrarTodos(List<Usuario> usuarios) {//recibe una lista con todas las areas del usuario
        ContArea contArea = new ContArea();
        List<Area> listReturn = new ArrayList<>();
        for (Usuario u : usuarios) {
            listReturn.add(contArea.encontrar(u.getIdaArea()));
        }
        
        return listReturn;
    }
}
