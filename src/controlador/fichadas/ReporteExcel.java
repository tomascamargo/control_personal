/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.fichadas;

import java.io.File;
import java.io.IOException;
import java.sql.Time;
import java.util.List;
import jxl.Workbook;
import jxl.write.DateFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import reporte.ReporteFichada;

/**
 *
 * @author Viktor
 */
public class ReporteExcel {

    public void escribirExcel(String pathName, List<ReporteFichada> listaFichada, String empleado) {
        try {
            Utilidades u = new Utilidades();
            //Se crea el libro Excel
            WritableWorkbook workbook
                    = Workbook.createWorkbook(new File(pathName));

            //Se crea una nueva hoja dentro del libro
            WritableSheet sheet
                    = workbook.createSheet(empleado, 0);

            //Creamos los titulos
            sheet.addCell(new jxl.write.Label(0, 0, "DIA"));
            sheet.addCell(new jxl.write.Label(1, 0, "FECHA"));
            sheet.addCell(new jxl.write.Label(2, 0, "ENTRADA"));
            sheet.addCell(new jxl.write.Label(3, 0, "SALIDA"));
            sheet.addCell(new jxl.write.Label(4, 0, "ENTRADA"));
            sheet.addCell(new jxl.write.Label(5, 0, "SALIDA"));
            sheet.addCell(new jxl.write.Label(6, 0, "ENTRADA"));
            sheet.addCell(new jxl.write.Label(7, 0, "SALIDA"));
            sheet.addCell(new jxl.write.Label(8, 0, "NORMALES"));
            sheet.addCell(new jxl.write.Label(9, 0, "EXTRAS"));
            sheet.addCell(new jxl.write.Label(10, 0, "RETRAZO"));

            int i = 1;
            for (ReporteFichada rf : listaFichada) {

                //Creamos una celda de tipo fecha y la mostramos
                //indicando un patón de formato
                DateFormat customDateFormatDIA
                        = new DateFormat("dd/MM/yyyy");
                DateFormat customDateFormatHORA = new DateFormat("hh:mm:ss");

                WritableCellFormat dateFormatDIA
                        = new WritableCellFormat(customDateFormatDIA);

                WritableCellFormat dateFormatHORA
                        = new WritableCellFormat(customDateFormatHORA);

                try {
                    //el dia de semana
                    sheet.addCell(new jxl.write.Label(0, i, rf.getDia()));
                    //asignamos las horas
                    sheet.addCell(new jxl.write.Label(1, i, rf.getFecha(), dateFormatDIA));

                    if (rf.getEntrada1() != null) {
                        sheet.addCell(new jxl.write.DateTime(2, i, Time.valueOf(rf.getEntrada1()), dateFormatHORA));
                    } else {
                        sheet.addCell(new jxl.write.Label(2, i, ""));
                    }
                    if (rf.getSalida1() != null) {
                        sheet.addCell(new jxl.write.DateTime(3, i, Time.valueOf(rf.getSalida1()), dateFormatHORA));
                    } else {
                        sheet.addCell(new jxl.write.Label(3, i, ""));
                    }
                    if (rf.getEntrada2() != null) {
                        sheet.addCell(new jxl.write.DateTime(4, i, Time.valueOf(rf.getEntrada2()), dateFormatHORA));
                    } else {
                        sheet.addCell(new jxl.write.Label(4, i, ""));
                    }
                    if (rf.getSalida2() != null) {
                        sheet.addCell(new jxl.write.DateTime(5, i, Time.valueOf(rf.getSalida2()), dateFormatHORA));
                    } else {
                        sheet.addCell(new jxl.write.Label(5, i, ""));
                    }
                    if (rf.getEntrada3() != null) {
                        sheet.addCell(new jxl.write.DateTime(6, i, Time.valueOf(rf.getEntrada3()), dateFormatHORA));
                    } else {
                        sheet.addCell(new jxl.write.Label(6, i, ""));
                    }
                    if (rf.getSalida3() != null) {
                        sheet.addCell(new jxl.write.DateTime(7, i, Time.valueOf(rf.getSalida3()), dateFormatHORA));
                    } else {
                        sheet.addCell(new jxl.write.Label(7, i, ""));
                    }
                    if (rf.getNormales() != null) {
                        try {
                            sheet.addCell(new jxl.write.DateTime(8, i, Time.valueOf(rf.getNormales()), dateFormatHORA));
                        } catch (Exception e) {
                            sheet.addCell(new jxl.write.Label(8, i, rf.getNormales()));
                        }
                    } else {
                        sheet.addCell(new jxl.write.Label(8, i, ""));
                    }
                    if (rf.getExtras() != null) {
                        try {
                            sheet.addCell(new jxl.write.DateTime(9, i, Time.valueOf(rf.getExtras()), dateFormatHORA));
                        } catch (Exception e) {
                            sheet.addCell(new jxl.write.Label(9, i, rf.getExtras()));
                        }
                    } else {
                        sheet.addCell(new jxl.write.Label(9, i, ""));
                    }
                    if (rf.getRetrazo() != null) {
                        try {
                            sheet.addCell(new jxl.write.DateTime(10, i, Time.valueOf(rf.getRetrazo()), dateFormatHORA));
                        } catch (Exception e) {
                            sheet.addCell(new jxl.write.Label(10, i, rf.getRetrazo()));
                        }
                    } else {
                        sheet.addCell(new jxl.write.Label(10, i, ""));
                    }
                } catch (Exception e) {

                }
                i++;
            }

            //Escribimos los resultados al fichero Excel
            workbook.write();
            workbook.close();

            System.out.println("Ejemplo finalizado.");
        } catch (IOException ex) {
            System.out.println("Error al crear el fichero.");
            ex.printStackTrace();
        } catch (WriteException ex) {
            System.out.println("Error al escribir el fichero.");
            ex.printStackTrace();
        }
    }
}
