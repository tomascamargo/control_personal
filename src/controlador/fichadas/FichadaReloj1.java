/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.fichadas;

import controlador.Conectate;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.sql.Date;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Fichada;

/**
 *
 * @author Viktor
 */
public class FichadaReloj1 {

    Conectate conexion;
    String dir;

    public FichadaReloj1(String dir) {
        this.dir = dir;
    }
    
    
    private void conectar() {
        conexion = new Conectate(dir, "firtdkw2k");
    }

    private void desconectar() {
        conexion.desconectar(dir);
    }

    public static Date ParseFecha(String fecha) {
        SimpleDateFormat formato = new SimpleDateFormat("YYYY-MM-dd");
        Date fechaDate = null;
        try {
            fechaDate = new Date(formato.parse(fecha).getTime());
        } catch (ParseException ex) {
            Logger.getLogger(FichadaReloj1.class.getName()).log(Level.SEVERE, null, ex);
        }
        return fechaDate;
    }

    public Calendar parseDateToCalendar(Date fecha) {
        Calendar c = GregorianCalendar.getInstance();
        c.setTime(fecha);
//        System.out.println("Fecha:--> "+ c.get(Calendar.DAY_OF_MONTH)+"/"+c.get(Calendar.MONTH)+"/"+c.get(Calendar.YEAR));
        return c;
    }

    public List<String> trerFichadas(String fecha, String codigo) {
        List<String> lista = new ArrayList<>();

        try {
            conectar();

            Connection conn = conexion.getConnection();

            Statement smt = conn.createStatement();

            ResultSet rs = smt.executeQuery("SELECT  ficHora FROM T_Fichadas WHERE ficFecha =#" + fecha + "# AND legCodigo =" + codigo + ";");
//            System.out.println("QUERY: "+"SELECT  ficHora FROM T_Fichadas WHERE ficFecha =#"+fecha+"# AND ficTarjeta ='"+tarjeta+"';");
//            System.out.println("tamaño: "+rs.getFetchSize());

            while (rs.next()) {
//                System.out.println(rs.getString(1));
                lista.add(rs.getString(1).split(" ")[1]);
            }

        } catch (Exception e) {
            System.out.println("problemas al buscar Contacto " + e.getMessage());

            e.printStackTrace();
        }
        desconectar();
        return lista;
    }
    
    public List<Fichada> trerFichadasTodos(String fecha1, String fecha2) {
        List<Fichada> lista = new ArrayList<>();
        try {
            conectar();

            Connection conn = conexion.getConnection();
            System.out.println("f1: "+fecha1);
            System.out.println("f2: "+fecha2);
            PreparedStatement pe = conn.prepareStatement("SELECT ficFecha,ficHora, legCUIL FROM T_Fichadas a"
                    + " INNER JOIN T_Legajos b ON a.legCodigo = b.legCodigo"
                    + " WHERE ficFecha BETWEEN ? AND ? "
                    + " ORDER BY a.legCodigo,a.ficFecha,a.ficHora");
            pe.setString(1, fecha1);
            pe.setString(2, fecha2);

            ResultSet rs = pe.executeQuery();
            System.out.println(pe.toString());
//            System.out.println("tamaño: "+rs.getFetchSize());

            while (rs.next()) {
//                System.out.println(rs.getString(1));
                Fichada fichada = new Fichada();
//                fichada.setCucuPers(rs.getLong("legCUIL")); TERMINAR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!111
//                Timestamp t = Timestamp.valueOf(rs.getString("ficFecha").split(" ")[0]+" " + rs.getString("ficHora").split(" ")[1]);
                Date fecha = Date.valueOf(rs.getString("ficFecha").split(" ")[0]);
                Time hora = Time.valueOf(rs.getString("ficHora").split(" ")[1]);//new Time(Timestamp.valueOf(fecha.toString()+" " + rs.getString("ficHora").split(" ")[1]).getTime());
                
                fichada.setFecha(fecha);
                fichada.setHora(hora);
                fichada.setReloj("Planta");
//                System.out.println(fichada.getFechaHora()+" "+ fichada.getUserid());
                lista.add(fichada);
                
            }

        } catch (Exception e) {
            System.out.println("problemas al buscar Contacto " + e.getMessage());

            e.printStackTrace();
        }finally{
            desconectar();
        }
            
        
        return lista;
    }

//    public List<Fichada> traerFichadaMes(int anio, int mes, Long empleado) throws IOException {
//        Utilidades u = new Utilidades();
//        List<String> listaFechas = u.fechas(anio, mes);
//        List<Fichada> listaFichada = new ArrayList<Fichada>();
//
//        for (String fecha : listaFechas) {
//            for (String es : trerFichadas(fecha, "" + empleado)) {
//                Fichada f = new Fichada();
//                f.setCucuPers(empleado);
////                f.setFechaHora(Timestamp.valueOf(fecha+" "+es));
//                f.setFecha(Date.valueOf(fecha));
//                f.setHora(Time.valueOf(es));//(new Time(Timestamp.valueOf(fecha+" "+es).getTime()));
//                listaFichada.add(f);
//            }
//        }
//
////        for (Fichada fichada : listaFichada) {
////            for (String hora : fichada.getEntradasSalidas()) {
////                System.out.println(fichada.getFecha() + " " + hora.split(" ")[1]);
////            }
////        }
//        return listaFichada;
//    }

    public List<Long> traerEmpleados() {
        List<Long> listaEmpleados = new ArrayList<>();

        try {
            conectar();

            Connection conn = conexion.getConnection();

            PreparedStatement psmt = conn.prepareStatement("SELECT DISTINCT legCUIL FROM T_Legajos");
            //psmt.setString(1, nombre);

            ResultSet rs = psmt.executeQuery();

            while (rs.next()) {

//                System.out.println("" + rs.getString(1)+ " - "+rs.getString(2));
                Long e = (rs.getLong(1));
                listaEmpleados.add(e);
            }

        } catch (Exception e) {
            System.out.println("problemas al buscar Contacto " + e.getMessage());
            e.printStackTrace();
        } finally {
            desconectar();
        }
        return listaEmpleados;
    }
}
