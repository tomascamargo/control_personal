/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.fichadas;

import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import java.util.List;

/**
 *
 * @author Viktor
 */
public class Utilidades {

    public static int CalculaDias(int a, int m) throws IOException {

        if ((m == 1) || (m == 3) || (m == 5) || (m == 7) || (m == 8) || (m == 10) || (m == 12)) {

            return 31;

        } else if ((m == 4) || (m == 6) || (m == 9) || (m == 11)) {

            return 30;

        } else if (m == 2) {

            if ((((a % 4) == 0) && ((a % 100)) != 100) || ((a % 400) == 0)) {

                return 29;

            }

            return 28;

        } else {

            return -1;

        }
    }

    public List<String> fechas(Timestamp t1, Timestamp t2) {

        Date diaInicio = new Date(t1.getTime());
        Date diaFin = new Date(t2.getTime());

        List<String> fechas = new ArrayList<>();

        while (diaInicio.getTime() < diaFin.getTime()) {

            fechas.add(diaInicio.toString());
            diaInicio.setTime(diaInicio.getTime() + Long.parseLong("86400000"));

        }

        return fechas;
    }

    public List<String> fechas(int anio, int mes) throws IOException {
        int dias = CalculaDias(anio, mes);

        String mesString = null;
        String diaString = null;

        List<String> listaFechas = new ArrayList<>();

        for (int i = 1; i <= dias; i++) {
            if (mes < 10) {
                mesString = "0" + mes;
            } else {
                mesString = "" + mes;
            }
            if (i < 10) {
                diaString = "0" + i;
            } else {
                diaString = "" + i;
            }
            listaFechas.add(anio + "-" + mesString + "-" + diaString);
//            listaFechas.add(diaString+"/"+mesString +"/"+anio);
//            System.out.println(anio+"-"+mesString+"-"+diaString);
        }

        return listaFechas;
    }

    public String[] ordenar(String hora1, String hora2) {
        String[] ordenado = new String[2];

        String[] v1 = hora1.split(":");
        String[] v2 = hora2.split(":");

        int h1 = Integer.parseInt(v1[0]);
        int m1 = Integer.parseInt(v1[1]);
        int s1 = Integer.parseInt(v1[2]);

        int h2 = Integer.parseInt(v2[0]);
        int m2 = Integer.parseInt(v2[1]);
        int s2 = Integer.parseInt(v2[2]);

        if (h1 < h2) {
            ordenado[0] = hora1;
            ordenado[1] = hora2;
        } else if (h1 > h2) {
            ordenado[0] = hora2;
            ordenado[1] = hora1;
        } else if (h1 == h2) {
            if (m1 < m2) {
                ordenado[0] = hora1;
                ordenado[1] = hora2;
            } else if (m1 > m2) {
                ordenado[0] = hora2;
                ordenado[1] = hora1;
            }
        }
        return ordenado;

    }

    public String traerFecha(Date fechaDate) {
        return fechaDate.toString();
    }

    public String traerHora(Time horaTime) {
        String hora2 = horaTime.toString().split(":")[0] + ":" + horaTime.toString().split(":")[1] + ":00";
        return hora2;
    }

    public String sumar(String hacu, String hs) {
        
        
        int hv1 = 0;
        int mv1 = 0;

        int hv2 = 0;
        int mv2 = 0;
        
        try{
        String[] v1 = hacu.split(":");
        hv1 = Integer.parseInt(v1[0]);
        mv1 = Integer.parseInt(v1[1]);

        String[] v2 = hs.split(":");
        hv2 = Integer.parseInt(v2[0]);
        mv2 = Integer.parseInt(v2[1]);

        hv1 = hv1 + hv2;
        mv1 = mv1 + mv2;

        if (mv1 > 59) {
            hv1 = hv1 + (mv1 / 60);
            mv1 = mv1 % 60;
        }
        }catch(Exception ex){
            System.out.println("Error: "+ex.getMessage()+"\n"+hs);
        }

        return hv1 + ":" + mv1 + ":0";
    }

    public boolean menorQ(long horaE, long horaEReal) {
        boolean resul = false;
        if (horaE < horaEReal) {//entro tarde
            //llego tarde
//            System.err.println(new Timestamp(horaE).toString() + " - " + new Timestamp(horaEReal).toString());
//            System.err.println("Llego tarde");
            resul = false;
        } else if (horaE > horaEReal) {//entro temprano
            //llego temprano
//            System.err.println("Llego temprano");
            resul = true;
        }

        return resul;
    }

    public String formatearFecha(String fechaEntrada) {
        String fechaSalida = "";
        try {
            SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
            java.util.Date fecha = formateador.parse(fechaEntrada);
            SimpleDateFormat formateador2 = new SimpleDateFormat("dd/MM/yyyy");
            fechaSalida = formateador2.format(fecha);
        } catch (Exception e) {
            System.err.println("Problemas: " + e.getStackTrace());
        }

        return fechaSalida;
    }

    public int obtenerDiaDeSemana(Date d) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(d);
        return cal.get(Calendar.DAY_OF_WEEK);
    }
    
    public String retornarFechaHora(Timestamp t){
        
        DateFormat df = DateFormat.getDateTimeInstance();
        
        return df.format(t);
    }
    
    public String retornarFecha(Timestamp t){
        
        DateFormat df = DateFormat.getDateInstance();
        
        return df.format(t);
    }
    
    public String retornarHora(Timestamp t){
        
        DateFormat df = DateFormat.getTimeInstance();
        
        return df.format(t);
    }

}
