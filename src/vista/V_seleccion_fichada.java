/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.BDSentencias;
import controlador.ContArea;
import controlador.ContFeriado;
import controlador.ContFichada;
import controlador.ContLogger;
import controlador.ContPersonaGrupo;
import controlador.ContReloj;
import controlador.ContSeccion;
import controlador.ContUserinfo;
import controlador.ControladorGeneral;
import controlador.ControladorReporte;
import controlador.fichadas.Utilidades;
import java.awt.Cursor;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableModel;
import modelo.Feriado;
import modelo.Fichada;
import modelo.Loggs;
import modelo.Reloj;
import modelo.Seccion;
import modelo.Userinfo;
import modelo.Usuario;
import reporte.Marcadas;
import reporte.ReporteFichada;

/**
 *
 * @author Viktor
 */
public class V_seleccion_fichada extends javax.swing.JDialog {

    /**
     * Creates new form V_seleccion_fichada
     */
    ControladorGeneral controladorGeneral = new ControladorGeneral();
    ControladorReporte controladorReporte = new ControladorReporte();
    List<Userinfo> listaEmpleados;
    List<Userinfo> listaEmpleadosTemp;
    List<Userinfo> listaGenerarR;
    ContUserinfo contEmpleado = new ContUserinfo();
    ContFichada contFichada = new ContFichada();
    ContReloj contReloj = new ContReloj();
    List<Feriado> listaFeriados = null;
    List<Reloj> listaReloj;
    Usuario usuario;
    boolean isConsulta = false;

    //para los encargados de seccion y jefes de area
    List<Seccion> listaSeccion;
    ContSeccion contSeccion = new ContSeccion();
    ContArea contArea = new ContArea();

    public V_seleccion_fichada(java.awt.Frame parent, boolean modal, Usuario usuario) {
        super(parent, modal);
        initComponents();
        listaEmpleados = new ArrayList<>();
        listaEmpleadosTemp = new ArrayList<>();
        listaGenerarR = new ArrayList<>();
        listaReloj = new ArrayList<>();
        this.header = tabla.getTableHeader();

        this.usuario = usuario;
        listaSeccion = new ArrayList<>();
        listaEmpleados = contEmpleado.listar();//listaEmpleados();
        listaEmpleadosTemp = listaEmpleados;

        mni_deseleccionar_todos.setEnabled(false);
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
//        cargarTabla();
        ordenarTablaPorColumna();
        desactivarComponentes();
        otorgarPermisos();
        anioActual();
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

        cargarComboSecciones();
        cargarComboRelojes();

        barra.setVisible(false);
        lbl_porcentaje.setVisible(false);
    }
    
    private void cargarComboRelojes() {
        listaReloj = contReloj.encontrarTodos();
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();

        for (Reloj r : listaReloj) {
            modelo.addElement(r.getNombre());
        }
        modelo.addElement("Seleccione...");
        modelo.setSelectedItem("Seleccione...");
        cbx_relojes.setModel(modelo);
    }    

    private void anioActual() {
        Calendar c = Calendar.getInstance();
        txt_anio.setText("" + c.get(Calendar.YEAR));
    }


    private void otorgarPermisos() {
        switch (usuario.getTipo()) {
            case "admin":
                chbx_seccion.setVisible(true);
                chbx_reloj.setVisible(true);
                cbx_relojes.setVisible(true);
                break;
            case "supervisor":
                chbx_seccion.setVisible(true);
                chbx_reloj.setVisible(true);
                cbx_relojes.setVisible(true);
                break;
            case "consulta":
                chbx_seccion.setVisible(false);
                chbx_reloj.setVisible(false);
                cbx_relojes.setVisible(false);
                isConsulta = true;
                break;
        }
    }

    private void cargarComboSecciones() {
        if (usuario.getTipo().equals("admin") || usuario.getTipo().equals("supervisor")) {
            listaSeccion = contSeccion.encontrarTodos();
        } else if (usuario.getTipo().equals("consulta")) {
            if (usuario.getIdaArea() > 0) {
                listaSeccion = contSeccion.encontrarPorArea(usuario.getIdaArea());
            } else if (usuario.getIdaSeccion() > 0) {
                listaSeccion.add(contSeccion.encontrar(usuario.getIdaSeccion()));
            }
        }

        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        for (Seccion s : listaSeccion) {
            modelo.addElement(s.getDetalle());
        }

        cbx_seccion.setModel(modelo);
    }

    private void desactivarComponentes() {
        txt_anio.setEnabled(false);
        cbx_seccion.setEnabled(false);
        cbx_relojes.setEnabled(false);
        chbx_reloj.setEnabled(false);
        chbx_seccion.setEnabled(false);
        tabla.setEnabled(false);
        btn_cancelar.setEnabled(false);
        btn_generarR.setEnabled(false);
    }

    private void activarComponentes() {
        txt_anio.setEnabled(true);
        if (isConsulta) {
            cbx_seccion.setEnabled(true);
        } else {
            chbx_reloj.setEnabled(true);
            chbx_seccion.setEnabled(true);
        }
        tabla.setEnabled(true);
        btn_cancelar.setEnabled(true);
        btn_generarR.setEnabled(true);
    }

    // <editor-fold defaultstate="collapsed" desc="TABLA">
    private void vaciarTabla() {
        tabla.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{
                    "CUIL", "NOMBRE", ""
                }
        ) {
            Class[] types = new Class[]{
                java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean[]{
                false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        jScrollPane1.setViewportView(tabla);
        if (tabla.getColumnModel().getColumnCount() > 0) {
            tabla.getColumnModel().getColumn(0).setMinWidth(150);
            tabla.getColumnModel().getColumn(0).setPreferredWidth(150);
            tabla.getColumnModel().getColumn(0).setMaxWidth(150);
            tabla.getColumnModel().getColumn(2).setMinWidth(50);
            tabla.getColumnModel().getColumn(2).setPreferredWidth(50);
            tabla.getColumnModel().getColumn(2).setMaxWidth(50);
        }
    }

    private void cargarTabla() {
        vaciarTabla();
        DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();
        ContUserinfo contEmpleado = new ContUserinfo();

        listaEmpleados = contEmpleado.listar();//listaEmpleados();
        listaEmpleadosTemp = listaEmpleados;

        for (Userinfo e : listaEmpleadosTemp) {
            if (e.getName() != null) {
                Object[] datos = new Object[tabla.getColumnCount()];

                datos[0] = e.getUserid();
                datos[1] = e.getName();
                datos[2] = false;
                modelo.addRow(datos);
            }
        }

        tabla.setModel(modelo);
    }

    private void cargarTabla(String relojOrigen) {
        vaciarTabla();
        int reloj = contReloj.encontrarPorNombre(relojOrigen).getId();
        listaEmpleadosTemp = new ArrayList<>();
        DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();
        ContUserinfo contEmpleado = new ContUserinfo();

        if (relojOrigen.equals("Todos")) {
            listaEmpleadosTemp = listaEmpleados;
        } else {
            for (Userinfo u : listaEmpleados) {
                if (u.getReloj() == reloj) {
                    listaEmpleadosTemp.add(u);
                }
            }
        }
//        listaEmpleadosTemp = listaEmpleados;

        for (Userinfo e : listaEmpleadosTemp) {
            if (e.getName() != null) {
                Object[] datos = new Object[tabla.getColumnCount()];

                datos[0] = e.getUserid();
                datos[1] = e.getName();
                datos[2] = false;
                modelo.addRow(datos);
            }
        }

        tabla.setModel(modelo);
    }

    private void cargarTabla(int idSecion) {
        vaciarTabla();
        listaEmpleadosTemp = new ArrayList<>();
        DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();
        ContUserinfo contEmpleado = new ContUserinfo();

        for (Userinfo u : listaEmpleados) {
            if (u.getIdSeccion() == idSecion) {
                listaEmpleadosTemp.add(u);
            }
        }

        for (Userinfo e : listaEmpleadosTemp) {
            if (e.getName() != null) {
                Object[] datos = new Object[tabla.getColumnCount()];

                datos[0] = e.getUserid();
                datos[1] = e.getName();
                datos[2] = false;
                modelo.addRow(datos);
            }
        }

        tabla.setModel(modelo);
    }
    // </editor-fold>

    private void seleccionar(int fila) {

        Userinfo e = listaEmpleadosTemp.get(fila);
        if (contEmpleado.tieneHorario(e.getId())) {
            if (!listaGenerarR.contains(e)) {
                listaGenerarR.add(e);
                tabla.setValueAt(true, fila, 2);
//                System.out.println("--> " + "SELECCIONADO " + e.getName());
//                System.out.println("Tamaño de lista: " + listaGenerarR.size());
            }
        } else {
//            System.err.println("--> " + "SELECCIONADO " + e.getName());
            JOptionPane.showMessageDialog(null, e.getName() + " no posee horario asignado", "Error", JOptionPane.ERROR_MESSAGE);
        }

//        System.out.println("Tamaño de lista: " + listaGenerarR.size());
    }

    private void deseleccionar(int fila) {
        Userinfo eBorrar = listaEmpleadosTemp.get(fila);
        listaGenerarR.remove(eBorrar);
        tabla.setValueAt(false, fila, 2);

//        System.err.println("Tamaño de lista: " + listaGenerarR.size());
    }

    private void seleccionarDeseleccionar(int selectedRow) {
        if ((Boolean) tabla.getValueAt(selectedRow, 2) == true) {
            seleccionar(selectedRow);

        } else if ((Boolean) tabla.getValueAt(selectedRow, 2) == false) {
            deseleccionar(selectedRow);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        mni_seleccionar = new javax.swing.JMenuItem();
        mni_deseleccionar = new javax.swing.JMenuItem();
        mni_seleccionar_todos = new javax.swing.JMenuItem();
        mni_deseleccionar_todos = new javax.swing.JMenuItem();
        buttonGroup1 = new javax.swing.ButtonGroup();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        btn_generarR = new javax.swing.JButton();
        btn_cancelar = new javax.swing.JButton();
        barra = new javax.swing.JProgressBar();
        lbl_porcentaje = new javax.swing.JLabel();
        btn_imprimir = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        chbx_seccion = new javax.swing.JCheckBox();
        chbx_reloj = new javax.swing.JCheckBox();
        cbx_relojes = new javax.swing.JComboBox();
        cbx_seccion = new javax.swing.JComboBox();
        txt_anio = new javax.swing.JTextField();
        lbl_seccion = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        lbl_mes = new javax.swing.JLabel();
        cbx_mes = new javax.swing.JComboBox();

        mni_seleccionar.setText("Seleccionar");
        mni_seleccionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mni_seleccionarActionPerformed(evt);
            }
        });
        jPopupMenu1.add(mni_seleccionar);

        mni_deseleccionar.setText("Deseleccionar");
        mni_deseleccionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mni_deseleccionarActionPerformed(evt);
            }
        });
        jPopupMenu1.add(mni_deseleccionar);

        mni_seleccionar_todos.setText("Seleccionar Todos");
        mni_seleccionar_todos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mni_seleccionar_todosActionPerformed(evt);
            }
        });
        jPopupMenu1.add(mni_seleccionar_todos);

        mni_deseleccionar_todos.setText("Deseleccionar Todos");
        mni_deseleccionar_todos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mni_deseleccionar_todosActionPerformed(evt);
            }
        });
        jPopupMenu1.add(mni_deseleccionar_todos);

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setIconImage(Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("Iconos/bicentenario90.png")));

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));

        jLabel1.setText("Seleccione los empleados a emitir fichada:");

        tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "CUIL", "NOMBRE", ""
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabla.setComponentPopupMenu(jPopupMenu1);
        tabla.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tablaMouseReleased(evt);
            }
        });
        tabla.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tablaKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tablaKeyTyped(evt);
            }
        });
        jScrollPane1.setViewportView(tabla);
        if (tabla.getColumnModel().getColumnCount() > 0) {
            tabla.getColumnModel().getColumn(0).setMinWidth(150);
            tabla.getColumnModel().getColumn(0).setPreferredWidth(150);
            tabla.getColumnModel().getColumn(0).setMaxWidth(150);
            tabla.getColumnModel().getColumn(2).setMinWidth(50);
            tabla.getColumnModel().getColumn(2).setPreferredWidth(50);
            tabla.getColumnModel().getColumn(2).setMaxWidth(50);
        }

        btn_generarR.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Estadisticas.png"))); // NOI18N
        btn_generarR.setText("Generar Reporte");
        btn_generarR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_generarRActionPerformed(evt);
            }
        });

        btn_cancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/No.png"))); // NOI18N
        btn_cancelar.setText("Cancelar");
        btn_cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelarActionPerformed(evt);
            }
        });

        lbl_porcentaje.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_porcentaje.setForeground(new java.awt.Color(153, 0, 0));
        lbl_porcentaje.setText("0%");

        btn_imprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/impresora.png"))); // NOI18N
        btn_imprimir.setText("Imprimir");
        btn_imprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_imprimirActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(204, 204, 204));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Parametros de seleccion de periodo y personal"));

        chbx_seccion.setBackground(new java.awt.Color(204, 204, 204));
        buttonGroup1.add(chbx_seccion);
        chbx_seccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chbx_seccionActionPerformed(evt);
            }
        });

        chbx_reloj.setBackground(new java.awt.Color(204, 204, 204));
        buttonGroup1.add(chbx_reloj);
        chbx_reloj.setText("Reloj:");
        chbx_reloj.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chbx_relojActionPerformed(evt);
            }
        });

        cbx_relojes.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Costa", "Cultura", "Planta", "Maestranza" }));
        cbx_relojes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbx_relojesActionPerformed(evt);
            }
        });

        cbx_seccion.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item1" }));
        cbx_seccion.setSelectedItem("Todos");
        cbx_seccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbx_seccionActionPerformed(evt);
            }
        });

        txt_anio.setText("2014");

        lbl_seccion.setText("Seccion:");

        jLabel2.setText("Año:");

        lbl_mes.setText("Mes:");

        cbx_mes.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre", "Seleccione..." }));
        cbx_mes.setSelectedItem("Seleccione...");
        cbx_mes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbx_mesActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(chbx_seccion, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(lbl_seccion)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbx_seccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29)
                        .addComponent(chbx_reloj))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(lbl_mes)
                        .addGap(18, 18, 18)
                        .addComponent(cbx_mes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(31, 31, 31)
                        .addComponent(jLabel2)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_anio, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbx_relojes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbx_mes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_mes)
                    .addComponent(jLabel2)
                    .addComponent(txt_anio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbx_seccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chbx_seccion)
                    .addComponent(chbx_reloj)
                    .addComponent(cbx_relojes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_seccion))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btn_imprimir)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_generarR)
                        .addGap(18, 18, 18)
                        .addComponent(btn_cancelar))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(barra, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbl_porcentaje))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 597, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel1)
                            .addGap(0, 0, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addGap(7, 7, 7)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 353, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_generarR)
                        .addComponent(btn_cancelar))
                    .addComponent(btn_imprimir))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(barra, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_porcentaje))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tablaMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaMouseReleased
        if (tabla.getSelectedRow() > -1) {
            seleccionarDeseleccionar(tabla.getSelectedRow());
        }
    }//GEN-LAST:event_tablaMouseReleased

    private void tablaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaMouseClicked

    }//GEN-LAST:event_tablaMouseClicked

    private void btn_cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelarActionPerformed
        dispose();
    }//GEN-LAST:event_btn_cancelarActionPerformed
    Utilidades u = new Utilidades();
    List<Fichada> fichada = new ArrayList<>();
    private void btn_generarRActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_generarRActionPerformed
        if (!cbx_mes.getSelectedItem().equals("Seleccione...")) {
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

            final float unoPorciento = (float) (100.0 / listaGenerarR.size());
            ContFeriado contFeriado = new ContFeriado();
            listaFeriados = contFeriado.feriados(Integer.parseInt(txt_anio.getText()), cbx_mes.getSelectedIndex() + 1);
//            System.out.println("1% = " + unoPorciento);

            Runnable r = new Runnable() {

                @Override
                public void run() {
                    barra.setVisible(true);
                    lbl_porcentaje.setVisible(true);
                    btn_generarR.setEnabled(false);
                    btn_cancelar.setEnabled(false);
                    btn_imprimir.setEnabled(false);
                    tabla.setEnabled(false);
                    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

                    float cont = (float) 0.0;
                    List<String> fechas = null;
                    try {
                        fechas = u.fechas(Integer.parseInt(txt_anio.getText()), cbx_mes.getSelectedIndex() + 1);

                        for (int i = 0; i < listaGenerarR.size(); i++) {
                            for (String fech : fechas) {
                                Object[] parametros = {listaGenerarR.get(i).getId(), fech};
                                for (List lista : controladorGeneral.buscarPorParametro(BDSentencias.fichadaColumnas, BDSentencias.fichadaMes3, parametros, BDSentencias.conexionRelojLocal)) {

                                    ContFichada contFichada = new ContFichada();
                                    Fichada f = contFichada.setearObjeto(lista);
                                    fichada.add(f);
//                          System.out.println("fdasdf");
                                }
                            }

                            cargarListaFichadaMes(fichada, fechas, i);

                            listaFichadamMes.add(listaFichadaDia);
                            fichada = new ArrayList<>();

                            cont = cont + unoPorciento;
                            barra.setValue((int) cont);
                            lbl_porcentaje.setText((int) cont + "%");
//                            System.out.println(cont + "%");
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(V_seleccion_fichada.class.getName()).log(Level.SEVERE, null, ex);

                        ContLogger contLogger = new ContLogger();
                        String error = "020";
                        Loggs loggs = new Loggs(new Timestamp(Calendar.getInstance().getTimeInMillis()),
                                Logger.getLogger(V_seleccion_fichada.class.getName()).getName(),
                                contLogger.getNombreTerminal(),
                                ex.fillInStackTrace().toString(),
                                error);
                        contLogger.insert(loggs);
                        JOptionPane.showMessageDialog(null, "Error:" + error + "\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);

                    }

                    try {
                        //controladorReporte.crearReportes(listaFichadaReporte, listaGenerarR, listaResumenEmpleado);
                        controladorReporte.crearReportes(listaListaaFichRep, listaGenerarR, listaResumenEmpleado, cbx_mes.getSelectedItem().toString() + " " + txt_anio.getText(), "ruta", listaFeriados, cbx_mes.getSelectedIndex() + 1, Integer.parseInt(txt_anio.getText()));
                        //listaListaaFichRep
                    } catch (IOException ex) {
                        Logger.getLogger(V_seleccion_fichada.class.getName()).log(Level.SEVERE, null, ex);

                        ContLogger contLogger = new ContLogger();
                        String error = "021";
                        Loggs loggs = new Loggs(new Timestamp(Calendar.getInstance().getTimeInMillis()),
                                Logger.getLogger(V_seleccion_fichada.class.getName()).getName(),
                                contLogger.getNombreTerminal(),
                                ex.fillInStackTrace().toString(),
                                error);
                        contLogger.insert(loggs);
                        JOptionPane.showMessageDialog(null, "Error:" + error + "\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);

                    }

                    barra.setVisible(false);
                    lbl_porcentaje.setVisible(false);
                    btn_generarR.setEnabled(true);
                    btn_cancelar.setEnabled(true);
                    btn_imprimir.setEnabled(true);
                    tabla.setEnabled(true);
                    setDefaultCloseOperation(DISPOSE_ON_CLOSE);

                }
            };
            Thread h = new Thread(r);
            h.start();

            //INICIALIZAMOS TODO
            listaFichadamMes = new ArrayList<>();
            listaFichadaDia = new ArrayList<>();
            listaResumenEmpleado = new ArrayList<>();

            listaListaaFichRep = new ArrayList<>();

            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione un mes.");
        }
    }//GEN-LAST:event_btn_generarRActionPerformed

    private void tablaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_tablaKeyReleased

    private void mni_seleccionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mni_seleccionarActionPerformed
        seleccionar(tabla.getSelectedRow());
    }//GEN-LAST:event_mni_seleccionarActionPerformed

    private void mni_deseleccionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mni_deseleccionarActionPerformed
        deseleccionar(tabla.getSelectedRow());
    }//GEN-LAST:event_mni_deseleccionarActionPerformed

    private void mni_seleccionar_todosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mni_seleccionar_todosActionPerformed
        seleccionarTodos();
    }//GEN-LAST:event_mni_seleccionar_todosActionPerformed

    private void tablaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaKeyTyped
        seleccionar(tabla.getSelectedRow());
    }//GEN-LAST:event_tablaKeyTyped

    private void mni_deseleccionar_todosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mni_deseleccionar_todosActionPerformed
        deseleccionarTodos();
    }//GEN-LAST:event_mni_deseleccionar_todosActionPerformed

    private void cbx_seccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbx_seccionActionPerformed
//        cargarTabla(cbx_reloj_o_area.getSelectedItem().toString());
        mni_seleccionar_todos.setEnabled(true);
        mni_deseleccionar_todos.setEnabled(false);
        for (int i = 0; i < tabla.getRowCount(); i++) {
            deseleccionar(i);
        }
        
        cargarTabla(listaSeccion.get(cbx_seccion.getSelectedIndex()).getId());
        listaFichadamMes = new ArrayList<>();
        listaFichadaDia = new ArrayList<>();
        listaResumenEmpleado = new ArrayList<>();

        listaListaaFichRep = new ArrayList<>();
    }//GEN-LAST:event_cbx_seccionActionPerformed

    private void cbx_mesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbx_mesActionPerformed
        if (!cbx_mes.getSelectedItem().equals("Seleccione...")) {
            activarComponentes();
        } else {
            desactivarComponentes();
        }
    }//GEN-LAST:event_cbx_mesActionPerformed

    private void btn_imprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_imprimirActionPerformed
        if (!cbx_mes.getSelectedItem().equals("Seleccione...")) {
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

            final float unoPorciento = (float) (100.0 / listaGenerarR.size());
            ContFeriado contFeriado = new ContFeriado();
            listaFeriados = contFeriado.feriados(Integer.parseInt(txt_anio.getText()), cbx_mes.getSelectedIndex() + 1);
//            System.out.println("1% = " + unoPorciento);

            Runnable r = new Runnable() {

                @Override
                public void run() {
                    barra.setVisible(true);
                    lbl_porcentaje.setVisible(true);
                    btn_generarR.setEnabled(false);
                    btn_cancelar.setEnabled(false);
                    tabla.setEnabled(false);

                    float cont = (float) 0.0;
                    List<String> fechas = null;
                    try {
                        fechas = u.fechas(Integer.parseInt(txt_anio.getText()), cbx_mes.getSelectedIndex() + 1);

                        for (int i = 0; i < listaGenerarR.size(); i++) {
                            for (String fech : fechas) {
                                Object[] parametros = {listaGenerarR.get(i).getId(), fech};
                                for (List lista : controladorGeneral.buscarPorParametro(BDSentencias.fichadaColumnas, BDSentencias.fichadaMes3, parametros, BDSentencias.conexionRelojLocal)) {

                                    ContFichada contFichada = new ContFichada();
                                    Fichada f = contFichada.setearObjeto(lista);
                                    fichada.add(f);
//                          System.out.println("fdasdf");
                                }
                            }

                            cargarListaFichadaMes(fichada, fechas, i);

                            listaFichadamMes.add(listaFichadaDia);
                            fichada = new ArrayList<>();

                            cont = cont + unoPorciento;
                            barra.setValue((int) cont);
                            lbl_porcentaje.setText((int) cont + "%");
//                            System.out.println(cont + "%");
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(V_seleccion_fichada.class.getName()).log(Level.SEVERE, null, ex);

                        ContLogger contLogger = new ContLogger();
                        String error = "022";
                        Loggs loggs = new Loggs(new Timestamp(Calendar.getInstance().getTimeInMillis()),
                                Logger.getLogger(V_seleccion_fichada.class.getName()).getName(),
                                contLogger.getNombreTerminal(),
                                ex.fillInStackTrace().toString(),
                                error);
                        contLogger.insert(loggs);
                        JOptionPane.showMessageDialog(null, "Error:" + error + "\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);

                    }

                    try {
                        //controladorReporte.crearReportes(listaFichadaReporte, listaGenerarR, listaResumenEmpleado);
                        controladorReporte.imprimirMuchosReportes(listaListaaFichRep, listaGenerarR, listaResumenEmpleado, cbx_mes.getSelectedItem().toString() + " " + txt_anio.getText(), "ruta", listaFeriados, cbx_mes.getSelectedIndex() + 1, Integer.parseInt(txt_anio.getText()));
                        //listaListaaFichRep
                    } catch (IOException ex) {
                        Logger.getLogger(V_seleccion_fichada.class.getName()).log(Level.SEVERE, null, ex);

                        ContLogger contLogger = new ContLogger();
                        String error = "023";
                        Loggs loggs = new Loggs(new Timestamp(Calendar.getInstance().getTimeInMillis()),
                                Logger.getLogger(V_seleccion_fichada.class.getName()).getName(),
                                contLogger.getNombreTerminal(),
                                ex.fillInStackTrace().toString(),
                                error);
                        contLogger.insert(loggs);
                        JOptionPane.showMessageDialog(null, "Error:" + error + "\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);

                    }

                    barra.setVisible(false);
                    lbl_porcentaje.setVisible(false);
                    btn_generarR.setEnabled(true);
                    btn_cancelar.setEnabled(true);
                    tabla.setEnabled(true);
                }
            };

            Thread h = new Thread(r);
            h.start();

            //INICIALIZAMOS TODO
            listaFichadamMes = new ArrayList<>();
            listaFichadaDia = new ArrayList<>();
            listaResumenEmpleado = new ArrayList<>();

            listaListaaFichRep = new ArrayList<>();

            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione un mes.");
        }
    }//GEN-LAST:event_btn_imprimirActionPerformed

    private void chbx_seccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chbx_seccionActionPerformed
        if (chbx_seccion.isSelected()) {
            cbx_seccion.setEnabled(true);
            cbx_relojes.setEnabled(false);
        } else {
            cbx_seccion.setEnabled(false);
        }
    }//GEN-LAST:event_chbx_seccionActionPerformed

    private void chbx_relojActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chbx_relojActionPerformed
        if (chbx_reloj.isSelected()) {
            cbx_relojes.setEnabled(true);
            cbx_seccion.setEnabled(false);
        } else {
            cbx_relojes.setEnabled(false);
        }
    }//GEN-LAST:event_chbx_relojActionPerformed

    private void cbx_relojesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbx_relojesActionPerformed
        mni_seleccionar_todos.setEnabled(true);
        mni_deseleccionar_todos.setEnabled(false);
        for (int i = 0; i < tabla.getRowCount(); i++) {
            deseleccionar(i);
        }
        
        cargarTabla(cbx_relojes.getSelectedItem().toString());
        listaFichadamMes = new ArrayList<>();
        listaFichadaDia = new ArrayList<>();
        listaResumenEmpleado = new ArrayList<>();

        listaListaaFichRep = new ArrayList<>();
    }//GEN-LAST:event_cbx_relojesActionPerformed

    List<List> listaFichadamMes = new ArrayList<>();
    List<Object[]> listaFichadaDia = new ArrayList<>();
    List<String[]> listaResumenEmpleado = new ArrayList<>();

    List<List<Marcadas>> listaListaaFichRep = new ArrayList<>();
    List<Marcadas> listaFichadaReporte = new ArrayList<>();

    private void cargarListaFichadaMes(List<Fichada> listaFichada, List<String> fechas, int index) throws IOException {

//        for (Empleado emp : listaGenerarR) {
        listaFichadaDia = contFichada.retornarFilaDia(listaFichada, fechas, listaGenerarR.get(index).getId(),false);

        //AÑADIMOS UN OBJ REPORTE
        for (Object[] vector : listaFichadaDia) {
            Marcadas rf = new Marcadas();

            if (vector[0] != null) {
                switch (u.obtenerDiaDeSemana(java.sql.Date.valueOf(vector[0].toString()))) {
                    case 1:
                        rf.setDia("D");
                        break;
                    case 2:
                        rf.setDia("L");
                        break;
                    case 3:
                        rf.setDia("M");
                        break;
                    case 4:
                        rf.setDia("M");
                        break;
                    case 5:
                        rf.setDia("J");
                        break;
                    case 6:
                        rf.setDia("V");
                        break;
                    case 7:
                        rf.setDia("S");
                        break;

                }
            }

            if (vector[0] != null) {
                rf.setFecha(vector[0].toString());

                try {
                    SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
                    Date fecha = formateador.parse(vector[0].toString());
                    SimpleDateFormat formateador2 = new SimpleDateFormat("dd/MM/yyyy");
                    rf.setFecha(formateador2.format(fecha));
                } catch (Exception ex) {

                    ContLogger contLogger = new ContLogger();
                    String error = "024";
                    Loggs loggs = new Loggs(new Timestamp(Calendar.getInstance().getTimeInMillis()),
                            Logger.getLogger(V_seleccion_fichada.class.getName()).getName(),
                            contLogger.getNombreTerminal(),
                            ex.fillInStackTrace().toString(),
                            error);
                    contLogger.insert(loggs);
                    JOptionPane.showMessageDialog(null, "Error:" + error + "\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);

                }
            }
            if (vector[1] != null) {
                rf.setEntrada1(vector[1].toString());
            }
            if (vector[2] != null) {
                rf.setSalida1(vector[2].toString());
            }
            if (vector[3] != null) {
                rf.setEntrada2(vector[3].toString());
            }
            if (vector[4] != null) {
                rf.setSalida2(vector[4].toString());
            }
            if (vector[5] != null) {
                rf.setEntrada3(vector[5].toString());
            }
            if (vector[6] != null) {
                rf.setSalida3(vector[6].toString());
            }
            if (vector[7] != null) {
                rf.setNormales(vector[7].toString());
            }
            if (vector[8] != null) {
                rf.setExtras(vector[8].toString());
            }
            if (vector[9] != null) {
                rf.setRetrazo(vector[9].toString());
            }

            listaFichadaReporte.add(rf);
        }

        listaResumenEmpleado.add(contFichada.resumenHoras(listaFichadaDia));

//        }
        listaListaaFichRep.add(listaFichadaReporte);
        listaFichadaReporte = new ArrayList<>();//inicializamos la lista de fichadas para el reporte
        listaFichadaDia = new ArrayList<>();//borramos toodo el contenido del array
//        resumenHoras();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(V_seleccion_fichada.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(V_seleccion_fichada.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(V_seleccion_fichada.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(V_seleccion_fichada.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                V_seleccion_fichada dialog = new V_seleccion_fichada(new javax.swing.JFrame(), true, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JProgressBar barra;
    private javax.swing.JButton btn_cancelar;
    private javax.swing.JButton btn_generarR;
    private javax.swing.JButton btn_imprimir;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox cbx_mes;
    private javax.swing.JComboBox cbx_relojes;
    private javax.swing.JComboBox cbx_seccion;
    private javax.swing.JCheckBox chbx_reloj;
    private javax.swing.JCheckBox chbx_seccion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JLabel lbl_mes;
    private javax.swing.JLabel lbl_porcentaje;
    private javax.swing.JLabel lbl_seccion;
    private javax.swing.JMenuItem mni_deseleccionar;
    private javax.swing.JMenuItem mni_deseleccionar_todos;
    private javax.swing.JMenuItem mni_seleccionar;
    private javax.swing.JMenuItem mni_seleccionar_todos;
    private javax.swing.JTable tabla;
    private javax.swing.JTextField txt_anio;
    // End of variables declaration//GEN-END:variables

    int cont = 0;
    JTableHeader header;

    private void ordenarTablaPorColumna() {
        header.addMouseListener(
                new MouseAdapter() {
                    public void mouseClicked(MouseEvent e) {
                        JTableHeader h = (JTableHeader) e.getSource();
                        int nColumn = h.columnAtPoint(e.getPoint());

                        if (nColumn != -1) {
                            sortColumn(nColumn, h.getTable().getModel());
                        }
                    }

                    void sortColumn(int nColumn, TableModel model) {
                        if ((cont / 2) * 2 == cont) {
                            ordenarTablaMENOR_MAYOR(nColumn);
                        } else {
                            ordenarTablaMAYOR_MENOR(nColumn);
                        }
                        cont++;
                    }
                });
    }

    private void ordenarTablaMENOR_MAYOR(final int nroFila) {
        List<Object[]> lista = ((DefaultTableModel) tabla.getModel()).getDataVector();  //ese obtienen los datos de la tabla y se guardan en un Array de Objects
        Collections.sort(lista, new Comparator() {
            public int compare(Object o1, Object o2) {   //este emtodo compar y va ordenando cada registro
                List<Object> fila1 = (List<Object>) o1;
                List<Object> fila2 = (List<Object>) o2;
                String hora1 = String.valueOf(fila1.get(nroFila));  //el dos especifica la columna a con la cual queremos ordenar
                String hora2 = String.valueOf(fila2.get(nroFila));
                return hora1.compareToIgnoreCase(hora2);
            }
        });
        tabla.repaint();
    }

    private void ordenarTablaMAYOR_MENOR(final int nroFila) {
        List<Object[]> lista = ((DefaultTableModel) tabla.getModel()).getDataVector();  //ese obtienen los datos de la tabla y se guardan en un Array de Objects
        Collections.sort(lista, new Comparator() {
            public int compare(Object o1, Object o2) {   //este emtodo compar y va ordenando cada registro
                List<Object> fila1 = (List<Object>) o1;
                List<Object> fila2 = (List<Object>) o2;
                String hora1 = String.valueOf(fila1.get(nroFila));  //el dos especifica la columna a con la cual queremos ordenar
                String hora2 = String.valueOf(fila2.get(nroFila));
                return hora2.compareToIgnoreCase(hora1);
            }
        });
        tabla.repaint();
    }

    private void deseleccionarTodos() {
        mni_seleccionar_todos.setEnabled(true);
        mni_deseleccionar_todos.setEnabled(false);

        Runnable r = new Runnable() {

            @Override
            public void run() {
                for (int i = 0; i < tabla.getRowCount(); i++) {
                    deseleccionar(i);
                }
            }
        };
        Thread hilo = new Thread(r);
        hilo.start();
    }

    private void seleccionarTodos() {
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

        mni_seleccionar_todos.setEnabled(false);
        mni_deseleccionar_todos.setEnabled(true);

        for (int i = 0; i < tabla.getRowCount(); i++) {
            seleccionar(i);
        }

        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }

}
