/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.ContLogger;
import controlador.ContReloj;
import controlador.ContSeccion;
import controlador.ContUserinfo;
import controlador.ContUsuario;
import controlador.GestorTabla;
import controlador.fichadas.FichadaReloj2;
import java.awt.Cursor;
import java.awt.event.KeyEvent;
import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import modelo.Loggs;
import modelo.Reloj;
import modelo.Seccion;
import modelo.Userinfo;
import modelo.Usuario;

/**
 *
 * @author Viktor
 */
public class V_userinfo extends javax.swing.JDialog {

    /**
     * Creates new form V_userinfo
     */
    List<Userinfo> nuevosUsuarios;//solo contendra los nuevos usuarios
    List<Userinfo> listaAux;//contendar los usuarios de las acces
    List<Userinfo> lista;//contendra los usuarios de la base mysql
    List<Reloj> listaReloj;
    List<Userinfo> listaTemp;//contendra los usuarios de costa,maestranza o edificio principal
    List<Seccion> listaSecciones;//
    Userinfo userSeleccionado = null;
    ContUserinfo contUserinfo = new ContUserinfo();
    ContSeccion contSeccion = new ContSeccion();
    ContReloj contReloj = new ContReloj();
    File file = null;
    String relojSelect = "";
    Usuario usuarioSistema;

    List<Usuario> listaCapataces;
    ContUsuario contCapataz;

    public V_userinfo(java.awt.Frame parent, boolean modal, Usuario usuario) {
        super(parent, modal);
        usuarioSistema = usuario;
        lista = new ArrayList<>();
        listaReloj = new ArrayList<>();
        nuevosUsuarios = new ArrayList<>();
        userSeleccionado = new Userinfo();

        listaCapataces = new ArrayList<>();
        contCapataz = new ContUsuario();

        initComponents();
        cargarTabla();
        cargarComboSeccion();
        otorgarPermisos();
        cargarComboRelojes();
        cargarPanelChecks();
    }

    private void otorgarPermisos() {
        switch (usuarioSistema.getTipo()) {
            case "admin":
                btn_importar.setEnabled(true);
                break;
            case "supervisor":
                btn_importar.setEnabled(false);
                break;
            case "consulta":
                btn_importar.setEnabled(false);
                break;
        }
    }

    private void cargarComboRelojes() {
        listaReloj = contReloj.encontrarTodos();
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();

        for (Reloj r : listaReloj) {
            modelo.addElement(r.getNombre());
        }
        modelo.addElement("Seleccione...");
        modelo.setSelectedItem("Seleccione...");
        cbx_reloj_de_usuario.setModel(modelo);
    }

    private void cargarPanelChecks() {
        ButtonGroup btn_grup = new ButtonGroup();
        for (Reloj r : listaReloj) {
            final String r2 = r.getNombre();
            JCheckBox check = new JCheckBox(r.getNombre());

            check.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    relojSelect = r2;
                    cargarTabla(r2);
                }
            });

            check.setName("check_" + r.getNombre());
            btn_grup.add(check);
            pnl_checks.add(check);

        }

        JCheckBox check_pendientes = new JCheckBox("Sin reloj ");
        check_pendientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                relojSelect = "";
                cargarTabla("");
            }
        });

        btn_grup.add(check_pendientes);
        pnl_checks.add(check_pendientes);
        
        JCheckBox checkTodos = new JCheckBox("Todos");
        checkTodos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                relojSelect = "Todos";
                cargarTabla("Todos");
            }
        });

        btn_grup.add(checkTodos);
        pnl_checks.add(checkTodos);

    }

    private void cargarComboSeccion() {
        listaSecciones = contSeccion.encontrarTodos();
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();

        for (Seccion s : listaSecciones) {
            modelo.addElement(s.getDetalle());
        }

        modelo.addElement("Seleccione...");
        modelo.setSelectedItem("Seleccione...");

        cbx_seccion.setModel(modelo);
    }

    private void vaciarTabla() {
        tabla.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{
                    "USERID", "NAME", "CUIL"
                }
        ) {
            boolean[] canEdit = new boolean[]{
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        jScrollPane1.setViewportView(tabla);
    }

    private void vaciarTablaAgregar() {
        tabla.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{
                    "USERID", "NAME", "CUIL", "AGREGAR"
                }
        ) {
            Class[] types = new Class[]{
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean[]{
                false, false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        jScrollPane1.setViewportView(tabla);
        if (tabla.getColumnModel().getColumnCount() > 0) {
            tabla.getColumnModel().getColumn(3).setMinWidth(50);
            tabla.getColumnModel().getColumn(3).setPreferredWidth(50);
            tabla.getColumnModel().getColumn(3).setMaxWidth(50);
        }
    }

    private void cargarTabla() {
        vaciarTabla();
        DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();
        lista = contUserinfo.listar();
        listaTemp = lista;
        for (Userinfo userinfo : listaTemp) {
            Object[] datos = new Object[tabla.getColumnCount()];
            datos[0] = userinfo.getUserid();
            datos[1] = userinfo.getName();
            datos[2] = userinfo.getCucupers();

            modelo.addRow(datos);
        }

        tabla.setModel(modelo);
    }

    private void cargarTabla(String origen) {
        listaTemp = new ArrayList<>();
        int relojS = contReloj.encontrarPorNombre(origen).getId();
        vaciarTabla();
        DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();

        if (origen.equals("Todos")) {
            listaTemp = lista;
        } else {
            for (Userinfo userinfo : lista) {
                if (userinfo.getReloj() == relojS) {
                    listaTemp.add(userinfo);
                }
            }
        }
//        listaTemp = lista;

        for (Userinfo userinfo : listaTemp) {
            Object[] datos = new Object[tabla.getColumnCount()];
            datos[0] = userinfo.getUserid();
            datos[1] = userinfo.getName();
            datos[2] = userinfo.getCucupers();

            modelo.addRow(datos);
        }

        tabla.setModel(modelo);
    }

    private void cargarTablaNuevos() {
        vaciarTablaAgregar();
        DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();
        for (Userinfo userinfo : nuevosUsuarios) {
            Object[] datos = new Object[tabla.getColumnCount()];
            datos[0] = userinfo.getUserid();
            datos[1] = userinfo.getName();
            datos[2] = userinfo.getCucupers();
            datos[3] = false;

            modelo.addRow(datos);
        }

        tabla.setModel(modelo);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jPanel1 = new javax.swing.JPanel();
        jButton2 = new javax.swing.JButton();
        cbx_reloj = new javax.swing.JComboBox();
        buttonGroup1 = new javax.swing.ButtonGroup();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        txt_id = new javax.swing.JTextField();
        txt_userid = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txt_name = new javax.swing.JTextField();
        txt_cuil = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        btn_cancelar = new javax.swing.JButton();
        btn_aceptar = new javax.swing.JButton();
        btn_importar = new javax.swing.JButton();
        btn_editar = new javax.swing.JButton();
        btn_save_cambios = new javax.swing.JButton();
        btn_cancel_cambios = new javax.swing.JButton();
        cbx_reloj_de_usuario = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        cbx_seccion = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        txt_busqueda = new javax.swing.JTextField();
        pnl_checks = new javax.swing.JPanel();

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Indique reloj"));

        jButton2.setText("Aceptar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        cbx_reloj.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Costa", "Cultura", "Maestranza", "Planta" }));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cbx_reloj, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cbx_reloj, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2)
                .addContainerGap(23, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "USERID", "NAME", "CUIL", "AGREGAR"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabla.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tablaMouseReleased(evt);
            }
        });
        tabla.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tablaKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tabla);
        if (tabla.getColumnModel().getColumnCount() > 0) {
            tabla.getColumnModel().getColumn(3).setMinWidth(50);
            tabla.getColumnModel().getColumn(3).setPreferredWidth(50);
            tabla.getColumnModel().getColumn(3).setMaxWidth(50);
        }

        jLabel1.setText("ID:");

        txt_id.setEnabled(false);

        txt_userid.setEnabled(false);
        txt_userid.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_useridKeyTyped(evt);
            }
        });

        jLabel2.setText("USERID:");

        jLabel3.setText("NAME:");

        jLabel4.setText("CUIL:");

        txt_name.setEnabled(false);

        txt_cuil.setEnabled(false);
        txt_cuil.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_cuilKeyTyped(evt);
            }
        });

        jLabel5.setText("RELOJ:");

        btn_cancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/No.png"))); // NOI18N
        btn_cancelar.setText("Cancelar");
        btn_cancelar.setEnabled(false);
        btn_cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelarActionPerformed(evt);
            }
        });

        btn_aceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/OK2.png"))); // NOI18N
        btn_aceptar.setText("Guardar");
        btn_aceptar.setEnabled(false);
        btn_aceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_aceptarActionPerformed(evt);
            }
        });

        btn_importar.setText("Importar Nuevos");
        btn_importar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_importarActionPerformed(evt);
            }
        });

        btn_editar.setText("Editar");
        btn_editar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_editarActionPerformed(evt);
            }
        });

        btn_save_cambios.setText("Guardar cambios");
        btn_save_cambios.setEnabled(false);
        btn_save_cambios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_save_cambiosActionPerformed(evt);
            }
        });

        btn_cancel_cambios.setText("Cancelar");
        btn_cancel_cambios.setEnabled(false);
        btn_cancel_cambios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancel_cambiosActionPerformed(evt);
            }
        });

        cbx_reloj_de_usuario.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Costa", "Cultura", "Maestranza", "Planta", "Seleccione..." }));
        cbx_reloj_de_usuario.setEnabled(false);

        jLabel7.setText("SECCION:");

        cbx_seccion.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbx_seccion.setEnabled(false);

        jLabel6.setText("Busqueda:");

        txt_busqueda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_busquedaKeyReleased(evt);
            }
        });

        pnl_checks.setLayout(new java.awt.GridBagLayout());

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(36, 36, 36)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txt_id, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(36, 36, 36))
                            .addComponent(txt_userid))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(9, 9, 9)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txt_cuil, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                            .addComponent(txt_name))
                        .addGap(38, 38, 38)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbx_seccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btn_save_cambios))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(18, 18, 18)
                                .addComponent(cbx_reloj_de_usuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_busqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(pnl_checks, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 642, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btn_cancelar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn_aceptar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn_importar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn_editar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn_cancel_cambios, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(txt_id, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3)
                        .addComponent(txt_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel5)
                        .addComponent(cbx_reloj_de_usuario, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txt_userid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel2))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel7)
                                .addComponent(cbx_seccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel4)
                                .addComponent(txt_cuil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(14, 14, 14))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btn_save_cambios)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txt_busqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btn_importar)
                        .addGap(21, 21, 21)
                        .addComponent(btn_editar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_cancel_cambios)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 114, Short.MAX_VALUE)
                        .addComponent(btn_aceptar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_cancelar)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(pnl_checks, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tablaMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaMouseReleased
        try {
            if (tabla.getSelectedRow() != -1) {
                userSeleccionado = listaTemp.get(tabla.getSelectedRow());
                cargarForm();
            }
        } catch (java.lang.IndexOutOfBoundsException e) {

            ContLogger contLogger = new ContLogger();
            String error = "018";
            Loggs loggs = new Loggs(new Timestamp(Calendar.getInstance().getTimeInMillis()),
                    Logger.getLogger(V_userinfo.class.getName()).getName(),
                    contLogger.getNombreTerminal(),
                    e.fillInStackTrace().toString(),
                    error);
            contLogger.insert(loggs);
            JOptionPane.showMessageDialog(null, "Error:" + error + "\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);

        }
    }//GEN-LAST:event_tablaMouseReleased

    private void btn_cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelarActionPerformed
        vaciarForm();
        cargarTabla(relojSelect);

        desabilitarBotonesImportar();
    }//GEN-LAST:event_btn_cancelarActionPerformed

    private void btn_importarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_importarActionPerformed
        vaciarForm();
        abilitarBotonesImportar();
        desabilitarBotonesEditar();

        file = new File("C:\\RELOJ_SYS\\CENTRALIZADO\\att2000.mdb");

        listaTemp = new ArrayList<>();

        for (Userinfo userinfo : contUserinfo.listar()) {
//            if (userinfo.getReloj().equals(cbx_reloj.getSelectedItem().toString())) {
            listaTemp.add(userinfo);
//            }
        }

        buscarUsuariosNuevos();
        jDialog1.dispose();
    }//GEN-LAST:event_btn_importarActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

        if (cbx_reloj.getSelectedItem().toString().equals("Planta")) {
            file = new File("C:\\RELOJ_SYS\\CENTRALIZADO\\att2000.mdb");
//            file = new File("C:\\RELOJ_SYS\\planta\\att2000.mdb");
        } else if (cbx_reloj.getSelectedItem().toString().equals("Costa")) {
            file = new File("C:\\RELOJ_SYS\\CENTRALIZADO\\att2000.mdb");
//            file = new File("C:\\RELOJ_SYS\\costa\\att2000.mdb");
        } else if (cbx_reloj.getSelectedItem().toString().equals("Cultura")) {
            file = new File("C:\\RELOJ_SYS\\CENTRALIZADO\\att2000.mdb");
//            file = new File("C:\\RELOJ_SYS\\cultura\\att2000.mdb");
        } else if (cbx_reloj.getSelectedItem().toString().equals("Maestranza")) {
            file = new File("C:\\RELOJ_SYS\\CENTRALIZADO\\att2000.mdb");
//            file = new File("C:\\RELOJ_SYS\\maestranza\\att2000.mdb");
        }

        listaTemp = new ArrayList<>();

        for (Userinfo userinfo : contUserinfo.listar()) {
//            if (userinfo.getReloj().equals(cbx_reloj.getSelectedItem().toString())) {
            listaTemp.add(userinfo);
//            }
        }

        buscarUsuariosNuevos();
        jDialog1.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void btn_aceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_aceptarActionPerformed
        if (JOptionPane.showConfirmDialog(null, "Confirmar operacion a realizar.") == 0) {
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            int i = 0;
            int cont = 0;
            for (Userinfo userinfo : nuevosUsuarios) {
                if (i < tabla.getRowCount()) {
                    if (tabla.getValueAt(i, 3).equals(true)) {
                        contUserinfo.create(userinfo);
                        cont++;
                    }
                    i++;
                }
            }

            btn_editar.setEnabled(true);

            JOptionPane.showMessageDialog(null, "Se han agregado " + cont + " usuarios nuevos.");
            cargarTabla();
            desabilitarBotonesImportar();

            lista = contUserinfo.listar();
            cargarTabla(relojSelect);

            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }//GEN-LAST:event_btn_aceptarActionPerformed

    private void btn_editarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_editarActionPerformed
        abilitarBotonesEditar();
        desabilitarBotonesImportar();
        btn_editar.setEnabled(false);
        otorgarPermisos();
    }//GEN-LAST:event_btn_editarActionPerformed

    private void btn_save_cambiosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_save_cambiosActionPerformed
        userSeleccionado.setName(txt_name.getText());
        userSeleccionado.setUserid(Long.parseLong(txt_userid.getText()));
        userSeleccionado.setCucupers(Long.parseLong(txt_cuil.getText()));

        if (cbx_seccion.getSelectedItem().equals("Seleccione...")) {
            userSeleccionado.setIdSeccion(0);
        } else {
            userSeleccionado.setIdSeccion(listaSecciones.get(cbx_seccion.getSelectedIndex()).getId());
        }
        if (!cbx_reloj_de_usuario.getSelectedItem().toString().equals("Seleccione...")) {
            userSeleccionado.setReloj(listaReloj.get(cbx_reloj_de_usuario.getSelectedIndex()).getId());

            userSeleccionado.setTerminal(contUserinfo.getNombreTerminal());
            userSeleccionado.setIp(contUserinfo.getIpTerminal());
            userSeleccionado.setUsuariosistema(usuarioSistema.getUsuario());
            userSeleccionado.setFechaauditoria(contUserinfo.getFechaServidor());
            System.out.println("USERSELECT: " + userSeleccionado.toString());
            if (contUserinfo.update(userSeleccionado) > 0) {
                JOptionPane.showMessageDialog(null, "Cambios guardados!");
            } else {
                JOptionPane.showMessageDialog(null, "Error al guardar los cambios", "Error!", JOptionPane.ERROR_MESSAGE);
            }

            desabilitarBotonesEditar();
            limpiarCampos();
            vaciarForm();
            otorgarPermisos();

            lista = contUserinfo.listar();
            cargarTabla(relojSelect);

        } else {
            JOptionPane.showMessageDialog(null, "Seleccione a donde pertenece");
        }

    }//GEN-LAST:event_btn_save_cambiosActionPerformed

    private void btn_cancel_cambiosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancel_cambiosActionPerformed
        desabilitarBotonesEditar();
        limpiarCampos();
        otorgarPermisos();
    }//GEN-LAST:event_btn_cancel_cambiosActionPerformed

    private void txt_cuilKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_cuilKeyTyped
        verificarIngresoDatos(evt);
    }//GEN-LAST:event_txt_cuilKeyTyped

    private void tablaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaKeyReleased
        try {
            if (tabla.getSelectedRow() != -1) {
                userSeleccionado = listaTemp.get(tabla.getSelectedRow());
                cargarForm();
            }
        } catch (java.lang.IndexOutOfBoundsException e) {

            ContLogger contLogger = new ContLogger();
            String error = "019";
            Loggs loggs = new Loggs(new Timestamp(Calendar.getInstance().getTimeInMillis()),
                    Logger.getLogger(V_userinfo.class.getName()).getName(),
                    contLogger.getNombreTerminal(),
                    e.fillInStackTrace().toString(),
                    error);
            contLogger.insert(loggs);
            JOptionPane.showMessageDialog(null, "Error:" + error + "\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);

        }
    }//GEN-LAST:event_tablaKeyReleased

    private void txt_busquedaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_busquedaKeyReleased
        listaTemp = new ArrayList<>();
        vaciarTabla();
        DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();

        for (Userinfo userinfo : lista) {
            if (userinfo.getName().toUpperCase().contains(txt_busqueda.getText().toUpperCase())) {
                listaTemp.add(userinfo);
            }
        }
//        listaTemp = lista;

        for (Userinfo userinfo : listaTemp) {
            Object[] datos = new Object[tabla.getColumnCount()];
            datos[0] = userinfo.getUserid();
            datos[1] = userinfo.getName();
            datos[2] = userinfo.getCucupers();

            modelo.addRow(datos);
        }

        tabla.setModel(modelo);
    }//GEN-LAST:event_txt_busquedaKeyReleased

    private void txt_useridKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_useridKeyTyped
        verificarIngresoDatos(evt);
    }//GEN-LAST:event_txt_useridKeyTyped

    private void verificarIngresoDatos(KeyEvent evt) {
        int key = evt.getKeyChar();

        if (key < 48 || key > 57) {
            if (!(key == 8 || key == 127)) {
                evt.consume();

                setAlwaysOnTop(false);
                JOptionPane.showMessageDialog(null, "No debe ingresar letras", "Advertencia", JOptionPane.WARNING_MESSAGE);
                setAlwaysOnTop(true);
            }
        }
    }

    private void buscarUsuariosNuevos() {
        if (file.getName().equals("att2000.mdb") || file.getName().equals("ATT2000.MDB")) {
            //Buscaremos los usuarios nuevos de att2000.mdb
            FichadaReloj2 f2 = new FichadaReloj2(file.getAbsolutePath());
            listaAux = f2.traerEmpleados();

//            System.out.println("Tamaño lista: " + lista.size());
            for (Userinfo u : listaAux) {
                boolean existe = false;
                for (Userinfo u1 : listaTemp) {

                    if (((u.getUserid() - u1.getUserid()) == 0 || u.getUserid() == u1.getUserid())/* && u.getReloj().equals(u1.getReloj())*/) {
                        existe = true;
                        break;

                    } else {
                    }
                }
                if (!existe) {

                    System.out.println("Usuario NO existente: " + u.toString());
                    nuevosUsuarios.add(u);
                }
            }

            JOptionPane.showMessageDialog(null, "Se han encontrado " + nuevosUsuarios.size() + " usuarios nuevos.");
            btn_editar.setEnabled(false);
            cargarTablaNuevos();
        }
    }

    private void cargarForm() {
        txt_id.setText(userSeleccionado.getId() + "");
        txt_cuil.setText(userSeleccionado.getCucupers() + "");
        txt_name.setText(userSeleccionado.getName());
        txt_userid.setText(userSeleccionado.getUserid() + "");
        System.err.println("RELOJ: " + userSeleccionado.getReloj());
        if (userSeleccionado.getReloj() == 0) {
            cbx_reloj_de_usuario.setSelectedIndex(4);
        } else {
            cbx_reloj_de_usuario.setSelectedItem(contReloj.encontrarPorId(userSeleccionado.getReloj()).getNombre());
        }

        if (userSeleccionado.getIdSeccion() == 0) {
            cbx_seccion.setSelectedIndex(listaSecciones.size());
        } else {
            cbx_seccion.setSelectedItem(contSeccion.encontrar(userSeleccionado.getIdSeccion()).getDetalle());
        }

    }

    private void vaciarForm() {
        listaAux = new ArrayList<>();
        nuevosUsuarios = new ArrayList<>();

        txt_id.setText("");
        txt_cuil.setText("");
        txt_name.setText("");
        txt_userid.setText("");
        cbx_reloj_de_usuario.setSelectedItem("Seleccione...");

    }

    private void limpiarCampos() {
        txt_id.setText("");
        txt_cuil.setText("");
        txt_name.setText("");
        txt_userid.setText("");
        cbx_reloj_de_usuario.setSelectedItem("Seleccione...");
    }

    private void abilitarBotonesImportar() {
        btn_aceptar.setEnabled(true);
        btn_cancelar.setEnabled(true);
        btn_editar.setEnabled(false);
    }

    private void desabilitarBotonesImportar() {
        btn_aceptar.setEnabled(false);
        btn_cancelar.setEnabled(false);
        btn_editar.setEnabled(true);
    }

    private void abilitarBotonesEditar() {
        btn_cancel_cambios.setEnabled(true);
        btn_save_cambios.setEnabled(true);

        txt_cuil.setEnabled(true);
        txt_name.setEnabled(true);
        txt_userid.setEnabled(true);

        btn_importar.setEnabled(false);
        cbx_reloj_de_usuario.setEnabled(true);
        cbx_seccion.setEnabled(true);

        switch (usuarioSistema.getTipo()) {
            case "admin":
                txt_userid.setEnabled(true);
                break;
            default:
                txt_userid.setEnabled(false);
                break;
        }

    }

    private void desabilitarBotonesEditar() {
        btn_cancel_cambios.setEnabled(false);
        btn_save_cambios.setEnabled(false);

        txt_cuil.setEnabled(false);
        txt_name.setEnabled(false);
        txt_userid.setEnabled(false);

        btn_importar.setEnabled(true);//es true
        btn_editar.setEnabled(true);
        cbx_reloj_de_usuario.setEnabled(false);
        cbx_seccion.setEnabled(false);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(V_userinfo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(V_userinfo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(V_userinfo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(V_userinfo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                V_userinfo dialog = new V_userinfo(new javax.swing.JFrame(), true, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_aceptar;
    private javax.swing.JButton btn_cancel_cambios;
    private javax.swing.JButton btn_cancelar;
    private javax.swing.JButton btn_editar;
    private javax.swing.JButton btn_importar;
    private javax.swing.JButton btn_save_cambios;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox cbx_reloj;
    private javax.swing.JComboBox cbx_reloj_de_usuario;
    private javax.swing.JComboBox cbx_seccion;
    private javax.swing.JButton jButton2;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JPanel pnl_checks;
    private javax.swing.JTable tabla;
    private javax.swing.JTextField txt_busqueda;
    private javax.swing.JTextField txt_cuil;
    private javax.swing.JTextField txt_id;
    private javax.swing.JTextField txt_name;
    private javax.swing.JTextField txt_userid;
    // End of variables declaration//GEN-END:variables
}
