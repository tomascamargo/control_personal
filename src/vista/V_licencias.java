/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.BDSentencias;
import controlador.ContLicencia;
import controlador.ContLicenciaEmpleado;
import controlador.ContUserinfo;
import controlador.ControladorGeneral;
import java.awt.Cursor;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.Licencia;
import modelo.LicenciaEmpleado;
import modelo.Userinfo;
import modelo.Usuario;

/**
 *
 * @author Viktor
 */
public class V_licencias extends javax.swing.JDialog {

    /**
     * Creates new form V_licencias
     */
    List<Userinfo> listaEmpleados = new ArrayList<>();
    List<Licencia> listaLicencia = new ArrayList<>();
    ControladorGeneral controladorGeneral = new ControladorGeneral();
    ContLicencia contLicencia = new ContLicencia();

    List<LicenciaEmpleado> listaLicEmp = new ArrayList<>();
    ContLicenciaEmpleado contLicenciaEmpleado = new ContLicenciaEmpleado();

    LicenciaEmpleado licCortar = new LicenciaEmpleado();
    Usuario usuario = null;

    public V_licencias(java.awt.Frame parent, boolean modal, Usuario usuario) {
        super(parent, modal);
        initComponents();
        cargarComboEmpleados();
        cargarComboLicencias();
        vaciarTabla();
        
        jLabel8.setVisible(false);
        txt_cant_dias.setVisible(false);
        this.usuario = usuario;
    }

    private void vaciarTabla() {
        tabla.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{
                    "Desde", "Hasta", "Detalle"
                }
        ));
        jScrollPane1.setViewportView(tabla);
        if (tabla.getColumnModel().getColumnCount() > 0) {
            tabla.getColumnModel().getColumn(0).setMinWidth(80);
            tabla.getColumnModel().getColumn(0).setPreferredWidth(80);
            tabla.getColumnModel().getColumn(0).setMaxWidth(80);
            tabla.getColumnModel().getColumn(1).setMinWidth(80);
            tabla.getColumnModel().getColumn(1).setPreferredWidth(80);
            tabla.getColumnModel().getColumn(1).setMaxWidth(80);
        }
    }

    private void cargarComboLicencias() {
        listaLicencia = contLicencia.findAll();
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();

        for (Licencia l : listaLicencia) {
            modelo.addElement(l.getDetalle());
        }

        cbx_licencias.setModel(modelo);
        
        if(cbx_licencias.getSelectedItem().toString().equals("Anual")){
            txt_detalle.setEnabled(true);
            Calendar c = Calendar.getInstance();
            if(c.get(Calendar.MONTH) == 12 && c.get(Calendar.DAY_OF_MONTH) == 1 ){
                txt_detalle.setText(c.get(Calendar.YEAR) + "");
            }else{
                txt_detalle.setText(c.get(Calendar.YEAR)-1 + "");
            }
            
        }
    }

    private void cargarComboEmpleados() {
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();

        ContUserinfo contEmpleado = new ContUserinfo();
        listaEmpleados = contEmpleado.listar();//listaEmpleados();
        for (Userinfo e : listaEmpleados) {
            modelo.addElement(e.getName());
        }
        cbx_empleads.setModel(modelo);
    }

    private void cargarTabla(Userinfo e) {
        vaciarTabla();
        DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();

        listaLicEmp = contLicenciaEmpleado.buscar(e);
        for (LicenciaEmpleado le : listaLicEmp) {
            Object[] datos = new Object[tabla.getColumnCount()];
            datos[0] = le.getFecha_inicio().toString();
            datos[1] = le.getFecha_fin().toString();
            if (le.getDetalle() == null) {
                for (Licencia l : listaLicencia) {
                    if (l.getId() == le.getLicencia()) {
                        datos[2] = l.getDetalle();
                    }
                }
            } else {
                for (Licencia l : listaLicencia) {
                    if (l.getId() == le.getLicencia()) {
                        if (le.getDetalle() != null) {
                            datos[2] = l.getDetalle() + ": " + le.getDetalle();
                        } else {
                            datos[2] = l.getDetalle();
                        }
                    }
                }
            }

            modelo.addRow(datos);
        }

        tabla.setModel(modelo);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        corte = new datechooser.beans.DateChooserCombo();
        jButton4 = new javax.swing.JButton();
        jDialog2 = new javax.swing.JDialog();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        txt_nombre = new javax.swing.JTextField();
        btn_aceptar = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        jList2 = new javax.swing.JList();
        jSeparator1 = new javax.swing.JSeparator();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        desde = new datechooser.beans.DateChooserCombo();
        jLabel2 = new javax.swing.JLabel();
        hasta = new datechooser.beans.DateChooserCombo();
        jLabel3 = new javax.swing.JLabel();
        cbx_empleads = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        btn_cortar_lic = new javax.swing.JButton();
        btn_guardar_licencia = new javax.swing.JButton();
        btn_cancelar = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        cbx_licencias = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        txt_detalle = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txt_cant_dias = new javax.swing.JTextField();
        btn_Eliminar = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        jDialog1.setTitle("Corte");
        jDialog1.setIconImage(Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("Iconos/bicentenario90.png")));

        jLabel4.setText("Fecha de corte de licencia");

        corte.setCurrentView(new datechooser.view.appearance.AppearancesList("Grey",
            new datechooser.view.appearance.ViewAppearance("custom",
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(0, 0, 0),
                    new java.awt.Color(0, 0, 255),
                    false,
                    true,
                    new datechooser.view.appearance.swing.ButtonPainter()),
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(0, 0, 0),
                    new java.awt.Color(0, 0, 255),
                    true,
                    true,
                    new datechooser.view.appearance.swing.ButtonPainter()),
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(0, 0, 255),
                    new java.awt.Color(0, 0, 255),
                    false,
                    true,
                    new datechooser.view.appearance.swing.ButtonPainter()),
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(128, 128, 128),
                    new java.awt.Color(0, 0, 255),
                    false,
                    true,
                    new datechooser.view.appearance.swing.LabelPainter()),
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(0, 0, 0),
                    new java.awt.Color(0, 0, 255),
                    false,
                    true,
                    new datechooser.view.appearance.swing.LabelPainter()),
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(0, 0, 0),
                    new java.awt.Color(255, 0, 0),
                    false,
                    false,
                    new datechooser.view.appearance.swing.ButtonPainter()),
                (datechooser.view.BackRenderer)null,
                false,
                true)));

    jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/OK2.png"))); // NOI18N
    jButton4.setText("Aceptar");
    jButton4.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jButton4ActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
    jPanel2.setLayout(jPanel2Layout);
    jPanel2Layout.setHorizontalGroup(
        jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel2Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(jLabel4)
                .addComponent(corte, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
    jPanel2Layout.setVerticalGroup(
        jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel2Layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(jLabel4)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(corte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jButton4)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
    jDialog1.getContentPane().setLayout(jDialog1Layout);
    jDialog1Layout.setHorizontalGroup(
        jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGap(0, 400, Short.MAX_VALUE)
        .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE)))
    );
    jDialog1Layout.setVerticalGroup(
        jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGap(0, 300, Short.MAX_VALUE)
        .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE)))
    );

    jDialog2.setTitle("Tipos de licencias");
    jDialog2.setIconImage(Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("Iconos/bicentenario90.png")));

    jPanel3.setBackground(new java.awt.Color(204, 204, 204));
    jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

    jPanel4.setBackground(new java.awt.Color(153, 153, 153));

    jLabel6.setText("Nombre licencia:");

    txt_nombre.setEnabled(false);

    btn_aceptar.setText("Aceptar");
    btn_aceptar.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btn_aceptarActionPerformed(evt);
        }
    });

    jList2.setModel(new javax.swing.AbstractListModel() {
        String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
        public int getSize() { return strings.length; }
        public Object getElementAt(int i) { return strings[i]; }
    });
    jScrollPane3.setViewportView(jList2);

    javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
    jPanel4.setLayout(jPanel4Layout);
    jPanel4Layout.setHorizontalGroup(
        jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                .addComponent(jSeparator1)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel4Layout.createSequentialGroup()
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btn_aceptar)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addGap(141, 141, 141)))
                        .addComponent(txt_nombre, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addContainerGap())
    );
    jPanel4Layout.setVerticalGroup(
        jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 117, Short.MAX_VALUE)
            .addGap(18, 18, 18)
            .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(18, 18, 18)
            .addComponent(jLabel6)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(txt_nombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(25, 25, 25)
            .addComponent(btn_aceptar)
            .addContainerGap())
    );

    jButton2.setText("Nuevo");
    jButton2.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jButton2ActionPerformed(evt);
        }
    });

    jButton3.setText("Editar");
    jButton3.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jButton3ActionPerformed(evt);
        }
    });

    jButton5.setText("Cancelar");

    javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
    jPanel3.setLayout(jPanel3Layout);
    jPanel3Layout.setHorizontalGroup(
        jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel3Layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(18, 18, 18)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jButton5)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton2, javax.swing.GroupLayout.Alignment.TRAILING)))
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
    jPanel3Layout.setVerticalGroup(
        jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel3Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap())
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addComponent(jButton2)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jButton3)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton5)
                    .addGap(22, 22, 22))))
    );

    javax.swing.GroupLayout jDialog2Layout = new javax.swing.GroupLayout(jDialog2.getContentPane());
    jDialog2.getContentPane().setLayout(jDialog2Layout);
    jDialog2Layout.setHorizontalGroup(
        jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    );
    jDialog2Layout.setVerticalGroup(
        jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    );

    setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
    setTitle("Administracion de Licencias");
    setIconImage(Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("Iconos/bicentenario90.png")));

    jPanel1.setBackground(new java.awt.Color(204, 204, 204));
    jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

    jLabel1.setText("Desde:");

    desde.setCurrentView(new datechooser.view.appearance.AppearancesList("Grey",
        new datechooser.view.appearance.ViewAppearance("custom",
            new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                new java.awt.Color(0, 0, 0),
                new java.awt.Color(0, 0, 255),
                false,
                true,
                new datechooser.view.appearance.swing.ButtonPainter()),
            new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                new java.awt.Color(0, 0, 0),
                new java.awt.Color(0, 0, 255),
                true,
                true,
                new datechooser.view.appearance.swing.ButtonPainter()),
            new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                new java.awt.Color(0, 0, 255),
                new java.awt.Color(0, 0, 255),
                false,
                true,
                new datechooser.view.appearance.swing.ButtonPainter()),
            new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                new java.awt.Color(128, 128, 128),
                new java.awt.Color(0, 0, 255),
                false,
                true,
                new datechooser.view.appearance.swing.LabelPainter()),
            new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                new java.awt.Color(0, 0, 0),
                new java.awt.Color(0, 0, 255),
                false,
                true,
                new datechooser.view.appearance.swing.LabelPainter()),
            new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                new java.awt.Color(0, 0, 0),
                new java.awt.Color(255, 0, 0),
                false,
                false,
                new datechooser.view.appearance.swing.ButtonPainter()),
            (datechooser.view.BackRenderer)null,
            false,
            true)));

jLabel2.setText("Hasta:");

hasta.setCurrentView(new datechooser.view.appearance.AppearancesList("Grey",
    new datechooser.view.appearance.ViewAppearance("custom",
        new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
            new java.awt.Color(0, 0, 0),
            new java.awt.Color(0, 0, 255),
            false,
            true,
            new datechooser.view.appearance.swing.ButtonPainter()),
        new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
            new java.awt.Color(0, 0, 0),
            new java.awt.Color(0, 0, 255),
            true,
            true,
            new datechooser.view.appearance.swing.ButtonPainter()),
        new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
            new java.awt.Color(0, 0, 255),
            new java.awt.Color(0, 0, 255),
            false,
            true,
            new datechooser.view.appearance.swing.ButtonPainter()),
        new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
            new java.awt.Color(128, 128, 128),
            new java.awt.Color(0, 0, 255),
            false,
            true,
            new datechooser.view.appearance.swing.LabelPainter()),
        new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
            new java.awt.Color(0, 0, 0),
            new java.awt.Color(0, 0, 255),
            false,
            true,
            new datechooser.view.appearance.swing.LabelPainter()),
        new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
            new java.awt.Color(0, 0, 0),
            new java.awt.Color(255, 0, 0),
            false,
            false,
            new datechooser.view.appearance.swing.ButtonPainter()),
        (datechooser.view.BackRenderer)null,
        false,
        true)));

jLabel3.setText("Empleado:");

cbx_empleads.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
cbx_empleads.addActionListener(new java.awt.event.ActionListener() {
public void actionPerformed(java.awt.event.ActionEvent evt) {
    cbx_empleadsActionPerformed(evt);
    }
    });

    tabla.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][] {
            {null, null, null},
            {null, null, null},
            {null, null, null},
            {null, null, null}
        },
        new String [] {
            "Desde", "Hasta", "Detalle"
        }
    ) {
        boolean[] canEdit = new boolean [] {
            false, false, false
        };

        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit [columnIndex];
        }
    });
    jScrollPane1.setViewportView(tabla);
    if (tabla.getColumnModel().getColumnCount() > 0) {
        tabla.getColumnModel().getColumn(0).setMinWidth(80);
        tabla.getColumnModel().getColumn(0).setPreferredWidth(80);
        tabla.getColumnModel().getColumn(0).setMaxWidth(80);
        tabla.getColumnModel().getColumn(1).setMinWidth(80);
        tabla.getColumnModel().getColumn(1).setPreferredWidth(80);
        tabla.getColumnModel().getColumn(1).setMaxWidth(80);
    }

    btn_cortar_lic.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Estadisticas.png"))); // NOI18N
    btn_cortar_lic.setText("Cortar Licencia");
    btn_cortar_lic.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btn_cortar_licActionPerformed(evt);
        }
    });

    btn_guardar_licencia.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/OK2.png"))); // NOI18N
    btn_guardar_licencia.setText("Guardar Licencia");
    btn_guardar_licencia.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btn_guardar_licenciaActionPerformed(evt);
        }
    });

    btn_cancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/No.png"))); // NOI18N
    btn_cancelar.setText("Cancelar");
    btn_cancelar.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btn_cancelarActionPerformed(evt);
        }
    });

    jLabel5.setText("Licencia:");

    cbx_licencias.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
    cbx_licencias.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            cbx_licenciasActionPerformed(evt);
        }
    });

    jLabel7.setText("Detalle:");

    txt_detalle.setEnabled(false);

    jLabel8.setText("Cant días:");

    txt_cant_dias.setText("0");
    txt_cant_dias.setEnabled(false);
    txt_cant_dias.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyReleased(java.awt.event.KeyEvent evt) {
            txt_cant_diasKeyReleased(evt);
        }
        public void keyTyped(java.awt.event.KeyEvent evt) {
            txt_cant_diasKeyTyped(evt);
        }
    });

    btn_Eliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/delete.png"))); // NOI18N
    btn_Eliminar.setText("Eliminar Licencia");
    btn_Eliminar.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btn_EliminarActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel3)
                        .addComponent(jLabel1)
                        .addComponent(jLabel5))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbx_empleads, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(desde, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cbx_licencias, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(18, 18, 18)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel7)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txt_detalle))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel8)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txt_cant_dias, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(jLabel2)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(hasta, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(0, 0, Short.MAX_VALUE))))))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                    .addComponent(btn_cortar_lic, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btn_Eliminar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btn_guardar_licencia, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btn_cancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addContainerGap())
    );
    jPanel1Layout.setVerticalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel3)
                .addComponent(cbx_empleads, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(18, 18, 18)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(cbx_licencias, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel7)
                        .addComponent(txt_detalle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(8, 8, 8)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(desde, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel1)))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(hasta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel2)))))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txt_cant_dias, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btn_guardar_licencia)
                .addComponent(btn_cortar_lic)
                .addComponent(btn_cancelar)
                .addComponent(btn_Eliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(120, 120, 120))
    );

    jMenu1.setText(" Archivo");

    jMenuItem1.setText(" Licencias");
    jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jMenuItem1ActionPerformed(evt);
        }
    });
    jMenu1.add(jMenuItem1);

    jMenuBar1.add(jMenu1);

    setJMenuBar(jMenuBar1);

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    );
    layout.setVerticalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    );

    pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_cortar_licActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cortar_licActionPerformed

        if (tabla.getSelectedRow() != -1) {
            licCortar = listaLicEmp.get(tabla.getSelectedRow());
            Calendar c= Calendar.getInstance();
            c.setTimeInMillis(licCortar.getFecha_fin().getTime());
            corte.setSelectedDate(c);
            jDialog1.setModal(true);
            jDialog1.setSize(300, 200);
            jDialog1.setLocationRelativeTo(this);
            jDialog1.setVisible(true);

        } else {
            JOptionPane.showMessageDialog(null, "Seleccione una fila de la tabla.");
        }
    }//GEN-LAST:event_btn_cortar_licActionPerformed

    private void cbx_empleadsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbx_empleadsActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        cargarTabla(listaEmpleados.get(cbx_empleads.getSelectedIndex()));
//        System.out.println("id de empleado: "+listaEmpleados.get(cbx_empleads.getSelectedIndex()).getId()+" USERID: "+listaEmpleados.get(cbx_empleads.getSelectedIndex()).getUserid());
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_cbx_empleadsActionPerformed

    private void btn_guardar_licenciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_guardar_licenciaActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

        Timestamp fecha1 = new Timestamp(desde.getSelectedDate().getTimeInMillis());
        Timestamp fecha2 = new Timestamp(hasta.getSelectedDate().getTimeInMillis());

        Date f1 = new Date(fecha1.getTime());
        Date f2 = new Date(fecha2.getTime());

        ContLicenciaEmpleado contLicenciaEmpleado = new ContLicenciaEmpleado();
        Userinfo e = listaEmpleados.get(cbx_empleads.getSelectedIndex());
        LicenciaEmpleado licenciaEmpleado = new LicenciaEmpleado();

        licenciaEmpleado.setUserinfoid(e.getId());
        licenciaEmpleado.setFecha_inicio(f1);
        licenciaEmpleado.setFecha_fin(f2);
        licenciaEmpleado.setLicencia(listaLicencia.get(cbx_licencias.getSelectedIndex()).getId());
        licenciaEmpleado.setJustificacionfalta(false);
        licenciaEmpleado.setTerminal(contLicenciaEmpleado.getNombreTerminal());
        licenciaEmpleado.setIp(contLicenciaEmpleado.getIpTerminal());
        licenciaEmpleado.setUsuariosistema(usuario.getUsuario());
        licenciaEmpleado.setFechaauditoria(contLicenciaEmpleado.getFechaServidor());
        switch (cbx_licencias.getSelectedItem().toString()) {
            case "Otros":
            case "Anual":
                licenciaEmpleado.setDetalle(txt_detalle.getText());
                break;
            default:
                licenciaEmpleado.setDetalle("");
        }
        if (cbx_licencias.getSelectedItem().toString().equals("Anual") && contLicencia.existe(licenciaEmpleado)) {
            JOptionPane.showMessageDialog(null, "No se puede tomar dos licencias anuales en el mismo año", "", JOptionPane.ERROR_MESSAGE);
        } else {
            contLicenciaEmpleado.create(licenciaEmpleado);

            cargarTabla(e);
            JOptionPane.showMessageDialog(null, "Guardado.");
        }

        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_btn_guardar_licenciaActionPerformed

    private void btn_cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelarActionPerformed
        dispose();
    }//GEN-LAST:event_btn_cancelarActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

        Timestamp corteTimestamp = new Timestamp(corte.getSelectedDate().getTimeInMillis());
        Date corteDate = new Date(corteTimestamp.getTime());

        licCortar.setFecha_fin(corteDate);
        licCortar.setTerminal(contLicenciaEmpleado.getNombreTerminal());
        licCortar.setIp(contLicenciaEmpleado.getIpTerminal());
        licCortar.setUsuariosistema(usuario.getUsuario());
        licCortar.setFechaauditoria(contLicenciaEmpleado.getFechaServidor());

        contLicenciaEmpleado.update(licCortar);

        JOptionPane.showMessageDialog(null, "Modificado.");
        cargarTabla(listaEmpleados.get(cbx_empleads.getSelectedIndex()));

        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

        initJDialog();

        jDialog2.setModal(true);
        jDialog2.setSize(500, 450);
        jDialog2.setLocationRelativeTo(this);
        jDialog2.setVisible(true);

        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        txt_nombre.setEnabled(true);
        txt_nombre.requestFocus();//pone el foco
        txt_nombre.setText("");
        nuevo = true;
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        if (jList2.getSelectedIndex() != -1) {
            txt_nombre.setEnabled(true);
            txt_nombre.requestFocus();//pone el foco
            nuevo = false;

            txt_nombre.setText(listaLicencia.get(jList2.getSelectedIndex()).getDetalle());
            licenciaModificar = listaLicencia.get(jList2.getSelectedIndex());
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione un item de la lista.");
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void btn_aceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_aceptarActionPerformed
        if (nuevo) {
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

            Licencia l = new Licencia();
            l.setDescuento(false);
            l.setDetalle(txt_nombre.getText());

            if (contLicencia.create(l) != 0) {
                JOptionPane.showMessageDialog(null, "Guardado con éxito.");
                txt_nombre.setText("");
                initJDialog();
            } else {
                JOptionPane.showMessageDialog(null, "Error.", "Error", JOptionPane.ERROR_MESSAGE);
                initJDialog();
            }

            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        } else {
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

            licenciaModificar.setDetalle(txt_nombre.getText());

            if (contLicencia.update(licenciaModificar) != 0) {
                JOptionPane.showMessageDialog(null, "Modificado con éxito.");
                txt_nombre.setText("");
                initJDialog();
            } else {
                JOptionPane.showMessageDialog(null, "Error.", "Error", JOptionPane.ERROR_MESSAGE);
                initJDialog();
            }

            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }//GEN-LAST:event_btn_aceptarActionPerformed

    private void cbx_licenciasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbx_licenciasActionPerformed
        if (cbx_licencias.getSelectedItem().toString().equals("Otros") || cbx_licencias.getSelectedItem().toString().equals("Anual")) {
                if(cbx_licencias.getSelectedItem().toString().equals("Anual")){
                    Calendar c = Calendar.getInstance();
                    txt_detalle.setText(c.get(Calendar.YEAR) + "");
                }
            txt_detalle.setEnabled(true);
        } else {
            txt_detalle.setText("");
            txt_detalle.setEnabled(false);
        }
    }//GEN-LAST:event_cbx_licenciasActionPerformed

    private void txt_cant_diasKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_cant_diasKeyTyped
        validarSoloNumeros(evt);
    }//GEN-LAST:event_txt_cant_diasKeyTyped

    private void txt_cant_diasKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_cant_diasKeyReleased
        
        if (!txt_cant_dias.getText().equals("")) {
            hasta.setEnabled(true);
            hasta.setSelectedDate(sumarDias(desde.getSelectedDate(), Integer.parseInt(txt_cant_dias.getText())));
            hasta.setEnabled(false);
        }
    }//GEN-LAST:event_txt_cant_diasKeyReleased

    private void btn_EliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_EliminarActionPerformed
        if (tabla.getSelectedRow() != -1) {
            int operacion = JOptionPane.showConfirmDialog(this, "¿Esta Seguro de eliminar el registro?", "Confirmar", JOptionPane.OK_CANCEL_OPTION);
            if(operacion == 0){
                setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                
                licCortar = listaLicEmp.get(tabla.getSelectedRow());
                licCortar.setTerminal(contLicenciaEmpleado.getNombreTerminal());
                licCortar.setIp(contLicenciaEmpleado.getIpTerminal());
                licCortar.setUsuariosistema(usuario.getUsuario());
                licCortar.setFechaauditoria(contLicenciaEmpleado.getFechaServidor());
                contLicenciaEmpleado.delete(licCortar); 
                
                JOptionPane.showMessageDialog(null, "Eliminado.");
                cargarTabla(listaEmpleados.get(cbx_empleads.getSelectedIndex()));

                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                
            }
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione una fila de la tabla.");
        }
    }//GEN-LAST:event_btn_EliminarActionPerformed

    private Calendar sumarDias(Calendar fecha, int cantidadDias){
        
        fecha.add(Calendar.DAY_OF_MONTH, cantidadDias);
        
        return  fecha;
    }
    
    private void validarSoloNumeros(KeyEvent evt) {
        char c = evt.getKeyChar();
        if ((c < '0' || c > '9')) {
            Toolkit.getDefaultToolkit().beep();
            evt.consume();
        }
    }

    //JDIALOG2
    boolean nuevo = true;
    Licencia licenciaModificar;

    private void initJDialog() {
        cargarComboLicencias();
        cargarLicecencias();
    }

    private void cargarLicecencias() {
        DefaultListModel modelo = new DefaultListModel();
        for (Licencia l : listaLicencia) {
            modelo.addElement(l.getDetalle());
        }

        jList2.setModel(modelo);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(V_licencias.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(V_licencias.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(V_licencias.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(V_licencias.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                V_licencias dialog = new V_licencias(new javax.swing.JFrame(), true, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_Eliminar;
    private javax.swing.JButton btn_aceptar;
    private javax.swing.JButton btn_cancelar;
    private javax.swing.JButton btn_cortar_lic;
    private javax.swing.JButton btn_guardar_licencia;
    private javax.swing.JComboBox cbx_empleads;
    private javax.swing.JComboBox cbx_licencias;
    private datechooser.beans.DateChooserCombo corte;
    private datechooser.beans.DateChooserCombo desde;
    private datechooser.beans.DateChooserCombo hasta;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JDialog jDialog2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JList jList2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable tabla;
    private javax.swing.JTextField txt_cant_dias;
    private javax.swing.JTextField txt_detalle;
    private javax.swing.JTextField txt_nombre;
    // End of variables declaration//GEN-END:variables

}
