/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.BDSentencias;
import controlador.ContArea;
import controlador.ContFeriado;
import controlador.ContFichada;
import controlador.ContFormatoCeldasTbl;
import controlador.ContLicenciaEmpleado;
import controlador.ContLogger;
import controlador.ContPersonaGrupo;
import controlador.ContReloj;
import controlador.ContSeccion;
import controlador.ContUserinfo;
import controlador.ContUsuario;
import controlador.ControladorGeneral;
import controlador.ControladorReporte;
import controlador.fichadas.ReporteExcel;
import controlador.fichadas.Utilidades;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.ListModel;
import javax.swing.table.DefaultTableModel;
import modelo.Area;
import modelo.Feriado;
import modelo.Fichada;
import modelo.LicenciaEmpleado;
import modelo.Loggs;
import modelo.Reloj;
import modelo.Seccion;
import modelo.Userinfo;
import modelo.Usuario;
import reporte.ReporteFichada;

/**
 *
 * @author Viktor
 */
public class V_ver_fichada extends javax.swing.JDialog {

    /**
     * Creates new form V_ver_fichada
     */
    ControladorGeneral controladorGeneral;
    ContUserinfo contEmpleado;

    List<Userinfo> listaEmpleadosTemp;
    List<Userinfo> listaEmpleado;
    List<Seccion> listaSecciones;
    Userinfo empleadoSeleccionado;
    List<Object[]> listaFichadaDia;
    List<Feriado> listaFeriadoMes;
    List<LicenciaEmpleado> listaLicencias;
    List<Area> listaAreas;
    List<Reloj> listaReloj;
    List<Userinfo> listaEmpleadosTempBusqueda = new ArrayList<>();
    ContFormatoCeldasTbl formatoCeldas;

    Utilidades u;
    Userinfo empleado;
    Usuario usuarioSistema;
    String fechaFila = "";
    ContFichada contFichada = new ContFichada();
    ContFeriado contFeriado = new ContFeriado();
    ContSeccion contSeccion = new ContSeccion();
    ContUsuario contUsuario = new ContUsuario();
    ContReloj contReloj = new ContReloj();
    ContLicenciaEmpleado contLicenciaEmpleado = new ContLicenciaEmpleado();
    String horaFila = "";
    Fichada fichadaFila = null;

    String accion = "vista previa";
    boolean radiografia = false;
    boolean busquedaActiva = false;

    public V_ver_fichada(java.awt.Frame parent, boolean modal, Usuario usuario) {
        super(parent, modal);
        this.usuarioSistema = usuario;

        initComponents();

        u = new Utilidades();
        contEmpleado = new ContUserinfo();
        controladorGeneral = new ControladorGeneral();
        listaEmpleadosTemp = new ArrayList<>();
        listaEmpleado = new ArrayList<>();
        listaLicencias = new ArrayList<>();
        listaAreas = new ArrayList<>();
        listaReloj = new ArrayList<>();
//        fichada = new ArrayList<>();
        listaFichadaDia = new ArrayList<>();
        formatoCeldas = new ContFormatoCeldasTbl();

        ///////////////////////////////////
        anioActual();

        cargarJList();
        desactivarPanel1();
        desactivarPanel2();
        vaciarTabla();

        jTable1.setDefaultRenderer(Object.class, formatoCeldas);

        otorgarPermisos();
        deshabilitarBotones();
        cargarItemMenus();

        establecerSelecciones();

        btn_listar.setEnabled(false);

    }

    private void establecerSelecciones() {
        checkMes.setSelected(true);
        activarPanel1();
        desactivarPanel2();

        if (list_empleados.getSelectedIndex() != -1) {
            if (checkMes.isSelected() || checkFechas.isSelected()) {
                deshabilitarBotones();
            }
        }

        Calendar c = Calendar.getInstance();
        switch (c.get(Calendar.MONTH)) {
            case 0:
                cbx_mes.setSelectedItem("Enero");
                break;
            case 2:
                cbx_mes.setSelectedItem("Febrero");
                break;
            case 3:
                cbx_mes.setSelectedItem("Marzo");
                break;
            case 4:
                cbx_mes.setSelectedItem("Abril");
                break;
            case 5:
                cbx_mes.setSelectedItem("Mayo");
                break;
            case 6:
                cbx_mes.setSelectedItem("Junio");
                break;
            case 7:
                cbx_mes.setSelectedItem("Julio");
                break;
            case 8:
                cbx_mes.setSelectedItem("Septiembre");
                break;
            case 9:
                cbx_mes.setSelectedItem("Octubre");
                break;
            case 10:
                cbx_mes.setSelectedItem("Nobiembre");
                break;
            case 11:
                cbx_mes.setSelectedItem("Diciembre");
                break;
            default:
                break;
        }
        
        list_empleados.requestFocus();
        list_empleados.setSelectedIndex(0);
        
        if (list_empleados.getSelectedIndex() != -1) {
            if (listaEmpleadosTemp.get(list_empleados.getSelectedIndex()) != null) {
                empleadoSeleccionado = listaEmpleadosTemp.get(list_empleados.getSelectedIndex());
                if (checkMes.isSelected() || checkFechas.isSelected()) {
                    deshabilitarBotones();
                    vaciarTabla();
                }
            }
        }
        
        
    }

    private void cargarItemMenus() {
        listaReloj = contReloj.encontrarTodos();
        for (Reloj r : listaReloj) {
            final String r2 = r.getNombre();
            JMenuItem mni_menu = new JMenuItem(r.getNombre());
            mni_menu.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    cargarJList(r2);
                    lbl_origen.setText("Reloj: " + r2);
                }
            });
            jmn_relojes.add(mni_menu);
        }

        JSeparator separador = new JSeparator(0);
        jmn_relojes.add(separador);

        JMenuItem mni_sin_reloj_especifico = new JMenuItem("Sin reloj");
        mni_sin_reloj_especifico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cargarJList("");
                lbl_origen.setText("Reloj: Sin reloj definido");
            }
        });
        jmn_relojes.add(mni_sin_reloj_especifico);

        JMenuItem mni_todos = new JMenuItem("Todos");
        mni_todos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cargarJList("Todos");
                lbl_origen.setText("Reloj: Todos");
            }
        });
        jmn_relojes.add(mni_todos);
    }

    private void otorgarPermisos() {
        switch (usuarioSistema.getTipo()) {
            case "admin":
                mni_agregar_licencia_temporal.setEnabled(true);
                mni_eliminar_hora.setEnabled(true);
                mni_insercion_multiple.setEnabled(true);
                mni_insertar_hora.setEnabled(true);
                mni_justificar.setEnabled(true);
                mni_ver_detalle_fichada.setEnabled(true);

                mni_agregar_licencia_temporal1.setEnabled(true);
                mni_eliminar_hora1.setEnabled(true);
                mni_insercion_multiple1.setEnabled(true);
                mni_insertar_hora1.setEnabled(true);
                mni_justificar1.setEnabled(true);
                mni_ver_detalle_fichada1.setEnabled(true);

                jmn_relojes.setEnabled(true);
                break;
            case "supervisor":
                mni_agregar_licencia_temporal.setEnabled(true);
                mni_eliminar_hora.setEnabled(true);
                mni_insercion_multiple.setEnabled(true);
                mni_insertar_hora.setEnabled(true);
                mni_justificar.setEnabled(true);
                mni_ver_detalle_fichada.setEnabled(true);

                mni_agregar_licencia_temporal1.setEnabled(true);
                mni_eliminar_hora1.setEnabled(true);
                mni_insercion_multiple1.setEnabled(true);
                mni_insertar_hora1.setEnabled(true);
                mni_justificar1.setEnabled(true);
                mni_ver_detalle_fichada1.setEnabled(true);

                jmn_relojes.setEnabled(true);
                break;
            case "consulta":
                mni_agregar_licencia_temporal.setEnabled(false);
                mni_eliminar_hora.setEnabled(false);
                mni_insercion_multiple.setEnabled(false);
                mni_insertar_hora.setEnabled(false);
                mni_justificar.setEnabled(false);
                mni_ver_detalle_fichada.setEnabled(false);

                mni_agregar_licencia_temporal1.setEnabled(false);
                mni_eliminar_hora1.setEnabled(false);
                mni_insercion_multiple1.setEnabled(false);
                mni_insertar_hora1.setEnabled(false);
                mni_justificar1.setEnabled(false);
                mni_ver_detalle_fichada1.setEnabled(false);

                jmn_relojes.setEnabled(false);
                break;
        }
    }

    private void anioActual() {
        Calendar c = Calendar.getInstance();
        txt_anio.setText("" + c.get(Calendar.YEAR));
    }

    private void vaciarTabla() {
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{
                    "Fecha", "Entrada", "Salida", "Entrada", "Salida", "Entrada", "Salida", "Normales", "Extras", "Retrazo"
                }
        ) {
            boolean[] canEdit = new boolean[]{
                false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        jScrollPane2.setViewportView(jTable1);
    }

    private void cargarJList() {

        listaEmpleado = contEmpleado.listar();//listaEmpleados();
        listaEmpleadosTemp = new ArrayList<>();
        DefaultListModel modelo = new DefaultListModel();

        if (usuarioSistema.getTipo().equals("admin") || usuarioSistema.getTipo().equals("supervisor")) {
            for (Userinfo e : listaEmpleado) {
                listaEmpleadosTemp = listaEmpleado;
                modelo.addElement(e.getName());
            }
            desaparecerSecciones();
        } else if (usuarioSistema.getIdaArea() > 0 && usuarioSistema.getIdaSeccion() == 0) { //jefe de area
            if (contUsuario.masDeUnArea(usuarioSistema.getUsuario()).size() > 1) {//si tiene mas de una area 
                listaAreas = contUsuario.encontrarTodos(contUsuario.masDeUnArea(usuarioSistema.getUsuario()));
                cargarListaAreas(listaAreas);
                cbx_areas.setVisible(true);
                lbl_areas.setVisible(true);
                cbx_areas.setSelectedIndex(0);

                if (!cbx_areas.getSelectedItem().toString().equals("Seleccione...")) {
                    listaEmpleadosTemp = contEmpleado.obtenerEmpleadosPorArea(listaAreas.get(cbx_areas.getSelectedIndex()).getId());
                    listaSecciones = contSeccion.encontrarPorArea(listaAreas.get(cbx_areas.getSelectedIndex()).getId());
            //en caso de ser un jefe de varias areas

                    //cargo el combo de secciones en caso de ser jefe de area
                    llenarComboSecciones();
                    aparecerSecciones();
                    //cargo la lista de todos los empleados del jefe d area
                    modelo = new DefaultListModel();
//                    for (Userinfo e : listaEmpleadosTemp) {
//                        modelo.addElement(e.getName());
//                    }
                }
            } else {
                listaEmpleadosTemp = contEmpleado.obtenerEmpleadosPorArea(usuarioSistema.getIdaArea());
                listaSecciones = contSeccion.encontrarPorArea(usuarioSistema.getIdaArea());
            //en caso de ser un jefe de varias areas

                //cargo el combo de secciones en caso de ser jefe de area
                cbx_areas.setVisible(false);
                lbl_areas.setVisible(false);
                llenarComboSecciones();
                aparecerSecciones();
                //cargo la lista de todos los empleados del jefe d area
                for (Userinfo e : listaEmpleadosTemp) {
                    modelo.addElement(e.getName());
                }
            }
        } else if (usuarioSistema.getIdaArea() == 0 && usuarioSistema.getIdaSeccion() > 0) {// jefe de seccion
            cbx_areas.setVisible(false);
            lbl_areas.setVisible(false);
            lbl_origen.setText("Sección: " + contSeccion.encontrar(usuarioSistema.getIdaSeccion()).getDetalle());
            listaEmpleadosTemp = contEmpleado.obtenerEmpleadosPorSeccion(usuarioSistema.getIdaSeccion());
            for (Userinfo e : listaEmpleadosTemp) {
                modelo.addElement(e.getName());
            }
            desaparecerSecciones();
        }

        list_empleados.setModel(modelo);
    }

    private void llenarComboSecciones() {

        lbl_origen.setText("Area: " + new ContArea().encontrar(usuarioSistema.getIdaArea()).getDetalle());
        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();
        for (Seccion s : listaSecciones) {
            modeloCombo.addElement(s.getDetalle());
        }
        modeloCombo.addElement("Seleccione...");
        modeloCombo.setSelectedItem("Seleccione...");
        cbx_seccion.setModel(modeloCombo);
    }

    private void cargarJList(String origen) {
        int reloj = contReloj.encontrarPorNombre(origen).getId();
        listaEmpleadosTemp = new ArrayList<>();

        DefaultListModel modelo = new DefaultListModel();

        if (origen.equals("Todos")) {
            listaEmpleadosTemp = listaEmpleado;
        } else {
            for (Userinfo userinfo : listaEmpleado) {
                if (userinfo.getReloj() == reloj) {
                    listaEmpleadosTemp.add(userinfo);
                }
            }
        }
//        listaEmpleadosTemp = listaEmpleado;

        for (Userinfo e : listaEmpleadosTemp) {
            modelo.addElement(e.getName());
        }

        list_empleados.setModel(modelo);
    }

    private void listar(boolean radiografia) throws IOException {
        List<Fichada> fichada = new ArrayList<>();
        List<String> fechas = new ArrayList<>();
        String f1 = "";
        String f2 = "";
        barra.setValue(37);
        lbl_porcentaje.setText("37%");

        if (checkFechas.isSelected()) {

            Timestamp fecha1 = new Timestamp(chooser_fechaInicio.getSelectedDate().getTimeInMillis());
            Timestamp fecha2 = new Timestamp(chooser_fechaFin.getSelectedDate().getTimeInMillis());
            fechas = u.fechas(fecha1, fecha2);

            f1 = fecha1.toString().split(" ")[0];
            f2 = fecha2.toString().split(" ")[0];

        } else if (checkMes.isSelected()) {

            fechas = u.fechas(Integer.parseInt(txt_anio.getText()), cbx_mes.getSelectedIndex() + 1);
            f1 = fechas.get(0);
            f2 = fechas.get(fechas.size() - 1);

        }
        if (checkMes.isSelected() || checkFechas.isSelected()) {

            barra.setValue(40);
            lbl_porcentaje.setText("40%");

            int i = 0;
            if (busquedaActiva) {
                Object[] parametros = {listaEmpleadosTempBusqueda.get(list_empleados.getSelectedIndex()).getId() + "", f1, f2};

                barra.setValue(45);
                lbl_porcentaje.setText("45%");

                if (i == 0) {
                    barra.setValue(50);
                    lbl_porcentaje.setText("50%");
                }
                i++;

                List<List> listList = controladorGeneral.buscarPorParametro(BDSentencias.fichadaColumnas, BDSentencias.fichadaMes2, parametros, BDSentencias.conexionRelojLocal);
                barra.setValue(55);
                lbl_porcentaje.setText("55%");
                Fichada f;
                for (List lista : listList) {
                    f = contFichada.setearObjeto(lista);
                    fichada.add(f);
//                    System.out.println("FICHADA: "+f.toString());
                }
                barra.setValue(60);
                lbl_porcentaje.setText("60%");
                barra.setValue(65);
                lbl_porcentaje.setText("65%");
//                System.err.println("TAMAÑO DE list<fihadas>: "+fichada.size());

                cargarTablaMes(fichada, fechas, radiografia);
                barra.setVisible(false);
                lbl_porcentaje.setVisible(false);

                listaLicencias = contLicenciaEmpleado.buscar(empleado, cbx_mes.getSelectedIndex() + 1, Integer.parseInt(txt_anio.getText()));//ACA QUEDE AYER, TRAE TODAS LAS LICENCIAS DEL EMPLEADO PARA DESPUES BUSCAR
                
            } else {
                Object[] parametros = {listaEmpleadosTemp.get(list_empleados.getSelectedIndex()).getId() + "", f1, f2};

                barra.setValue(45);
                lbl_porcentaje.setText("45%");

                if (i == 0) {
                    barra.setValue(50);
                    lbl_porcentaje.setText("50%");
                }
                i++;

                List<List> listList = controladorGeneral.buscarPorParametro(BDSentencias.fichadaColumnas, BDSentencias.fichadaMes2, parametros, BDSentencias.conexionRelojLocal);
                barra.setValue(55);
                lbl_porcentaje.setText("55%");
                Fichada f;
                for (List lista : listList) {
                    f = contFichada.setearObjeto(lista);
                    fichada.add(f);
                }
                barra.setValue(60);
                lbl_porcentaje.setText("60%");
                barra.setValue(65);
                lbl_porcentaje.setText("65%");

                cargarTablaMes(fichada, fechas, radiografia);
                barra.setVisible(false);
                lbl_porcentaje.setVisible(false);

                listaLicencias = contLicenciaEmpleado.buscar(empleado, cbx_mes.getSelectedIndex() + 1, Integer.parseInt(txt_anio.getText()));//ACA QUEDE AYER, TRAE TODAS LAS LICENCIAS DEL EMPLEADO PARA DESPUES BUSCAR

            }

        } else {
            JOptionPane.showMessageDialog(null, "No ha seleccionado tipo de busqueda", "", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void habilitarBotones() {
        btn_listar.setEnabled(false);
        btn_vista_previa.setEnabled(true);
        btn_imprimir_rep.setEnabled(true);
    }

    private void deshabilitarBotones() {
        btn_listar.setEnabled(true);
        btn_vista_previa.setEnabled(false);
        btn_imprimir_rep.setEnabled(false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        dialog_insertar_hora = new javax.swing.JDialog();
        jPanel6 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        cbx_hora = new javax.swing.JComboBox();
        cbx_minutos = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        btn_aceptar = new javax.swing.JButton();
        jLabel20 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        txt_observacion_insSimple = new javax.swing.JEditorPane();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        mni_agregar_licencia_temporal = new javax.swing.JMenuItem();
        mni_insertar_hora = new javax.swing.JMenuItem();
        mni_insercion_multiple = new javax.swing.JMenuItem();
        mni_eliminar_hora = new javax.swing.JMenuItem();
        mni_exportar_excel = new javax.swing.JMenuItem();
        mni_justificar = new javax.swing.JMenuItem();
        mni_ver_detalle_fichada = new javax.swing.JMenuItem();
        mni_ver_sin_procesar = new javax.swing.JMenuItem();
        dialog_insert_multiple = new javax.swing.JDialog();
        jPanel9 = new javax.swing.JPanel();
        cbx_minutos1 = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        cbx_hora1 = new javax.swing.JComboBox();
        jLabel18 = new javax.swing.JLabel();
        btn_agregar_incercion_m = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        lista_marcadas_insertar = new javax.swing.JList();
        btn_cancelar_incercion_m = new javax.swing.JButton();
        btn_aceptar_incercion_m = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel19 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        txt_observacion = new javax.swing.JEditorPane();
        dialog_justificar = new javax.swing.JDialog();
        jPanel10 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        txt_detalle = new javax.swing.JEditorPane();
        btn_aceptar_detalle = new javax.swing.JButton();
        dialog_ver_detalle = new javax.swing.JDialog();
        jPanel11 = new javax.swing.JPanel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jPopupMenu2 = new javax.swing.JPopupMenu();
        mni_borrar_de_lista = new javax.swing.JMenuItem();
        mni_borrar_todos = new javax.swing.JMenuItem();
        jDialog1 = new javax.swing.JDialog();
        jPanel12 = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        txt_justifiacion_de_marcada = new javax.swing.JEditorPane();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jScrollPane7 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        btn_listar = new javax.swing.JButton();
        btn_vista_previa = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        txt_mes = new javax.swing.JLabel();
        cbx_mes = new javax.swing.JComboBox();
        checkMes = new javax.swing.JCheckBox();
        jLabel1 = new javax.swing.JLabel();
        txt_anio = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        chooser_fechaInicio = new datechooser.beans.DateChooserCombo();
        chooser_fechaFin = new datechooser.beans.DateChooserCombo();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        checkFechas = new javax.swing.JCheckBox();
        btn_imprimir_rep = new javax.swing.JButton();
        pnl_seccion = new javax.swing.JPanel();
        cbx_seccion = new javax.swing.JComboBox();
        lbl_seccion = new javax.swing.JLabel();
        lbl_areas = new javax.swing.JLabel();
        cbx_areas = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        list_empleados = new javax.swing.JList();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        lbl_sabado_mañana = new javax.swing.JLabel();
        lbl_total_normales = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        lbl_sabado_tarde = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        lbl_domingo = new javax.swing.JLabel();
        lbl_feriado = new javax.swing.JLabel();
        lbl_extras = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lbl_retrazo = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        lbl_nombre = new javax.swing.JLabel();
        lbl_cuil = new javax.swing.JLabel();
        lbl_funcion = new javax.swing.JLabel();
        lbl_horario = new javax.swing.JLabel();
        barra = new javax.swing.JProgressBar();
        lbl_porcentaje = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jLabel29 = new javax.swing.JLabel();
        txt_busqueda = new javax.swing.JTextField();
        lbl_origen = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu2 = new javax.swing.JMenu();
        mni_ver_detalle_fichada1 = new javax.swing.JMenuItem();
        mni_justificar1 = new javax.swing.JMenuItem();
        mni_eliminar_hora1 = new javax.swing.JMenuItem();
        mni_insercion_multiple1 = new javax.swing.JMenuItem();
        mni_insertar_hora1 = new javax.swing.JMenuItem();
        mni_agregar_licencia_temporal1 = new javax.swing.JMenuItem();
        jmn_relojes = new javax.swing.JMenu();

        dialog_insertar_hora.setTitle("Nueva Marcada");
        dialog_insertar_hora.setIconImage(Toolkit.getDefaultToolkit().
            getImage(ClassLoader.getSystemResource("Iconos/bicentenario90.png")));

        jPanel6.setBackground(new java.awt.Color(204, 204, 204));

        jLabel2.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel2.setText("Hora:");

        cbx_hora.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        cbx_hora.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));

        cbx_minutos.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        cbx_minutos.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));

        jLabel4.setText(":");

        btn_aceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/OK2.png"))); // NOI18N
        btn_aceptar.setText("Aceptar");
        btn_aceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_aceptarActionPerformed(evt);
            }
        });

        jLabel20.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel20.setText("Observación:");

        jScrollPane5.setViewportView(txt_observacion_insSimple);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cbx_hora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbx_minutos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel20))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addComponent(btn_aceptar)
                .addContainerGap(53, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cbx_hora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbx_minutos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(18, 18, 18)
                .addComponent(jLabel20)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_aceptar)
                .addContainerGap())
        );

        javax.swing.GroupLayout dialog_insertar_horaLayout = new javax.swing.GroupLayout(dialog_insertar_hora.getContentPane());
        dialog_insertar_hora.getContentPane().setLayout(dialog_insertar_horaLayout);
        dialog_insertar_horaLayout.setHorizontalGroup(
            dialog_insertar_horaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        dialog_insertar_horaLayout.setVerticalGroup(
            dialog_insertar_horaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        mni_agregar_licencia_temporal.setText("Agregar justificacion de falta");
        mni_agregar_licencia_temporal.setToolTipText("Se usa cuando el empleado no ha venido a trabajar  \npor un motivo específico.");
        mni_agregar_licencia_temporal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mni_agregar_licencia_temporalActionPerformed(evt);
            }
        });
        jPopupMenu1.add(mni_agregar_licencia_temporal);

        mni_insertar_hora.setText("Insertar marcada por sistema");
        mni_insertar_hora.setToolTipText("Ingresa marcadas  por sistemas en caso de un empleado\n que haya presentado nota y para justificar una marcada \n faltante de algún día.");
        mni_insertar_hora.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mni_insertar_horaActionPerformed(evt);
            }
        });
        jPopupMenu1.add(mni_insertar_hora);

        mni_insercion_multiple.setText("Inserción múltiple de marcadas");
        mni_insercion_multiple.setToolTipText("Ingresa marcadas multiples por sistemas en caso de un empleado\n que haya presentado nota y para justificar una marcada faltante\n de algún día.");
        mni_insercion_multiple.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mni_insercion_multipleActionPerformed(evt);
            }
        });
        jPopupMenu1.add(mni_insercion_multiple);

        mni_eliminar_hora.setText("Eliminar marcada");
        mni_eliminar_hora.setToolTipText("Elimina solo horas ingresadas por sistema.");
        mni_eliminar_hora.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mni_eliminar_horaActionPerformed(evt);
            }
        });
        jPopupMenu1.add(mni_eliminar_hora);

        mni_exportar_excel.setText("Exportar a excel");
        mni_exportar_excel.setToolTipText("Crea un documendo excel en la ubicación que se elija.");
        mni_exportar_excel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mni_exportar_excelActionPerformed(evt);
            }
        });
        jPopupMenu1.add(mni_exportar_excel);

        mni_justificar.setText("Justificar marcadas del día");
        mni_justificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mni_justificarActionPerformed(evt);
            }
        });
        jPopupMenu1.add(mni_justificar);

        mni_ver_detalle_fichada.setText("Ver detalles de marcada");
        mni_ver_detalle_fichada.setToolTipText("");
        mni_ver_detalle_fichada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mni_ver_detalle_fichadaActionPerformed(evt);
            }
        });
        jPopupMenu1.add(mni_ver_detalle_fichada);

        mni_ver_sin_procesar.setText("Ver fichada original");
        mni_ver_sin_procesar.setToolTipText("Muestra marcadas relizadas tal cual el reloj");
        mni_ver_sin_procesar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mni_ver_sin_procesarActionPerformed(evt);
            }
        });
        jPopupMenu1.add(mni_ver_sin_procesar);

        dialog_insert_multiple.setTitle("Insercion Multiple");
        dialog_insert_multiple.setIconImage(Toolkit.getDefaultToolkit().
            getImage(ClassLoader.getSystemResource("Iconos/bicentenario90.png")));

        jPanel9.setBackground(new java.awt.Color(204, 204, 204));
        jPanel9.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "Inserción múltiple de marcadas"));

        cbx_minutos1.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        cbx_minutos1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));

        jLabel6.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel6.setText("Hora:");

        cbx_hora1.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        cbx_hora1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" }));

        jLabel18.setText(":");

        btn_agregar_incercion_m.setText("Agregar");
        btn_agregar_incercion_m.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_agregar_incercion_mActionPerformed(evt);
            }
        });

        lista_marcadas_insertar.setComponentPopupMenu(jPopupMenu2);
        jScrollPane3.setViewportView(lista_marcadas_insertar);

        btn_cancelar_incercion_m.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/No.png"))); // NOI18N
        btn_cancelar_incercion_m.setText("Cancelar");
        btn_cancelar_incercion_m.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelar_incercion_mActionPerformed(evt);
            }
        });

        btn_aceptar_incercion_m.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/OK2.png"))); // NOI18N
        btn_aceptar_incercion_m.setText("Aceptar");
        btn_aceptar_incercion_m.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_aceptar_incercion_mActionPerformed(evt);
            }
        });

        jLabel19.setText("Observación:");

        jScrollPane4.setViewportView(txt_observacion);

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 374, Short.MAX_VALUE)
                    .addComponent(jScrollPane4)
                    .addComponent(jSeparator1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btn_aceptar_incercion_m)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_cancelar_incercion_m))
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(jLabel19)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cbx_hora1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbx_minutos1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_agregar_incercion_m)))
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(cbx_hora1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbx_minutos1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18)
                    .addComponent(btn_agregar_incercion_m))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 47, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_cancelar_incercion_m)
                    .addComponent(btn_aceptar_incercion_m))
                .addContainerGap())
        );

        javax.swing.GroupLayout dialog_insert_multipleLayout = new javax.swing.GroupLayout(dialog_insert_multiple.getContentPane());
        dialog_insert_multiple.getContentPane().setLayout(dialog_insert_multipleLayout);
        dialog_insert_multipleLayout.setHorizontalGroup(
            dialog_insert_multipleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        dialog_insert_multipleLayout.setVerticalGroup(
            dialog_insert_multipleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        dialog_justificar.setTitle("Justificar Marcada");
        dialog_justificar.setIconImage(Toolkit.getDefaultToolkit().
            getImage(ClassLoader.getSystemResource("Iconos/bicentenario90.png")));

        jPanel10.setBackground(new java.awt.Color(204, 204, 204));
        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "Agregar Detalle"));

        jScrollPane6.setViewportView(txt_detalle);

        btn_aceptar_detalle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/OK2.png"))); // NOI18N
        btn_aceptar_detalle.setText("Aceptar");
        btn_aceptar_detalle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_aceptar_detalleActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap(114, Short.MAX_VALUE)
                .addComponent(btn_aceptar_detalle)
                .addGap(110, 110, 110))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_aceptar_detalle)
                .addContainerGap())
        );

        javax.swing.GroupLayout dialog_justificarLayout = new javax.swing.GroupLayout(dialog_justificar.getContentPane());
        dialog_justificar.getContentPane().setLayout(dialog_justificarLayout);
        dialog_justificarLayout.setHorizontalGroup(
            dialog_justificarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        dialog_justificarLayout.setVerticalGroup(
            dialog_justificarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        dialog_ver_detalle.setTitle("Detalles");
        dialog_ver_detalle.setIconImage(Toolkit.getDefaultToolkit().
            getImage(ClassLoader.getSystemResource("Iconos/bicentenario90.png")));
        dialog_ver_detalle.setResizable(false);

        jPanel11.setBackground(new java.awt.Color(204, 204, 204));

        jLabel21.setText("Fecha:");

        jLabel22.setText("Hora:");

        jLabel23.setText("Fichada por sistema:");

        jLabel24.setText("Observación:");

        jLabel25.setText("jLabel25");

        jLabel26.setText("jLabel26");

        jLabel27.setText("jLabel27");

        jLabel28.setText("jLabel28");

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/OK2.png"))); // NOI18N
        jButton1.setText("Aceptar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel23)
                    .addComponent(jLabel22, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel21, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel24, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(18, 18, 18)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1)
                    .addComponent(jLabel25)
                    .addComponent(jLabel26)
                    .addComponent(jLabel27)
                    .addComponent(jLabel28))
                .addContainerGap(159, Short.MAX_VALUE))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(jLabel25))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(jLabel26))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23)
                    .addComponent(jLabel27))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(jLabel28))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addContainerGap())
        );

        javax.swing.GroupLayout dialog_ver_detalleLayout = new javax.swing.GroupLayout(dialog_ver_detalle.getContentPane());
        dialog_ver_detalle.getContentPane().setLayout(dialog_ver_detalleLayout);
        dialog_ver_detalleLayout.setHorizontalGroup(
            dialog_ver_detalleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        dialog_ver_detalleLayout.setVerticalGroup(
            dialog_ver_detalleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        mni_borrar_de_lista.setText("Borrar de lista");
        mni_borrar_de_lista.setToolTipText("");
        mni_borrar_de_lista.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mni_borrar_de_listaActionPerformed(evt);
            }
        });
        jPopupMenu2.add(mni_borrar_de_lista);

        mni_borrar_todos.setText("Borrar todo");
        mni_borrar_todos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mni_borrar_todosActionPerformed(evt);
            }
        });
        jPopupMenu2.add(mni_borrar_todos);

        jPanel12.setBorder(javax.swing.BorderFactory.createTitledBorder("Justificacion de falta de marcadas"));

        jScrollPane8.setViewportView(txt_justifiacion_de_marcada);

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/OK2.png"))); // NOI18N
        jButton3.setText("Guardar");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/No.png"))); // NOI18N
        jButton4.setText("Cancelar");

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addComponent(jScrollPane8)
                .addContainerGap())
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGap(0, 143, Short.MAX_VALUE)
                .addComponent(jButton3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton4)
                .addGap(1, 1, 1))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton3, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton4, javax.swing.GroupLayout.Alignment.TRAILING)))
        );

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Ver Fichadas");
        setIconImage(Toolkit.getDefaultToolkit().
            getImage(ClassLoader.getSystemResource("Iconos/bicentenario90.png")));

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));

        btn_listar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/Estadisticas.png"))); // NOI18N
        btn_listar.setText("Listar");
        btn_listar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_listarActionPerformed(evt);
            }
        });

        btn_vista_previa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/File_24.png"))); // NOI18N
        btn_vista_previa.setText("Vista previa");
        btn_vista_previa.setEnabled(false);
        btn_vista_previa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_vista_previaActionPerformed(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        txt_mes.setText("Mes:");

        cbx_mes.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" }));
        cbx_mes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbx_mesActionPerformed(evt);
            }
        });

        buttonGroup1.add(checkMes);
        checkMes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkMesActionPerformed(evt);
            }
        });

        jLabel1.setText("Año:");

        txt_anio.setText("2014");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(checkMes)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(txt_mes)
                        .addGap(18, 18, 18)
                        .addComponent(cbx_mes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(txt_anio)))
                .addContainerGap(81, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(checkMes)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txt_anio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbx_mes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_mes))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        chooser_fechaInicio.setCurrentView(new datechooser.view.appearance.AppearancesList("Grey",
            new datechooser.view.appearance.ViewAppearance("custom",
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(0, 0, 0),
                    new java.awt.Color(0, 0, 255),
                    false,
                    true,
                    new datechooser.view.appearance.swing.ButtonPainter()),
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(0, 0, 0),
                    new java.awt.Color(0, 0, 255),
                    true,
                    true,
                    new datechooser.view.appearance.swing.ButtonPainter()),
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(0, 0, 255),
                    new java.awt.Color(0, 0, 255),
                    false,
                    true,
                    new datechooser.view.appearance.swing.ButtonPainter()),
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(128, 128, 128),
                    new java.awt.Color(0, 0, 255),
                    false,
                    true,
                    new datechooser.view.appearance.swing.LabelPainter()),
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(0, 0, 0),
                    new java.awt.Color(0, 0, 255),
                    false,
                    true,
                    new datechooser.view.appearance.swing.LabelPainter()),
                new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                    new java.awt.Color(0, 0, 0),
                    new java.awt.Color(255, 0, 0),
                    false,
                    false,
                    new datechooser.view.appearance.swing.ButtonPainter()),
                (datechooser.view.BackRenderer)null,
                false,
                true)));
    chooser_fechaInicio.addCommitListener(new datechooser.events.CommitListener() {
        public void onCommit(datechooser.events.CommitEvent evt) {
            chooser_fechaInicioOnCommit(evt);
        }
    });

    chooser_fechaFin.setCurrentView(new datechooser.view.appearance.AppearancesList("Grey",
        new datechooser.view.appearance.ViewAppearance("custom",
            new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                new java.awt.Color(0, 0, 0),
                new java.awt.Color(0, 0, 255),
                false,
                true,
                new datechooser.view.appearance.swing.ButtonPainter()),
            new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                new java.awt.Color(0, 0, 0),
                new java.awt.Color(0, 0, 255),
                true,
                true,
                new datechooser.view.appearance.swing.ButtonPainter()),
            new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                new java.awt.Color(0, 0, 255),
                new java.awt.Color(0, 0, 255),
                false,
                true,
                new datechooser.view.appearance.swing.ButtonPainter()),
            new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                new java.awt.Color(128, 128, 128),
                new java.awt.Color(0, 0, 255),
                false,
                true,
                new datechooser.view.appearance.swing.LabelPainter()),
            new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                new java.awt.Color(0, 0, 0),
                new java.awt.Color(0, 0, 255),
                false,
                true,
                new datechooser.view.appearance.swing.LabelPainter()),
            new datechooser.view.appearance.swing.SwingCellAppearance(new java.awt.Font("Tahoma", java.awt.Font.PLAIN, 11),
                new java.awt.Color(0, 0, 0),
                new java.awt.Color(255, 0, 0),
                false,
                false,
                new datechooser.view.appearance.swing.ButtonPainter()),
            (datechooser.view.BackRenderer)null,
            false,
            true)));
chooser_fechaFin.addCommitListener(new datechooser.events.CommitListener() {
    public void onCommit(datechooser.events.CommitEvent evt) {
        chooser_fechaFinOnCommit(evt);
    }
    });

    jLabel10.setText("Fecha inicio:");

    jLabel11.setText("Fecha fin:");

    buttonGroup1.add(checkFechas);
    checkFechas.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            checkFechasActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
    jPanel5.setLayout(jPanel5Layout);
    jPanel5Layout.setHorizontalGroup(
        jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel5Layout.createSequentialGroup()
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel5Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel10)
                        .addComponent(jLabel11)))
                .addComponent(checkFechas))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(chooser_fechaInicio, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(chooser_fechaFin, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
    );
    jPanel5Layout.setVerticalGroup(
        jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel5Layout.createSequentialGroup()
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addGroup(jPanel5Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(chooser_fechaInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                    .addComponent(checkFechas)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel10)))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jLabel11)
                .addComponent(chooser_fechaFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    btn_imprimir_rep.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/impresora.png"))); // NOI18N
    btn_imprimir_rep.setText("Impimir");
    btn_imprimir_rep.setEnabled(false);
    btn_imprimir_rep.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btn_imprimir_repActionPerformed(evt);
        }
    });

    pnl_seccion.setBackground(new java.awt.Color(204, 204, 204));

    cbx_seccion.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
    cbx_seccion.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            cbx_seccionActionPerformed(evt);
        }
    });

    lbl_seccion.setText("Seccion:");

    lbl_areas.setText("Area:");

    cbx_areas.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
    cbx_areas.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            cbx_areasActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout pnl_seccionLayout = new javax.swing.GroupLayout(pnl_seccion);
    pnl_seccion.setLayout(pnl_seccionLayout);
    pnl_seccionLayout.setHorizontalGroup(
        pnl_seccionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnl_seccionLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnl_seccionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(lbl_areas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbl_seccion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnl_seccionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(cbx_seccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(cbx_areas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(14, Short.MAX_VALUE))
    );
    pnl_seccionLayout.setVerticalGroup(
        pnl_seccionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnl_seccionLayout.createSequentialGroup()
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pnl_seccionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lbl_areas)
                .addComponent(cbx_areas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnl_seccionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbx_seccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lbl_seccion))
            .addContainerGap())
    );

    javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
    jPanel2.setLayout(jPanel2Layout);
    jPanel2Layout.setHorizontalGroup(
        jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel2Layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(pnl_seccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(btn_listar, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(btn_imprimir_rep, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(btn_vista_previa, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap())
    );
    jPanel2Layout.setVerticalGroup(
        jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel2Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_vista_previa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_imprimir_rep, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_listar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pnl_seccion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addContainerGap())
    );

    list_empleados.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
    list_empleados.setModel(new javax.swing.AbstractListModel() {
        String[] strings = { "Empleado 1", "Empleado 2", "Empleado 3", "Empleado 4", " " };
        public int getSize() { return strings.length; }
        public Object getElementAt(int i) { return strings[i]; }
    });
    list_empleados.addMouseListener(new java.awt.event.MouseAdapter() {
        public void mouseReleased(java.awt.event.MouseEvent evt) {
            list_empleadosMouseReleased(evt);
        }
    });
    list_empleados.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyReleased(java.awt.event.KeyEvent evt) {
            list_empleadosKeyReleased(evt);
        }
    });
    jScrollPane1.setViewportView(list_empleados);

    jTable1.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][] {
            {null, null, null, null, null, null, null, null, null, null},
            {null, null, null, null, null, null, null, null, null, null},
            {null, null, null, null, null, null, null, null, null, null},
            {null, null, null, null, null, null, null, null, null, null}
        },
        new String [] {
            "Fecha", "Entrada", "Salida", "Entrada", "Salida", "Entrada", "Salida", "Normales", "Extras", "Retrazo"
        }
    ) {
        boolean[] canEdit = new boolean [] {
            false, false, false, false, false, false, false, false, false, false
        };

        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit [columnIndex];
        }
    });
    jTable1.setComponentPopupMenu(jPopupMenu1);
    jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
        public void mouseReleased(java.awt.event.MouseEvent evt) {
            jTable1MouseReleased(evt);
        }
    });
    jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyReleased(java.awt.event.KeyEvent evt) {
            jTable1KeyReleased(evt);
        }
    });
    jScrollPane2.setViewportView(jTable1);

    jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "Totales", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, null, new java.awt.Color(0, 0, 153)));

    lbl_sabado_mañana.setForeground(new java.awt.Color(0, 0, 153));
    lbl_sabado_mañana.setText("00:00:00");

    lbl_total_normales.setForeground(new java.awt.Color(0, 0, 153));
    lbl_total_normales.setText("00:00:00");

    jLabel3.setForeground(new java.awt.Color(0, 0, 153));
    jLabel3.setText("Normal:");

    jLabel8.setForeground(new java.awt.Color(0, 0, 153));
    jLabel8.setText("Sabado Mañana:");

    jLabel5.setForeground(new java.awt.Color(0, 0, 153));
    jLabel5.setText("Extras:");

    jLabel13.setForeground(new java.awt.Color(0, 0, 153));
    jLabel13.setText("Sabado Tarde:");

    lbl_sabado_tarde.setForeground(new java.awt.Color(0, 0, 153));
    lbl_sabado_tarde.setText("00:00:00");

    jLabel15.setForeground(new java.awt.Color(0, 0, 153));
    jLabel15.setText("Domingo:");

    jLabel16.setForeground(new java.awt.Color(0, 0, 153));
    jLabel16.setText("Feriado:");

    lbl_domingo.setForeground(new java.awt.Color(0, 0, 153));
    lbl_domingo.setText("00:00:00");

    lbl_feriado.setForeground(new java.awt.Color(0, 0, 153));
    lbl_feriado.setText("00:00:00");

    lbl_extras.setForeground(new java.awt.Color(0, 0, 153));
    lbl_extras.setText("00:00:00");

    jLabel7.setForeground(new java.awt.Color(0, 0, 153));
    jLabel7.setText("Retrazo:");

    lbl_retrazo.setForeground(new java.awt.Color(0, 0, 153));
    lbl_retrazo.setText("00:00:00");

    javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
    jPanel7.setLayout(jPanel7Layout);
    jPanel7Layout.setHorizontalGroup(
        jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel7Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                .addGroup(jPanel7Layout.createSequentialGroup()
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel3)
                        .addComponent(jLabel8))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(lbl_total_normales)
                        .addComponent(lbl_sabado_mañana)))
                .addGroup(jPanel7Layout.createSequentialGroup()
                    .addComponent(jLabel15)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(lbl_domingo)))
            .addGap(18, 18, 18)
            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jLabel16, javax.swing.GroupLayout.Alignment.TRAILING)
                .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.TRAILING)
                .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel7Layout.createSequentialGroup()
                    .addComponent(lbl_extras)
                    .addGap(18, 18, 18)
                    .addComponent(jLabel7)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(lbl_retrazo))
                .addComponent(lbl_sabado_tarde)
                .addComponent(lbl_feriado))
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
    jPanel7Layout.setVerticalGroup(
        jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel7Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel7Layout.createSequentialGroup()
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(lbl_extras)
                        .addComponent(jLabel7)
                        .addComponent(lbl_retrazo))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel13)
                        .addComponent(lbl_sabado_tarde))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel16)
                        .addComponent(lbl_feriado)))
                .addGroup(jPanel7Layout.createSequentialGroup()
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(lbl_total_normales))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel8)
                        .addComponent(lbl_sabado_mañana))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel15)
                        .addComponent(lbl_domingo))))
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED), "Información"));

    jLabel9.setText("Nombre:");

    jLabel12.setText("Cuil:");

    jLabel14.setText("Función que desempeña:");

    jLabel17.setText("Horario:");

    lbl_nombre.setText(" ");

    lbl_cuil.setText(" ");

    lbl_funcion.setText(" ");

    lbl_horario.setText("00:00 a 00:00");

    javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
    jPanel8.setLayout(jPanel8Layout);
    jPanel8Layout.setHorizontalGroup(
        jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel8Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jLabel14)
                .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING)
                .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.TRAILING)
                .addComponent(jLabel17, javax.swing.GroupLayout.Alignment.TRAILING))
            .addGap(18, 18, 18)
            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(lbl_nombre)
                .addComponent(lbl_cuil)
                .addComponent(lbl_funcion)
                .addComponent(lbl_horario))
            .addContainerGap(112, Short.MAX_VALUE))
    );
    jPanel8Layout.setVerticalGroup(
        jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel8Layout.createSequentialGroup()
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel9)
                .addComponent(lbl_nombre))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel12)
                .addComponent(lbl_cuil))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel14)
                .addComponent(lbl_funcion))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel17)
                .addComponent(lbl_horario)))
    );

    javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
    jPanel3.setLayout(jPanel3Layout);
    jPanel3Layout.setHorizontalGroup(
        jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
    jPanel3Layout.setVerticalGroup(
        jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    );

    lbl_porcentaje.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
    lbl_porcentaje.setForeground(new java.awt.Color(153, 0, 0));
    lbl_porcentaje.setText("0%");

    jLabel29.setText("Busqueda:");

    txt_busqueda.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyReleased(java.awt.event.KeyEvent evt) {
            txt_busquedaKeyReleased(evt);
        }
    });

    lbl_origen.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
    lbl_origen.setText("Todos");

    javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
    jPanel13.setLayout(jPanel13Layout);
    jPanel13Layout.setHorizontalGroup(
        jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel13Layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(jLabel29)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(txt_busqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(225, 225, 225)
            .addComponent(lbl_origen)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
    jPanel13Layout.setVerticalGroup(
        jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel13Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel29)
                .addComponent(txt_busqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lbl_origen))
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(jScrollPane2))
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(lbl_porcentaje))
                .addComponent(barra, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addContainerGap())
    );
    jPanel1Layout.setVerticalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addGap(4, 4, 4)
            .addComponent(barra, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 344, Short.MAX_VALUE)
                .addComponent(jScrollPane1))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(lbl_porcentaje)
            .addGap(5, 5, 5))
    );

    jScrollPane7.setViewportView(jPanel1);

    jMenu2.setText("Opciones");

    mni_ver_detalle_fichada1.setText("Ver detalles de marcada");
    mni_ver_detalle_fichada1.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            mni_ver_detalle_fichada1ActionPerformed(evt);
        }
    });
    jMenu2.add(mni_ver_detalle_fichada1);

    mni_justificar1.setText("Justificar fichada");
    mni_justificar1.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            mni_justificar1ActionPerformed(evt);
        }
    });
    jMenu2.add(mni_justificar1);

    mni_eliminar_hora1.setText("Eliminar hora");
    mni_eliminar_hora1.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            mni_eliminar_hora1ActionPerformed(evt);
        }
    });
    jMenu2.add(mni_eliminar_hora1);

    mni_insercion_multiple1.setText("Inserción múltiple");
    mni_insercion_multiple1.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            mni_insercion_multiple1ActionPerformed(evt);
        }
    });
    jMenu2.add(mni_insercion_multiple1);

    mni_insertar_hora1.setText("Insertar hora por sistema");
    mni_insertar_hora1.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            mni_insertar_hora1ActionPerformed(evt);
        }
    });
    jMenu2.add(mni_insertar_hora1);

    mni_agregar_licencia_temporal1.setText("Agregar justificacion de falta");
    mni_agregar_licencia_temporal1.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            mni_agregar_licencia_temporal1ActionPerformed(evt);
        }
    });
    jMenu2.add(mni_agregar_licencia_temporal1);

    jMenuBar1.add(jMenu2);

    jmn_relojes.setText("Ver");
    jMenuBar1.add(jmn_relojes);

    setJMenuBar(jMenuBar1);

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGap(0, 1055, Short.MAX_VALUE)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane7))
    );
    layout.setVerticalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGap(0, 565, Short.MAX_VALUE)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 565, Short.MAX_VALUE))
    );

    pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_listarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_listarActionPerformed
        radiografia = false;
        Runnable r = new Runnable() {

            @Override
            public void run() {
                setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                lbl_porcentaje.setVisible(true);
                barra.setVisible(true);
                barra.setForeground(Color.GREEN);
                barra.setValue(5);
                lbl_porcentaje.setText("5%");

                if (list_empleados.getSelectedIndex() != -1) {
                    barra.setValue(10);
                    lbl_porcentaje.setText("10%");
                    barra.setValue(15);
                    lbl_porcentaje.setText("15%");

                    ContFeriado contFeriado = new ContFeriado();

                    listaFichadaDia = new ArrayList<>();//borramos toodo el contenido del array
                    if (busquedaActiva) {
                        empleado = listaEmpleadosTempBusqueda.get(list_empleados.getSelectedIndex());
                    } else {
                        empleado = listaEmpleadosTemp.get(list_empleados.getSelectedIndex());
                    }
                    listaFeriadoMes = contFeriado.feriados(Integer.parseInt(txt_anio.getText()), cbx_mes.getSelectedIndex() + 1);

                    barra.setValue(20);
                    lbl_porcentaje.setText("20%");

                    llenarInfo();

                    barra.setValue(25);
                    lbl_porcentaje.setText("25%");

                    ContUserinfo contEmpleado = new ContUserinfo();
                    if (contEmpleado.tieneHorario(empleado.getId())) {

                        barra.setValue(30);
                        lbl_porcentaje.setText("30%");
                        barra.setValue(35);
                        lbl_porcentaje.setText("35%");

                        try {
                            listar(radiografia);
                        } catch (IOException ex) {
                            Logger.getLogger(V_ver_fichada.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El empleado no tiene un grupo horario asignado.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "No ha seleccionado ningún empleado.", "Error", JOptionPane.ERROR_MESSAGE);
                }

                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        };
        Thread hilo = new Thread(r);
        hilo.start();
        habilitarBotones();

    }//GEN-LAST:event_btn_listarActionPerformed

    private void checkMesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkMesActionPerformed
        if (checkMes.isSelected()) {
            activarPanel1();
            desactivarPanel2();

            if (list_empleados.getSelectedIndex() != -1) {
                if (checkMes.isSelected() || checkFechas.isSelected()) {
                    deshabilitarBotones();
                }
            }
        }
    }//GEN-LAST:event_checkMesActionPerformed

    private void checkFechasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkFechasActionPerformed
        if (checkFechas.isSelected()) {
            activarPanel2();
            desactivarPanel1();

            if (list_empleados.getSelectedIndex() != -1) {
                if (checkMes.isSelected() || checkFechas.isSelected()) {
                    deshabilitarBotones();
                }
            }
        }
    }//GEN-LAST:event_checkFechasActionPerformed

    private void list_empleadosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_list_empleadosKeyReleased
        if (list_empleados.getSelectedIndex() != -1) {
            if (listaEmpleadosTemp.get(list_empleados.getSelectedIndex()) != null) {
                empleadoSeleccionado = listaEmpleadosTemp.get(list_empleados.getSelectedIndex());
                if (checkMes.isSelected() || checkFechas.isSelected()) {
                    deshabilitarBotones();
                    vaciarTabla();
                }
            }
        }
        
        if(evt.getKeyChar() == KeyEvent.VK_ENTER){
            btn_listarActionPerformed(null);
        }
        
    }//GEN-LAST:event_list_empleadosKeyReleased

    private void list_empleadosMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_list_empleadosMouseReleased
        if (list_empleados.getSelectedIndex() != -1) {
            if (listaEmpleadosTemp.get(list_empleados.getSelectedIndex()) != null) {
//                System.out.println(listaEmpleadosTemp.get(jList1.getSelectedIndex()).getName() + " " + listaEmpleadosTemp.get(jList1.getSelectedIndex()).getUserid());
                empleadoSeleccionado = listaEmpleadosTemp.get(list_empleados.getSelectedIndex());
                if (checkMes.isSelected() || checkFechas.isSelected()) {
                    deshabilitarBotones();
                    vaciarTabla();
                }
            }
        }
    }//GEN-LAST:event_list_empleadosMouseReleased

    private void mni_insertar_horaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mni_insertar_horaActionPerformed

//        System.err.println("FECHA: " + fechaFila);
        dialog_insertar_hora.setModal(true);
        dialog_insertar_hora.setSize(300, 400);
        dialog_insertar_hora.setLocationRelativeTo(this);
        dialog_insertar_hora.setVisible(true);

    }//GEN-LAST:event_mni_insertar_horaActionPerformed

    private void btn_aceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_aceptarActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

        if (empleadoSeleccionado != null && !fechaFila.equals("")) {

            Timestamp hora = Timestamp.valueOf(fechaFila + " " + cbx_hora.getSelectedItem().toString()
                    + ":" + cbx_minutos.getSelectedItem().toString()
                    + ":00");
            java.sql.Date fecha = new java.sql.Date(hora.getTime());
            Time horaTime = new Time(hora.getTime());

            Fichada f = new Fichada();
            f.setUserinfoid(empleado.getId());
            f.setFecha(fecha);
            f.setHora(horaTime);
            f.setObservacion(txt_observacion_insSimple.getText());
            f.setTerminal(contFichada.getNombreTerminal());
            f.setIp(contFichada.getIpTerminal());
            f.setUsuario(usuarioSistema.getUsuario());
            f.setFechasubido(contFichada.getFechaServidor());

            if (contFichada.insertar(f) > 0) {

                dialog_insertar_hora.dispose();

                recargar();

            } else {
                JOptionPane.showMessageDialog(null, "Entrada de datos no insertada.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }

        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_btn_aceptarActionPerformed

    private void btn_vista_previaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_vista_previaActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

        if (checkFechas.isSelected()) {
            try {

                fichadaReporte();
            } catch (IOException ex) {
                Logger.getLogger(V_ver_fichada.class.getName()).log(Level.SEVERE, null, ex);

                ContLogger contLogger = new ContLogger();
                String error = "010";
                Loggs loggs = new Loggs(new Timestamp(Calendar.getInstance().getTimeInMillis()),
                        Logger.getLogger(V_ver_fichada.class.getName()).getName(),
                        contLogger.getNombreTerminal(),
                        ex.fillInStackTrace().toString(),
                        error);
                contLogger.insert(loggs);
                JOptionPane.showMessageDialog(null, "Error:" + error + "\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);

            }
        }

        if (checkMes.isSelected()) {
            try {
                accion = "vista previa";
                fichadaReporte();
            } catch (IOException ex) {
                Logger.getLogger(V_ver_fichada.class.getName()).log(Level.SEVERE, null, ex);

                ContLogger contLogger = new ContLogger();
                String error = "011";
                Loggs loggs = new Loggs(new Timestamp(Calendar.getInstance().getTimeInMillis()),
                        Logger.getLogger(V_ver_fichada.class.getName()).getName(),
                        contLogger.getNombreTerminal(),
                        ex.fillInStackTrace().toString(),
                        error);
                contLogger.insert(loggs);
                JOptionPane.showMessageDialog(null, "Error:" + error + "\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);

            }

        }

        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_btn_vista_previaActionPerformed

    private void mni_eliminar_horaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mni_eliminar_horaActionPerformed
        if (JOptionPane.showConfirmDialog(null, "Confirme operacion") == 0) {
//            System.out.println("Fecha: " + java.sql.Date.valueOf(fechaFila) + " hora: " + Time.valueOf(horaFila) + " EMPLEADO: " + empleado.toString());
            if (!horaFila.equals("") && contFichada.esMarcadaPorSistema(java.sql.Date.valueOf(fechaFila), empleado, Time.valueOf(horaFila))) {
                Fichada f = new Fichada();
                f.setUserinfoid(empleado.getId());
                f.setFecha(java.sql.Date.valueOf(fechaFila));
                f.setHora(Time.valueOf(horaFila));
                f.setEstado(false);

                if (contFichada.delete(f) > 0) {
                    JOptionPane.showMessageDialog(null, "Hora eliminada correctamente.");
                    ContUserinfo contEmpleado = new ContUserinfo();
                    if (contEmpleado.tieneHorario(empleado.getId())) {
                        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                        try {
                            listar(false);
                        } catch (IOException ex) {
                            Logger.getLogger(V_ver_fichada.class.getName()).log(Level.SEVERE, null, ex);

                            ContLogger contLogger = new ContLogger();
                            String error = "012";
                            Loggs loggs = new Loggs(new Timestamp(Calendar.getInstance().getTimeInMillis()),
                                    Logger.getLogger(V_ver_fichada.class.getName()).getName(),
                                    contLogger.getNombreTerminal(),
                                    ex.fillInStackTrace().toString(),
                                    error);
                            contLogger.insert(loggs);
                            JOptionPane.showMessageDialog(null, "Error:" + error + "\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);

                        }
                        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                    } else {
                        JOptionPane.showMessageDialog(null, "El empleado no tiene un grupo horario asignado.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "No se puede eliminar la hora seleccionada,\nElija una hora insertada por sistema.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null, "El dato que pretende eliminar es un campo vacio o hora insertada por reloj,\nElija una hora insertada por sistema.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_mni_eliminar_horaActionPerformed

    private void jTable1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseReleased
        if (jTable1.getSelectedRow() != -1) {
            fechaFila = jTable1.getValueAt(jTable1.getSelectedRow(), 0).toString();
            try {
                horaFila = jTable1.getValueAt(jTable1.getSelectedRow(), jTable1.getSelectedColumn()).toString();
            } catch (Exception ex) {
                horaFila = "";
            }

            int i = 0;
            for (List<Fichada> list : listaLFichada) {
                if (i == jTable1.getSelectedRow()) {
                    if (jTable1.getValueAt(jTable1.getSelectedRow(), jTable1.getSelectedColumn()) != null && jTable1.getSelectedColumn() < 7 && jTable1.getSelectedColumn() > 0) {
                        fichadaFila = list.get(jTable1.getSelectedColumn() - 1);
                    }
                }
                i++;
            }
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione fila por favor");
        }
    }//GEN-LAST:event_jTable1MouseReleased

    private void jTable1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyReleased
        if (jTable1.getSelectedRow() != -1) {
            fechaFila = jTable1.getValueAt(jTable1.getSelectedRow(), 0).toString();
            try {
                horaFila = jTable1.getValueAt(jTable1.getSelectedRow(), jTable1.getSelectedColumn()).toString();
            } catch (NullPointerException ex) {
                horaFila = "";
            }

            int i = 0;
            for (List<Fichada> list : listaLFichada) {
                if (i == jTable1.getSelectedRow()) {
                    if (jTable1.getValueAt(jTable1.getSelectedRow(), jTable1.getSelectedColumn()) != null && jTable1.getSelectedColumn() < 7 && jTable1.getSelectedColumn() > 0) {
                        fichadaFila = list.get(jTable1.getSelectedColumn() - 1);
                    }
                }
                i++;
            }
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione fila por favor");
        }
    }//GEN-LAST:event_jTable1KeyReleased

    private void btn_agregar_incercion_mActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_agregar_incercion_mActionPerformed
        //sacamos los datos q contenia originalmente el jlist

        DefaultListModel modeloNuevo = new DefaultListModel();
        ListModel modeloViejo = lista_marcadas_insertar.getModel();
        for (int i = 0; i < modeloViejo.getSize(); i++) {
            modeloNuevo.addElement(modeloViejo.getElementAt(i).toString());
        }

        //colocamos los datos al nuevo modelo
        modeloNuevo.addElement(cbx_hora1.getSelectedItem().toString() + ":" + cbx_minutos1.getSelectedItem().toString() + ":00");
        lista_marcadas_insertar.setModel(modeloNuevo);
    }//GEN-LAST:event_btn_agregar_incercion_mActionPerformed

    private void mni_insercion_multipleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mni_insercion_multipleActionPerformed
        // [406, 312]
        DefaultListModel modelo = new DefaultListModel();
        lista_marcadas_insertar.setModel(modelo);
        txt_observacion.setText("");

        dialog_insert_multiple.setModal(true);
        dialog_insert_multiple.setSize(450, 440);
        dialog_insert_multiple.setLocationRelativeTo(this);
        dialog_insert_multiple.setVisible(true);


    }//GEN-LAST:event_mni_insercion_multipleActionPerformed

    private void btn_aceptar_incercion_mActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_aceptar_incercion_mActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

        ListModel modelo = lista_marcadas_insertar.getModel();

        if (empleadoSeleccionado != null && !fechaFila.equals("")) {
            for (int i = 0; i < modelo.getSize(); i++) {

                Timestamp hora = Timestamp.valueOf(fechaFila + " " + modelo.getElementAt(i).toString());
                java.sql.Date fecha = new java.sql.Date(hora.getTime());
                Time horaTime = new Time(hora.getTime());

                Fichada f = new Fichada();
                f.setUserinfoid(empleado.getId());
                f.setFecha(fecha);
                f.setHora(horaTime);
                f.setObservacion(txt_observacion.getText());
                f.setTerminal(contFichada.getNombreTerminal());
                f.setIp(contFichada.getIpTerminal());
                f.setUsuario(usuarioSistema.getUsuario());
                f.setFechasubido(contFichada.getFechaServidor());

//            System.err.println("FECHA2: "+fecha.toString()+" hora: "+horaTime.toString()+" TIMESTAMP: "+hora.toString());
                if (contFichada.insertar(f) > 0) {

                } else {
                    JOptionPane.showMessageDialog(null, "Entrada de datos no insertada.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }

            dialog_insert_multiple.dispose();
            recargar();
            JOptionPane.showMessageDialog(null, "Inserción exitosa!");
        }

        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_btn_aceptar_incercion_mActionPerformed

    private void btn_cancelar_incercion_mActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelar_incercion_mActionPerformed
        dialog_insert_multiple.dispose();
    }//GEN-LAST:event_btn_cancelar_incercion_mActionPerformed

    private void btn_aceptar_detalleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_aceptar_detalleActionPerformed
        if (fichadaFila != null) {
            fichadaFila.setObservacion(txt_detalle.getText());
            if (contFichada.update(fichadaFila) > 0) {
                txt_detalle.setText("");
                dialog_justificar.dispose();

                recargar();
            } else {
                JOptionPane.showMessageDialog(null, "Problemas: No se pudo realizar la modificacion", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Problemas: la fichada es nula", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_aceptar_detalleActionPerformed

    private void mni_justificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mni_justificarActionPerformed
        dialog_justificar.setModal(true);
        dialog_justificar.setSize(350, 250);
        dialog_justificar.setLocationRelativeTo(this);
        dialog_justificar.setVisible(true);

        txt_detalle.setText("");
    }//GEN-LAST:event_mni_justificarActionPerformed

    private void mni_ver_detalle_fichadaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mni_ver_detalle_fichadaActionPerformed
        llenarVerDetalles();

        dialog_ver_detalle.setModal(true);
        dialog_ver_detalle.setSize(350, 250);
        dialog_ver_detalle.setLocationRelativeTo(this);
        dialog_ver_detalle.setVisible(true);
    }//GEN-LAST:event_mni_ver_detalle_fichadaActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        dialog_ver_detalle.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void mni_borrar_de_listaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mni_borrar_de_listaActionPerformed
        DefaultListModel modelo = (DefaultListModel) lista_marcadas_insertar.getModel();
        for (int i = 0; i < modelo.getSize(); i++) {
            if (i == lista_marcadas_insertar.getSelectedIndex()) {
                modelo.remove(i);
            }
        }

        lista_marcadas_insertar.setModel(modelo);
    }//GEN-LAST:event_mni_borrar_de_listaActionPerformed

    private void mni_borrar_todosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mni_borrar_todosActionPerformed
        DefaultListModel modelo = (DefaultListModel) lista_marcadas_insertar.getModel();
        modelo.removeAllElements();

        lista_marcadas_insertar.setModel(modelo);
    }//GEN-LAST:event_mni_borrar_todosActionPerformed

    private void mni_agregar_licencia_temporalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mni_agregar_licencia_temporalActionPerformed
        if (jTable1.getSelectedRow() > -1) {
            jDialog1.setSize(400, 250);
            jDialog1.setLocationRelativeTo(this);
            jDialog1.setResizable(false);
            jDialog1.setModal(true);
            jDialog1.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione fila", "", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_mni_agregar_licencia_temporalActionPerformed

    private void mni_ver_detalle_fichada1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mni_ver_detalle_fichada1ActionPerformed
        mni_ver_detalle_fichadaActionPerformed(evt);
    }//GEN-LAST:event_mni_ver_detalle_fichada1ActionPerformed

    private void mni_justificar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mni_justificar1ActionPerformed
        mni_justificarActionPerformed(evt);
    }//GEN-LAST:event_mni_justificar1ActionPerformed

    private void mni_eliminar_hora1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mni_eliminar_hora1ActionPerformed
        mni_eliminar_horaActionPerformed(evt);
    }//GEN-LAST:event_mni_eliminar_hora1ActionPerformed

    private void mni_insercion_multiple1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mni_insercion_multiple1ActionPerformed
        mni_insercion_multipleActionPerformed(evt);
    }//GEN-LAST:event_mni_insercion_multiple1ActionPerformed

    private void mni_insertar_hora1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mni_insertar_hora1ActionPerformed
        mni_insertar_horaActionPerformed(evt);
    }//GEN-LAST:event_mni_insertar_hora1ActionPerformed

    private void mni_agregar_licencia_temporal1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mni_agregar_licencia_temporal1ActionPerformed
        mni_agregar_licencia_temporalActionPerformed(evt);
    }//GEN-LAST:event_mni_agregar_licencia_temporal1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        if (!txt_justifiacion_de_marcada.getText().equals("")) {
            LicenciaEmpleado le = new LicenciaEmpleado(empleado.getId(),
                    Date.valueOf(fechaFila),
                    new Date(Date.valueOf(fechaFila).getTime() + 86400000),
                    6);
            le.setDetalle(txt_justifiacion_de_marcada.getText());
            le.setJustificacionfalta(true);

            contLicenciaEmpleado.create(le);

            txt_justifiacion_de_marcada.setText("");
            jDialog1.dispose();

            recargar();
        } else {
            JOptionPane.showMessageDialog(null, "Debe de detallar la causa de falta de marcado.", "", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void btn_imprimir_repActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_imprimir_repActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

        if (checkFechas.isSelected()) {
            try {

                fichadaReporte();

            } catch (IOException ex) {
                Logger.getLogger(V_ver_fichada.class
                        .getName()).log(Level.SEVERE, null, ex);

                ContLogger contLogger = new ContLogger();
                String error = "013";
                Loggs loggs = new Loggs(new Timestamp(Calendar.getInstance().getTimeInMillis()),
                        Logger.getLogger(V_ver_fichada.class.getName()).getName(),
                        contLogger.getNombreTerminal(),
                        ex.fillInStackTrace().toString(),
                        error);
                contLogger.insert(loggs);
                JOptionPane.showMessageDialog(null, "Error:" + error + "\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);

            }
        }

        if (checkMes.isSelected()) {
            try {
                accion = "imprimir";
                fichadaReporte();

            } catch (IOException ex) {
                Logger.getLogger(V_ver_fichada.class
                        .getName()).log(Level.SEVERE, null, ex);

                ContLogger contLogger = new ContLogger();
                String error = "014";
                Loggs loggs = new Loggs(new Timestamp(Calendar.getInstance().getTimeInMillis()),
                        Logger.getLogger(V_ver_fichada.class.getName()).getName(),
                        contLogger.getNombreTerminal(),
                        ex.fillInStackTrace().toString(),
                        error);
                contLogger.insert(loggs);
                JOptionPane.showMessageDialog(null, "Error:" + error + "\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);

            }

        }

        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_btn_imprimir_repActionPerformed

    private void mni_exportar_excelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mni_exportar_excelActionPerformed

        List<ReporteFichada> listaReporte = new ArrayList<>();
//        System.out.println("TAMAÑO LISTA: " + listaFichadaDia.size());
        for (Object[] vector : listaFichadaDia) {
            //AÑADIMOS UN OBJ REPORTE
            ReporteFichada rf = new ReporteFichada();
            if (vector[0] != null) {
                switch (u.obtenerDiaDeSemana(Date.valueOf(vector[0].toString()))) {
                    case 1:
                        rf.setDia("D");
                        break;
                    case 2:
                        rf.setDia("L");
                        break;
                    case 3:
                        rf.setDia("M");
                        break;
                    case 4:
                        rf.setDia("M");
                        break;
                    case 5:
                        rf.setDia("J");
                        break;
                    case 6:
                        rf.setDia("V");
                        break;
                    case 7:
                        rf.setDia("S");
                        break;

                }
            }
            if (vector[0] != null) {
                rf.setFecha(u.formatearFecha(vector[0].toString()));
            }
            if (vector[1] != null) {
                rf.setEntrada1(vector[1].toString());
            }
            if (vector[2] != null) {
                rf.setSalida1(vector[2].toString());
            }
            if (vector[3] != null) {
                rf.setEntrada2(vector[3].toString());
            }
            if (vector[4] != null) {
                rf.setSalida2(vector[4].toString());
            }
            if (vector[5] != null) {
                rf.setEntrada3(vector[5].toString());
            }
            if (vector[6] != null) {
                rf.setSalida3(vector[6].toString());
            }
            if (vector[7] != null) {
                rf.setNormales(vector[7].toString());
            }
            if (vector[8] != null) {
                rf.setExtras(vector[8].toString());
            }
            if (vector[9] != null) {
                rf.setRetrazo(vector[9].toString());
            }

            listaReporte.add(rf);

        }

        javax.swing.JFileChooser jF1 = new javax.swing.JFileChooser();
        String ruta = "";
        try {
            if (jF1.showSaveDialog(null) == jF1.APPROVE_OPTION) {
                ruta = jF1.getSelectedFile().getAbsolutePath();
                if (new File(ruta).exists()) {
                    if (JOptionPane.OK_OPTION == JOptionPane.showConfirmDialog(this, "El fichero existe,deseas reemplazarlo?", "Titulo", JOptionPane.YES_NO_OPTION)) //Has aceptado...has lo kieras...... 
                    {
                        ReporteExcel excel = new ReporteExcel();
                        excel.escribirExcel(ruta, listaReporte, empleadoSeleccionado.getName());
                    }
                } else {
                    ReporteExcel excel = new ReporteExcel();
                    ruta = ruta + ".xls";
                    excel.escribirExcel(ruta, listaReporte, empleadoSeleccionado.getName());
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();

            ContLogger contLogger = new ContLogger();
            String error = "015";
            Loggs loggs = new Loggs(new Timestamp(Calendar.getInstance().getTimeInMillis()),
                    Logger.getLogger(V_ver_fichada.class.getName()).getName(),
                    contLogger.getNombreTerminal(),
                    ex.fillInStackTrace().toString(),
                    error);
            contLogger.insert(loggs);
            JOptionPane.showMessageDialog(null, "Error:" + error + "\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);

        }
    }//GEN-LAST:event_mni_exportar_excelActionPerformed

    private void cbx_seccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbx_seccionActionPerformed
        busquedaActiva = false;
        if (checkMes.isSelected() || checkFechas.isSelected()) {
            deshabilitarBotones();
            vaciarTabla();
        }

        lbl_origen.setText("Sección: " + cbx_seccion.getSelectedItem().toString());

        listaEmpleadosTemp = new ArrayList<>();

        DefaultListModel modelo = new DefaultListModel();

        if (cbx_seccion.getSelectedItem().equals("Seleccione...")) {
            listaEmpleadosTemp = listaEmpleado;
        } else {
            for (Userinfo userinfo : listaEmpleado) {
                if (userinfo.getIdSeccion() == listaSecciones.get(cbx_seccion.getSelectedIndex()).getId()) {
                    listaEmpleadosTemp.add(userinfo);
                }
            }

            for (Userinfo e : listaEmpleadosTemp) {
                modelo.addElement(e.getName());
            }

            list_empleados.setModel(modelo);
        }
    }//GEN-LAST:event_cbx_seccionActionPerformed

    private void cbx_mesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbx_mesActionPerformed
        if (checkMes.isSelected() || checkFechas.isSelected()) {
            deshabilitarBotones();
            vaciarTabla();
        }
    }//GEN-LAST:event_cbx_mesActionPerformed

    private void chooser_fechaFinOnCommit(datechooser.events.CommitEvent evt) {//GEN-FIRST:event_chooser_fechaFinOnCommit
        if (checkMes.isSelected() || checkFechas.isSelected()) {
            deshabilitarBotones();
            vaciarTabla();
        }
    }//GEN-LAST:event_chooser_fechaFinOnCommit

    private void chooser_fechaInicioOnCommit(datechooser.events.CommitEvent evt) {//GEN-FIRST:event_chooser_fechaInicioOnCommit
        if (checkMes.isSelected() || checkFechas.isSelected()) {
            deshabilitarBotones();
            vaciarTabla();
        }
    }//GEN-LAST:event_chooser_fechaInicioOnCommit

    private void mni_ver_sin_procesarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mni_ver_sin_procesarActionPerformed
        radiografia = true;
        Runnable r = new Runnable() {

            @Override
            public void run() {
                setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                lbl_porcentaje.setVisible(true);
                barra.setVisible(true);
                barra.setForeground(Color.GREEN);
                barra.setValue(5);
                lbl_porcentaje.setText("5%");

                if (list_empleados.getSelectedIndex() != -1) {
                    barra.setValue(10);
                    lbl_porcentaje.setText("10%");
                    barra.setValue(15);
                    lbl_porcentaje.setText("15%");

                    ContFeriado contFeriado = new ContFeriado();

                    listaFichadaDia = new ArrayList<>();//borramos toodo el contenido del array
                    empleado = listaEmpleadosTemp.get(list_empleados.getSelectedIndex());
                    listaFeriadoMes = contFeriado.feriados(Integer.parseInt(txt_anio.getText()), cbx_mes.getSelectedIndex() + 1);

                    barra.setValue(20);
                    lbl_porcentaje.setText("20%");

                    llenarInfo();

                    barra.setValue(25);
                    lbl_porcentaje.setText("25%");

                    ContUserinfo contEmpleado = new ContUserinfo();
                    if (contEmpleado.tieneHorario(empleado.getId())) {

                        barra.setValue(30);
                        lbl_porcentaje.setText("30%");
                        barra.setValue(35);
                        lbl_porcentaje.setText("35%");

                        try {
                            listar(radiografia);
                        } catch (IOException ex) {
                            Logger.getLogger(V_ver_fichada.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El empleado no tiene un grupo horario asignado.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "No ha seleccionado ningún empleado.", "Error", JOptionPane.ERROR_MESSAGE);
                }

                setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            }
        };
        Thread hilo = new Thread(r);
        hilo.start();
        habilitarBotones();
    }//GEN-LAST:event_mni_ver_sin_procesarActionPerformed

    private void cbx_areasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbx_areasActionPerformed
        if (!cbx_areas.getSelectedItem().toString().equals("Seleccione...")
                && !cbx_areas.getSelectedItem().toString().equals("Todos")) {
            DefaultListModel modelo = new DefaultListModel();
            cbx_seccion.setEnabled(true);
            listaEmpleadosTemp = contEmpleado.obtenerEmpleadosPorArea(listaAreas.get(cbx_areas.getSelectedIndex()).getId());
            listaSecciones = contSeccion.encontrarPorArea(listaAreas.get(cbx_areas.getSelectedIndex()).getId());
            //en caso de ser un jefe de varias areas

            //cargo el combo de secciones en caso de ser jefe de area
            llenarComboSecciones();
            aparecerSecciones();
            //cargo la lista de todos los empleados del jefe d area

            modelo = new DefaultListModel();
//            for (Userinfo e : listaEmpleadosTemp) {
//                modelo.addElement(e.getName());
//            }
            list_empleados.setModel(modelo);
            vaciarTabla();
        } else if (cbx_areas.getSelectedItem().toString().equals("Todos")) {
            DefaultListModel modelo = new DefaultListModel();
            listaEmpleadosTemp = contEmpleado.obtenerEmpleadosPorUSUARIO(usuarioSistema.getUsuario());
            //en caso de ser un jefe de varias areas

            //cargo el combo de secciones en caso de ser jefe de area
            cbx_seccion.setEnabled(false);
//            aparecerSecciones();
            //cargo la lista de todos los empleados del jefe d area

            modelo = new DefaultListModel();
            for (Userinfo e : listaEmpleadosTemp) {
                modelo.addElement(e.getName());
            }
            list_empleados.setModel(modelo);
            vaciarTabla();
        }
    }//GEN-LAST:event_cbx_areasActionPerformed

    private void txt_busquedaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_busquedaKeyReleased
        DefaultListModel modelo = new DefaultListModel();
        busquedaActiva = true;
        listaEmpleadosTempBusqueda = new ArrayList<>();

        String busqueda = txt_busqueda.getText();

        for (Userinfo userinfo : listaEmpleadosTemp) {
            if (userinfo.getName().toUpperCase().contains(busqueda.toUpperCase())) {
                listaEmpleadosTempBusqueda.add(userinfo);
            }
        }

//        listaEmpleadosTemp = listaEmpleado;
        for (Userinfo e : listaEmpleadosTempBusqueda) {
            modelo.addElement(e.getName());
        }

        list_empleados.setModel(modelo);
    }//GEN-LAST:event_txt_busquedaKeyReleased

    private void llenarVerDetalles() {
        if (fichadaFila != null) {
            jLabel25.setText(u.formatearFecha(fichadaFila.getFecha().toString()));
            jLabel26.setText(fichadaFila.getHora().toString());
            if (fichadaFila.isFichadaPorSistema()) {
                jLabel27.setText("SI");
            } else {
                jLabel27.setText("NO");
            }
            jLabel28.setText(fichadaFila.getObservacion());
        } else {
            jLabel25.setText("");
            jLabel26.setText("");
            jLabel27.setText("");
            jLabel28.setText("");
        }

        if (jTable1.getSelectedRow() != -1) {
            if (jTable1.getValueAt(jTable1.getSelectedRow(), jTable1.getSelectedColumn()) == null) {
                jLabel25.setText("");
                jLabel26.setText("");
                jLabel27.setText("");
                jLabel28.setText("");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Importante: seleccione una fila");
        }
    }

    private void recargar() {
        ContPersonaGrupo contPersonaGrupo = new ContPersonaGrupo();
        Time horaEntrada = contPersonaGrupo.getHoraEntrada(contPersonaGrupo.getIdGrupo(empleado.getId())); //treae la hora en la que debe de entrar el empleado dependeiendo de su cuil
        if (horaEntrada != null) {
            Runnable r = new Runnable() {

                @Override
                public void run() {
                    try {
                        listar(radiografia);

                    } catch (IOException ex) {
                        Logger.getLogger(V_ver_fichada.class
                                .getName()).log(Level.SEVERE, null, ex);

                        ContLogger contLogger = new ContLogger();
                        String error = "016";
                        Loggs loggs = new Loggs(new Timestamp(Calendar.getInstance().getTimeInMillis()),
                                Logger.getLogger(V_ver_fichada.class.getName()).getName(),
                                contLogger.getNombreTerminal(),
                                ex.fillInStackTrace().toString(),
                                error);
                        contLogger.insert(loggs);
                        JOptionPane.showMessageDialog(null, "Error:" + error + "\n" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);

                    }
                }
            };
            Thread hilo = new Thread(r);
            hilo.start();
        } else {
            JOptionPane.showMessageDialog(null, "El empleado no tiene un grupo horario asignado.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void fichadaReporte() throws IOException {

        List<ReporteFichada> listaReporte = new ArrayList<>();
        for (Object[] vector : listaFichadaDia) {
            //AÑADIMOS UN OBJ REPORTE
            ReporteFichada rf = new ReporteFichada();
            if (vector[0] != null) {
                switch (u.obtenerDiaDeSemana(Date.valueOf(vector[0].toString()))) {
                    case 1:
                        rf.setDia("D");
                        break;
                    case 2:
                        rf.setDia("L");
                        break;
                    case 3:
                        rf.setDia("M");
                        break;
                    case 4:
                        rf.setDia("M");
                        break;
                    case 5:
                        rf.setDia("J");
                        break;
                    case 6:
                        rf.setDia("V");
                        break;
                    case 7:
                        rf.setDia("S");
                        break;

                }
            }
            if (vector[0] != null) {
                rf.setFecha(u.formatearFecha(vector[0].toString()));
            }
            if (vector[1] != null) {
                rf.setEntrada1(vector[1].toString());
            }
            if (vector[2] != null) {
                rf.setSalida1(vector[2].toString());
            }
            if (vector[3] != null) {
                rf.setEntrada2(vector[3].toString());
            }
            if (vector[4] != null) {
                rf.setSalida2(vector[4].toString());
            }
            if (vector[5] != null) {
                rf.setEntrada3(vector[5].toString());
            }
            if (vector[6] != null) {
                rf.setSalida3(vector[6].toString());
            }
            if (vector[7] != null) {
                rf.setNormales(vector[7].toString());
            }
            if (vector[8] != null) {
                rf.setExtras(vector[8].toString());
            }
            if (vector[9] != null) {
                rf.setRetrazo(vector[9].toString());
            }

            listaReporte.add(rf);

        }

//        resumenHoras();
        String[] resumen = contFichada.resumenHoras(listaFichadaDia);
        ControladorReporte controladorReporte = new ControladorReporte();
        if (accion.equals("vista previa")) {
            controladorReporte.crearReportes(listaReporte, empleado, resumen, cbx_mes.getSelectedItem().toString() + " " + txt_anio.getText(), listaFeriadoMes, contReloj.encontrarPorId(empleado.getReloj()).getNombre(), listaLicencias);
        } else if (accion.equals("imprimir")) {
            try {
                controladorReporte.imprimirReporteFichada(listaReporte, empleado, resumen, cbx_mes.getSelectedItem().toString() + " " + txt_anio.getText(), listaFeriadoMes, contReloj.encontrarPorId(empleado.getReloj()).getNombre(), listaLicencias);
            } catch (Exception e) {
                e.printStackTrace();

                ContLogger contLogger = new ContLogger();
                String error = "017";
                Loggs loggs = new Loggs(new Timestamp(Calendar.getInstance().getTimeInMillis()),
                        Logger.getLogger(V_ver_fichada.class.getName()).getName(),
                        contLogger.getNombreTerminal(),
                        e.fillInStackTrace().toString(),
                        error);
                contLogger.insert(loggs);
                JOptionPane.showMessageDialog(null, "Error:" + error + "\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);

            }
        }

    }

    private void aparecerSecciones() {
        pnl_seccion.setVisible(true);
        lbl_seccion.setVisible(true);
        cbx_seccion.setVisible(true);
    }

    private void desaparecerSecciones() {
        pnl_seccion.setVisible(false);
        lbl_seccion.setVisible(false);
        cbx_seccion.setVisible(false);
    }

    private void activarPanel1() {
        txt_anio.setEnabled(true);
        cbx_mes.setEnabled(true);
    }

    private void desactivarPanel1() {
        txt_anio.setEnabled(false);
        cbx_mes.setEnabled(false);
    }

    private void activarPanel2() {
        chooser_fechaFin.setEnabled(true);
        chooser_fechaInicio.setEnabled(true);
    }

    private void desactivarPanel2() {
        chooser_fechaFin.setEnabled(false);
        chooser_fechaInicio.setEnabled(false);
    }

    private void cargarTablaFechas(List<Fichada> listaFichada) throws IOException {

        vaciarTabla();
        List<String> listaFechas = new ArrayList<>();
        DefaultTableModel modelo = (DefaultTableModel) jTable1.getModel();

        String fecha = "";

        for (Fichada f : listaFichada) {

            if (!fecha.equals(u.traerFecha(f.getFecha()))) {
                fecha = u.traerFecha(f.getFecha());
                listaFechas.add(fecha);
            }
        }

        for (String dia : listaFechas) {
            List<Fichada> fichadaDia = new ArrayList<>();

            Object[] vector = new Object[jTable1.getColumnCount()];
            vector[0] = dia;

            int i = 1;

            //HORAS DE ENTRADA Y SALIDA
            for (Fichada f : listaFichada) {

                if (dia.equals(u.traerFecha(f.getFecha()))) {
                    fichadaDia.add(f);
                }

            }

            for (Fichada f : fichadaDia) {
                vector[i] = u.traerHora(f.getHora());
                i++;
            }

            //TOTALES
//            contFichada.totales(vector, fichadaDia, listaEmpleadosTemp.get(jList1.getSelectedIndex()).getCuil().toString());
            //RETRAZO
//            contFichada.retrazo(vector, fichadaDia, listaEmpleadosTemp.get(jList1.getSelectedIndex()).getCuil().toString());
            //AÑADIMOS LA NUEVA FILA
            modelo.addRow(vector);

            listaFichadaDia.add(vector);

            modelo.addRow(vector);
        }

        jTable1.setModel(modelo);

    }

    List<List<Fichada>> listaLFichada;

    private void cargarTablaMes(List<Fichada> listaFichada, List<String> fechas, boolean radiografia) throws IOException {

        listaLFichada = new ArrayList<>();
        contFichada.setListaLF(listaLFichada);

        barra.setValue(70);
        lbl_porcentaje.setText("70%");
        listaFichadaDia = contFichada.retornarFilaDia(listaFichada, fechas, empleado.getId(), radiografia);

        barra.setValue(80);
        lbl_porcentaje.setText("80%");
        listaLFichada = contFichada.getListLF();

        barra.setValue(90);
        lbl_porcentaje.setText("90%");
        formatoCeldas.setListaFeriados(listaFeriadoMes);
        formatoCeldas.setListaLFichadas(listaLFichada);

//        resumenHoras();
        barra.setValue(95);
        lbl_porcentaje.setText("95%");
        String[] resumen = contFichada.resumenHoras(listaFichadaDia);
        lbl_total_normales.setText(resumen[0]);
        lbl_extras.setText(resumen[1]);
        lbl_retrazo.setText(resumen[2]);
        lbl_sabado_mañana.setText(resumen[3]);
        lbl_sabado_tarde.setText(resumen[4]);
        lbl_domingo.setText(resumen[5]);
        lbl_feriado.setText(resumen[6]);

        barra.setValue(100);
        lbl_porcentaje.setText("100%");

        vaciarTabla();
        DefaultTableModel modelo = (DefaultTableModel) jTable1.getModel();
        for (Object[] v : listaFichadaDia) {
            modelo.addRow(v);
        }
        jTable1.setModel(modelo);
    }

//<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
    //</editor-fold>
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel 
        
        
         //        for (int k = 1; k < vector.length - 3; k++)
         for (int j = 1; j < vector.length - 3; j = j + 2) {//empezamos desde la primer hora de entrada
         if (vector[j] != null) {
         Time h = Time.valueOf(vector[j].toString());
                
         if ((horaEntrada.getTime() - Long.parseLong("7200000")) > h.getTime()) {//7200000 son 2 hs en miliseg.
         Object o = vector[j];
         System.out.println(""+vector[j]);
         Fichada f = null;
         if (j < fichadaDia.size()) {
         f = fichadaDia.get(j);
         }

         for (int i = j + 1; i < vector.length - 3; i++) {
         System.out.println("-");
         if (vector[i] != null) {
         vectorAUX[i - 1] = vectorAUX[i];
         vectorAUX[i] = o;

         if (i < fichadaDia.size()) {
         fichadaDia.set(i - 1, fichadaDia.get(i));
         fichadaDia.set(i, f);
         }

         }
         }
         }
         }
         }
        
         */

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(V_ver_fichada.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(V_ver_fichada.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(V_ver_fichada.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(V_ver_fichada.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                V_ver_fichada dialog = new V_ver_fichada(new javax.swing.JFrame(), true, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JProgressBar barra;
    private javax.swing.JButton btn_aceptar;
    private javax.swing.JButton btn_aceptar_detalle;
    private javax.swing.JButton btn_aceptar_incercion_m;
    private javax.swing.JButton btn_agregar_incercion_m;
    private javax.swing.JButton btn_cancelar_incercion_m;
    private javax.swing.JButton btn_imprimir_rep;
    private javax.swing.JButton btn_listar;
    private javax.swing.JButton btn_vista_previa;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox cbx_areas;
    private javax.swing.JComboBox cbx_hora;
    private javax.swing.JComboBox cbx_hora1;
    private javax.swing.JComboBox cbx_mes;
    private javax.swing.JComboBox cbx_minutos;
    private javax.swing.JComboBox cbx_minutos1;
    private javax.swing.JComboBox cbx_seccion;
    private javax.swing.JCheckBox checkFechas;
    private javax.swing.JCheckBox checkMes;
    private datechooser.beans.DateChooserCombo chooser_fechaFin;
    private datechooser.beans.DateChooserCombo chooser_fechaInicio;
    private javax.swing.JDialog dialog_insert_multiple;
    private javax.swing.JDialog dialog_insertar_hora;
    private javax.swing.JDialog dialog_justificar;
    private javax.swing.JDialog dialog_ver_detalle;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JPopupMenu jPopupMenu2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable jTable1;
    private javax.swing.JMenu jmn_relojes;
    private javax.swing.JLabel lbl_areas;
    private javax.swing.JLabel lbl_cuil;
    private javax.swing.JLabel lbl_domingo;
    private javax.swing.JLabel lbl_extras;
    private javax.swing.JLabel lbl_feriado;
    private javax.swing.JLabel lbl_funcion;
    private javax.swing.JLabel lbl_horario;
    private javax.swing.JLabel lbl_nombre;
    private javax.swing.JLabel lbl_origen;
    private javax.swing.JLabel lbl_porcentaje;
    private javax.swing.JLabel lbl_retrazo;
    private javax.swing.JLabel lbl_sabado_mañana;
    private javax.swing.JLabel lbl_sabado_tarde;
    private javax.swing.JLabel lbl_seccion;
    private javax.swing.JLabel lbl_total_normales;
    private javax.swing.JList list_empleados;
    private javax.swing.JList lista_marcadas_insertar;
    private javax.swing.JMenuItem mni_agregar_licencia_temporal;
    private javax.swing.JMenuItem mni_agregar_licencia_temporal1;
    private javax.swing.JMenuItem mni_borrar_de_lista;
    private javax.swing.JMenuItem mni_borrar_todos;
    private javax.swing.JMenuItem mni_eliminar_hora;
    private javax.swing.JMenuItem mni_eliminar_hora1;
    private javax.swing.JMenuItem mni_exportar_excel;
    private javax.swing.JMenuItem mni_insercion_multiple;
    private javax.swing.JMenuItem mni_insercion_multiple1;
    private javax.swing.JMenuItem mni_insertar_hora;
    private javax.swing.JMenuItem mni_insertar_hora1;
    private javax.swing.JMenuItem mni_justificar;
    private javax.swing.JMenuItem mni_justificar1;
    private javax.swing.JMenuItem mni_ver_detalle_fichada;
    private javax.swing.JMenuItem mni_ver_detalle_fichada1;
    private javax.swing.JMenuItem mni_ver_sin_procesar;
    private javax.swing.JPanel pnl_seccion;
    private javax.swing.JTextField txt_anio;
    private javax.swing.JTextField txt_busqueda;
    private javax.swing.JEditorPane txt_detalle;
    private javax.swing.JEditorPane txt_justifiacion_de_marcada;
    private javax.swing.JLabel txt_mes;
    private javax.swing.JEditorPane txt_observacion;
    private javax.swing.JEditorPane txt_observacion_insSimple;
    // End of variables declaration//GEN-END:variables

    private void llenarInfo() {
        lbl_nombre.setText(empleado.getName());
        lbl_cuil.setText(empleado.getUserid() + "");
        lbl_funcion.setText(new ContUserinfo().buscarFuncion(empleado.getId()));
        lbl_horario.setText(contEmpleado.getHorario(empleado.getId()));

    }

    private void cargarListaAreas(List<Area> listaAreas) {
        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();
        for (Area a : listaAreas) {
            modeloCombo.addElement(a.getDetalle());
        }
        modeloCombo.addElement("Todos");
        modeloCombo.addElement("Seleccione...");
        modeloCombo.setSelectedItem("Seleccione...");
        cbx_areas.setModel(modeloCombo);
    }

}
