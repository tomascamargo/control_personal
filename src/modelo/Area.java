/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Viktor
 */
public class Area {
    private int id;
    private String detalle;

    public Area() {
    }

    public Area(int id, String detalle) {
        this.id = id;
        this.detalle = detalle;
    }

    public Area(String detalle) {
        this.detalle = detalle;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    @Override
    public String toString() {
        return "Area{" + "id=" + id + ", detalle=" + detalle + '}';
    }
    
}
