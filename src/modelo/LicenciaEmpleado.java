/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author Viktor
 */
@Entity
public class LicenciaEmpleado implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private Date fecha_inicio;
    private Date fecha_fin;
    private int userinfoid;
    private int licencia;
    private String detalle;
    private String usuario;
    private boolean justificacionfalta;
    private String terminal;
    private String ip;
    private String usuariosistema;
    private Timestamp fechaauditoria;

    public LicenciaEmpleado() {
    }

    public LicenciaEmpleado(int id, Date fecha_fin, Date fecha_inicio, int userinfoid, int licencia, String detalle,String usuario, boolean justificacionfalta, String terminal, String ip, String usuariosistema, Timestamp fechaauditoria) {
        this.id = id;
        this.fecha_inicio = fecha_inicio;
        this.fecha_fin = fecha_fin;
        this.userinfoid = userinfoid;
        this.licencia = licencia;
        this.detalle = detalle;
        this.usuario = usuario;
        this.justificacionfalta = justificacionfalta;
        this.terminal = terminal;
        this.ip = ip;
        this.usuariosistema = usuariosistema;
        this.fechaauditoria = fechaauditoria;
    }
    
    public LicenciaEmpleado(int userinfoid, Date fecha_inicio, Date fecha_fin, int licencia) {
        this.userinfoid = userinfoid;
        this.fecha_inicio = fecha_inicio;
        this.fecha_fin = fecha_fin;
        this.licencia = licencia;
    }

    public boolean isJustificacionfalta() {
        return justificacionfalta;
    }

    public void setJustificacionfalta(boolean justificacionfalta) {
        this.justificacionfalta = justificacionfalta;
    }
    
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }
    
    public int getUserinfoid() {
        return userinfoid;
    }

    public void setUserinfoid(int userinfoid) {
        this.userinfoid = userinfoid;
    }

    public Date getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(Date fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public Date getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(Date fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    public int getLicencia() {
        return licencia;
    }

    public void setLicencia(int licencia) {
        this.licencia = licencia;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUsuariosistema() {
        return usuariosistema;
    }

    public void setUsuariosistema(String usuariosistema) {
        this.usuariosistema = usuariosistema;
    }

    public Timestamp getFechaauditoria() {
        return fechaauditoria;
    }

    public void setFechaauditoria(Timestamp fechaauditoria) {
        this.fechaauditoria = fechaauditoria;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LicenciaEmpleado)) {
            return false;
        }
        LicenciaEmpleado other = (LicenciaEmpleado) object;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.LicenciaEmpleado[ id=" + id + " ]";
    }
    
}
