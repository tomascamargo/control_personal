/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Viktor
 */
public class Reloj {
    private int id;
    private String nombre;
    private String ip;
    private boolean estado;

    public Reloj() {
    }

    public Reloj(int id, String nombre, String ip, boolean estado) {
        this.id = id;
        this.nombre = nombre;
        this.ip = ip;
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Reloj{" + "id=" + id + ", nombre=" + nombre + ", ip=" + ip + ", estado=" + estado + '}';
    }
    
}
