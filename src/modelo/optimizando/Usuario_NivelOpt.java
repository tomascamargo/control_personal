/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo.optimizando;

/**
 *
 * @author fernando
 */
public class Usuario_NivelOpt {
    
    
    private int id;
    
    private String id_usuario;
    private int id_nivel;

    public Usuario_NivelOpt() {
    }

    public Usuario_NivelOpt(String id_usuario, int id_nivel) {
        this.id_usuario = id_usuario;
        this.id_nivel = id_nivel;
    }

    public String getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(String id_usuario) {
        this.id_usuario = id_usuario;
    }

    public int getId_nivel() {
        return id_nivel;
    }

    public void setId_nivel(int id_nivel) {
        this.id_nivel = id_nivel;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
