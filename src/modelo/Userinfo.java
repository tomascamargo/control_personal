/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo;

import java.sql.Timestamp;

/**
 *
 * @author Viktor
 */
public class Userinfo {
    private int id;
    private Long userid;
    private String name;
    private long cucupers;
    private int reloj;
    private int idSeccion;
    private String terminal;
    private String ip;
    private String usuariosistema;
    private Timestamp fechaauditoria;
    
    public Userinfo() {
    }

    public Userinfo(int id, Long userid, String name, long cucupers, int reloj, int idSeccion, String terminal, String ip, String usuariosistema, Timestamp fechaauditoria) {
        this.id = id;
        this.userid = userid;
        this.name = name;
        this.cucupers = cucupers;
        this.reloj = reloj;
        this.idSeccion = idSeccion;
        this.terminal = terminal;
        this.ip = ip;
        this.usuariosistema = usuariosistema;
        this.fechaauditoria = fechaauditoria;
    }

    public Userinfo(Long userid, String name, long cucupers, int reloj,int idSeccion) {
        this.userid = userid;
        this.name = name;
        this.cucupers = cucupers;
        this.reloj = reloj;
        this.idSeccion = idSeccion;
    }
    
    public Userinfo(Long userid, String name) {
        this.userid = userid;
        this.name = name;
    }
    
    public Userinfo(Long userid, String name, long cucupers) {
        this.userid = userid;
        this.name = name;
        this.cucupers = cucupers;
    }

    public int getIdSeccion() {
        return idSeccion;
    }

    public void setIdSeccion(int idSeccion) {
        this.idSeccion = idSeccion;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCucupers() {
        return cucupers;
    }

    public void setCucupers(long cucupers) {
        this.cucupers = cucupers;
    }

    public int getReloj() {
        return reloj;
    }

    public void setReloj(int reloj) {
        this.reloj = reloj;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUsuariosistema() {
        return usuariosistema;
    }

    public void setUsuariosistema(String usuariosistema) {
        this.usuariosistema = usuariosistema;
    }

    public Timestamp getFechaauditoria() {
        return fechaauditoria;
    }

    public void setFechaauditoria(Timestamp fechaauditoria) {
        this.fechaauditoria = fechaauditoria;
    }
    
    

    @Override
    public String toString() {
        return "Userinfo{" + "id=" + id + ", userid=" + userid + ", name=" + name + ", cucupers=" + cucupers + ", reloj=" + reloj + ", idSeccion=" + idSeccion + '}';
    }
    
}
