/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Timestamp;

/**
 *
 * @author Viktor
 */
public class Loggs {
    private Timestamp fecha;
    private String lugarFalla;
    private String nombreDelTerminal;
    private String detalles;
    private String nroError;
    

    public Loggs() {
    }

    public Loggs(Timestamp fecha, String lugarFalla, String nombreDelTerminal, String detalles,String nroError) {
        this.fecha = fecha;
        this.lugarFalla = lugarFalla;
        this.nombreDelTerminal = nombreDelTerminal;
        this.detalles = detalles;
        this.nroError = nroError;
    }

    public String getNroError() {
        return nroError;
    }

    public void setNroError(String nroError) {
        this.nroError = nroError;
    }
    
    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }
    public String getLugarFalla() {
        return lugarFalla;
    }

    public void setLugarFalla(String lugarFalla) {
        this.lugarFalla = lugarFalla;
    }

    public String getNombreDelTerminal() {
        return nombreDelTerminal;
    }

    public void setNombreDelTerminal(String nombreDelTerminal) {
        this.nombreDelTerminal = nombreDelTerminal;
    }

    public String getDetalles() {
        return detalles;
    }

    public void setDetalles(String detalles) {
        this.detalles = detalles;
    }
    
}
